import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssosiateSiteVpnComponent } from './assosiate-site-vpn.component';

describe('AssosiateSiteVpnComponent', () => {
  let component: AssosiateSiteVpnComponent;
  let fixture: ComponentFixture<AssosiateSiteVpnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssosiateSiteVpnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssosiateSiteVpnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
