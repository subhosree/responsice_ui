interface sotnServiceList{
  service: sotnService;
  sites: sotnSites[];
}

interface sotnService{
  serviceName:String;
  description:String;
  cos:String;
  serviceClassificationType:String;
  SLAType:String;
  highAvailability:String;
  cir:String;
  eir:String;
}

interface sotnSites {
  siteId:String;
  sotnSiteName:String;
  description:String;
  zipCode:String;
  address:String;
  vlan:String;
}