import { Component, OnInit } from '@angular/core';

import { MyhttpService } from '../myhttp.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-model',
  templateUrl: './model.component.html',
  styleUrls: ['./model.component.less']
})
export class ModelComponent implements OnInit {

  models = [];
  edges = [];
  modelArray = [];
  edgeSelectedValue: any;
  displayMsg: boolean = false;
  modelPostData = {} as modelInfo;

  constructor(private http: HttpClient, private myhttp: MyhttpService) { }
  ngOnInit() {
    this.getModelData();
  }

  getModelData() {
    this.myhttp.getModelJsonData().subscribe((data) => {
      let tempArray = data.body.modelInfoList;
      tempArray.forEach(item => {
        item.checked = false;
      });

      this.modelArray = tempArray; //data.body.modelInfoList;
      this.edges = data.body.edgeNameList;
      this.edgeSelectedValue = this.edges[0];
    }, (err) => {
      console.log(err);
    }
    );
  }

  postModelData(data) {
    let comp = this;
    this.myhttp.postModelData(data)
      .subscribe((data) => {
        setTimeout(function () {
          comp.displayMsg = false;
        }, 3000);
        comp.displayMsg = true;

      }, (err) => {
        console.log(err);
      }
      );

  }
  selectedData(a, i) {
    this.modelPostData.modelInfoList = this.modelArray[i];
    this.modelArray.forEach((item, ind) => {
      item.checked = ind == i ? true : false;
    });
  }

  selectedEdge(value: string): void {
    this.modelPostData.edgeNameList = this.edgeSelectedValue;
  }

  sendModelInfo() {
    if (this.modelPostData.modelInfoList == null || this.modelPostData.edgeNameList == null) {
      return false;
    }
    else {
      this.postModelData(this.modelPostData);
    }
  }
}
