import { Component, OnInit, Input } from '@angular/core';
import { MyhttpService } from '../myhttp.service';
import { OrderserviceeService } from './../orderservicee.service';


@Component({
  selector: 'app-ordered-service',
  templateUrl: './ordered-service.component.html',
  styleUrls: ['./ordered-service.component.less'],
  
})
export class OrderedServiceComponent implements OnInit {
 
  @Input() orderServiceData;
  
  expandDataSet = [
    { rowIdx: 1, name: 'siteInformation', quantity: 'quantity', prize: 'price', expand: true, quantityNo:1,cost:1, mobileExpand:false },
    { rowIdx: 2, name: 'vpnInformation', quantity: 'quantity', prize: 'price', expand: true, quantityNo:1,cost:1 , mobileExpand:false},
    { rowIdx: 3, name: 'bandWidthPolicy', quantity: 'quantity', prize: 'price', expand: true, quantityNo:1,cost:1, mobileExpand:false},
    { rowIdx: 4, name: 'vasInformation', quantity: 'quantity', prize: 'price', expand: true, quantityNo:1,cost:1, mobileExpand:false },

  ];

  summaryInfo={} as summary;
  serviceList={} as serviceList;

  camerasLength:number=0;
  camerasCost:number=0;
  bandWidthLength:number=0;
  bandWidthCost:number=0;
  vpnDataLength:number=0;
  vpnDataCost:number=0;
  siteDataLength:number=0;
  siteDataCost:number=0;
  expandTrue:boolean=true;
  wlanAccess:string="";
  Tablenth:any;

  orderedServiceDetails:boolean = false;
  listOfParentData: any[] = [];
  listOfChildrenData: any[] = [];
  isMobile:boolean=false;

  constructor(private myhttp: OrderserviceeService) { }

  ngOnInit() {
   
    this.isMobile = screen.width < 575 ? true : false;
 
   
    if(this.orderServiceData && this.orderServiceData.numOfCamera)
    {
      this.orderedServiceDetails = true;
      this.postOrderServiceData();      
    }
    else
    {
      this.orderedServiceDetails = false;
      this.loadSummaryData();
      
    }
  
  }


  mapPostObject():any{
    let obj = {} as postObjInterface;
    obj.wlanAccess = this.orderServiceData.wanAccess;
    obj.bandWidth = this.orderServiceData.serviceBandwidth;
    obj.vpnInformations = [];

    let vpnObj;
    for(var i=0;i<this.orderServiceData.vpnArray.length;i++)
    {
      vpnObj = this.orderServiceData.vpnArray[i];
      delete vpnObj.expand;
      vpnObj.cameras = this.orderServiceData.numOfCamera;

      for(var j=0;j<this.orderServiceData.orderBandwidth.length;j++)
      {
        if(this.orderServiceData.orderBandwidth[j].vpnId == vpnObj.vpnId)
        {
          vpnObj.vpnBandwidth = this.orderServiceData.orderBandwidth[j].vpnBandwidth;       

        }      
      }
      obj.vpnInformations.push(vpnObj);
    }
    return obj;
  }

  postOrderServiceData(): void {
    let finalObj = this.mapPostObject();
    this.myhttp.postOrderServiceData(finalObj)
        .subscribe((data)=> {
          this.summaryInfo = data;
          console.log("summary info post ====>", this.summaryInfo);
          console.log("post order service bean ====>", finalObj);
          this.wlanAccess = this.summaryInfo.wlanAccess.join(",");
          this.getCameraData(this.summaryInfo);
          this.getBandwidthData(this.summaryInfo);
          this.getVpnData(this.summaryInfo);
          this.getSiteData(this.summaryInfo);
          
          console.log("post");
        }, (err) => {
          console.log(err);
        }
      )
  }
 

  loadSummaryData(){

    // for (let i = 0; i < 3; ++i) {
    //   this.listOfParentData.push({
    //     key: i,
    //     "serviceInstanceId": "600",
    //     "serviceInstanceName": "600",
    //     "serviceType": "example-service-type-val-52265",
    //     "serviceRole": "example-service-role-val-24156",
    //     "resourceVersion": "1556088536117",
    //     expand: false
    //   });
    // }
    
    
    this.myhttp.getSummaryData().subscribe((data)=>{     
      this.summaryInfo = data;
      this.Tablenth = data.vpnInformations.sites;
      console.log("summary info load  ====>", this.Tablenth);
      this.wlanAccess= this.summaryInfo.wlanAccess.join(",");     
      this.getCameraData(this.summaryInfo);
      this.getBandwidthData(this.summaryInfo);
      this.getVpnData(this.summaryInfo);
      this.getSiteData(this.summaryInfo);  
      //this.Tablelength(this.summaryInfo);    
    }, (err) => {
      console.log(err);
    });
  }
  // Tablelength(){
  //   if(this.Tablenth.length != 0){
  //     document.getElementById("vpnData").style.minHeight = this.Tablenth.length+ "*" + "20px";
  //   }
  // }
  getCameraData(summaryInfoData){
    if(summaryInfoData && summaryInfoData.vpnInformations &&  summaryInfoData.vpnInformations.length)
    {
      this.camerasLength = Number(summaryInfoData.vpnInformations[0].cameras);
      this.expandDataSet[3].quantityNo = this.camerasLength;     
    } 
    this.camerasCost = summaryInfoData.cameraCost;
    this.expandDataSet[3].cost = this.camerasCost;     
  }

  getBandwidthData(summaryInfoData){
    this.bandWidthLength=summaryInfoData.vpnInformations.length;
    this.expandDataSet[2].quantityNo = this.bandWidthLength;
    this.bandWidthCost = summaryInfoData.bandwidthCost;
    this.expandDataSet[2].cost = this.bandWidthCost;
  }

  getVpnData(summaryInfoData){
    this.vpnDataLength=summaryInfoData.vpnInformations.length;
    this.expandDataSet[1].quantityNo = this.vpnDataLength;
    this.vpnDataCost = summaryInfoData.vpnCost;
    this.expandDataSet[1].cost = this.vpnDataCost;
  }

  getSiteData(summaryInfoData){
    this.siteDataLength = summaryInfoData.vpnInformations[0].sites.length;
    this.expandDataSet[0].quantityNo = this.siteDataLength;

    var cost = 0;
    for(var i=0;i< summaryInfoData.vpnInformations[0].sites.length;i++)
    {
      cost+=Number(summaryInfoData.vpnInformations[0].sites[i].price);
      
    }
    console.log(summaryInfoData.vpnInformations[0].sites)
    this.siteDataCost = cost; 
    this.expandDataSet[0].cost = this.siteDataCost;          
  }
  // Tablelength(summaryInfoData){
  //     this.Tablenth = summaryInfoData.vpnInformations[0].sites.length;
  //     if( this.Tablenth != 0){
  //       var sum = this.Tablenth * 30
  //       console.log("sum", sum)
  //       document.getElementById("vpnData").style.height =  '0'+sum ;
  //     }
  //   }

}
