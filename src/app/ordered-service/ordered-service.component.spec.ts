import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderedServiceComponent } from './ordered-service.component';

describe('OrderedServiceComponent', () => {
  let component: OrderedServiceComponent;
  let fixture: ComponentFixture<OrderedServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderedServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderedServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
