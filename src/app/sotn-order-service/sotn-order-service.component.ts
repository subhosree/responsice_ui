import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MyhttpService } from '../myhttp.service';
import { OrderserviceeService } from './../orderservicee.service';


@Component({
  selector: 'app-sotn-order-service',
  templateUrl: './sotn-order-service.component.html',
  styleUrls: ['./sotn-order-service.component.less']  
})
export class SotnOrderServiceComponent implements OnChanges {

  @Input() orderServiceData;
  @Input() serviceInstanceId;
  @Input() subscriptionType
  expandDataSet = [
    { rowIdx: 1, name: 'serviceInformation',cost: '', expand: true , mobileExpand:false},
    { rowIdx: 2, name: 'vpnInformation',cost: '', expand: true,mobileExpand:false }
  ];

  summaryInfo = {} as summary;
  serviceList = {} as serviceList;
  mapped: any;
  myKeys = [] as Array<any>;
  isMobile:boolean=false;

  constructor(private myhttp: OrderserviceeService) { }

  ngOnInit()
  {
    this.isMobile = screen.width<576 ? true :false;
  }


  ngOnChanges(changes: SimpleChanges) {    
    console.log("on change");
    if (this.serviceInstanceId) {
      this.myhttp.getSotnSummaryData(this.subscriptionType, this.serviceInstanceId).subscribe((data) => {
        this.assignData(data, false);
      }, (err) => {
        console.log(err);
      });
    }
    else {
      this.myhttp.postSotnDataToGetCost(this.orderServiceData).subscribe((data) => {
        this.assignData(data, true);
      }, (err) => {
        console.log(err);
      });
    }
  }

  assignData(data,isCost) {
    this.summaryInfo = data;
    this.mapped = JSON.parse(JSON.stringify(data));
    delete this.mapped.vpninformations;
    delete this.mapped.vpnInformations;
    if(isCost)
    {
      delete this.mapped.cost;
      this.expandDataSet[0].cost = this.summaryInfo.cost.serviceCost;
      this.expandDataSet[1].cost = this.summaryInfo.cost.vpnCost;
    }  
    this.myKeys = Object.keys(this.mapped);
  }

}
