import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SotnOrderServiceComponent } from './sotn-order-service.component';

describe('SotnOrderServiceComponent', () => {
  let component: SotnOrderServiceComponent;
  let fixture: ComponentFixture<SotnOrderServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SotnOrderServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SotnOrderServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
