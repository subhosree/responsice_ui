import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataMonitorCmccComponent } from './data-monitor-cmcc.component';

describe('DataMonitorComponent', () => {
  let component: DataMonitorCmccComponent;
  let fixture: ComponentFixture<DataMonitorCmccComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataMonitorCmccComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataMonitorCmccComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
