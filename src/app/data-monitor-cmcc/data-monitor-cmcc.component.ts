import { Component, OnInit, Input, Output, EventEmitter, HostBinding, OnDestroy } from '@angular/core';
import { MyhttpService } from '../myhttp.service';
import { slideToRight } from '../animates';
import { ThemeService } from '../theme';


@Component({
  selector: 'app-data-monitor-cmcc',
  templateUrl: './data-monitor-cmcc.component.html',
  styleUrls: ['./data-monitor-cmcc.component.less']

})
export class DataMonitorCmccComponent implements OnInit, OnDestroy {


  constructor(private myhttp: MyhttpService, private themeService: ThemeService) { }
  intervalData: any;
  returnResponse: boolean = true;
  ngOnInit() {
    this.intervalData = setInterval(() => {
      if (this.returnResponse)
        this.getBandwidthChartData();
      this.returnResponse = false;
    }, 1000);
  }

  ngOnDelete() {
    if (this.intervalData) {
      clearInterval(this.intervalData);
    }
  }

  ngOnDestroy()
  {
    if (this.intervalData) {
      clearInterval(this.intervalData);
    }

  }
  sourceNameSelected: String = "day";

  bandwidthLineChartInit: Object = {
    height: 260,
    width: 370,
    option: {
      tooltip: {
        trigger: 'item',
        formatter: '<b>Time:</b>{b}<br/><b>{a}:</b>{c}KB'
      },
      legend: {
        data: [''],
        textStyle: {
          color: '#ffff'
        }

      },
      xAxis: {
        type: 'time',
        boundaryGap: true,
        axisTick: {
          show: false,
        },
        axisLine: {
          show: false
        },
        axisLabel: {
          color: '#ffff'
        }
      },
      yAxis: {
        min: 0,
        max: 10000,
        splitNumber: 10,
        type: 'value',
        axisLabel: {
          formatter: '{value} KB',
          textStyle: {
            color: '#ffff'
          }
        },
        axisPointer: {
          snap: true
        }
      },
      visualMap: {
        show: false,
        dimension: 1,
        pieces: [{
          lte: 10000,
          gt: 0,
          color: '#00fffc'
        }]
      },
      series: [
        {
          name: 'BandWidth',
          type: 'line',
          step: 'end',
          data: []
        }
      ]
    }
  };

  bandwidthLineChartData: Object = this.bandwidthLineChartInit;

  getBandwidthChartData() {
    let obj = {
      vpnId: "vpnid",
      duration: "1800"
    }

    this.myhttp.getBandwidthChartData(obj)
      .subscribe((data) => {
        let yListNewDate = [];
        data.forEach(function (d, i) {
          var dateSelected = d.changeTime.split(' ');
          var dateArray = dateSelected[0].split("-");
          var dateF = "20" + dateArray[2] + "-" + dateArray[1] + "-" + dateArray[0] + " " + dateSelected[1];
          yListNewDate.push(
            {
              name: dateF,
              value: [dateF, d.bandwidth
              ]

            }
          )
        });
        this.bandwidthLineChartData = {
          series: [
            {
              data: yListNewDate //.slice(Math.max(yListNewDate.length - 10, 0))
            }
          ]
        }
        this.returnResponse = true;
      }, (err) => {
        console.log(err);
        this.returnResponse = true;
      })
  }

}

