import { Component, OnInit, Input } from '@angular/core';
import { SimpleChanges } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-time-line',
  templateUrl: './time-line.component.html',
  styleUrls: ['./time-line.component.less']
})
export class TimeLineComponent implements OnInit {
  // 图形数据
  @Input() chartData;
  // 初始化数据
  @Input() initData;


  constructor() { }

  ngOnInit() {    
    this.initOpts = {
      renderer: 'canvas',
      height: this.initData.height,
      width: this.initData.width,
    };
    this.lineOption = {
      tooltip: this.initData.option.tooltip,
      icon: 'circle',
      legend: this.initData.option.legend,
      dataZoom: this.initData.option.dataZoom,
      grid: {
        left: '1%',
        right: '5%',
        top: '10%',
        bottom: '10%',
        containLabel: true
      },
      xAxis: this.initData.option.xAxis,
      yAxis: {
        axisTick: {
          show: false,
        },
        axisLine: {
          show: false
        }
      },
      series: this.initData.option.series
    }

    if(this.initData.option.yAxis.min !== undefined)
    {
      this.lineOption.yAxis.min = this.initData.option.yAxis.min;
    }
    if(this.initData.option.yAxis.max !== undefined)
    {
      this.lineOption.yAxis.max = this.initData.option.yAxis.max;
    }  
    if(this.initData.option.yAxis.splitNumber !== undefined)
    {
      this.lineOption.yAxis.splitNumber = this.initData.option.yAxis.splitNumber;
    } 
    if(this.initData.option.yAxis.axisLabel !== undefined)
    {
      this.lineOption.yAxis.axisLabel = this.initData.option.yAxis.axisLabel;
    }
    if(this.initData.option.visualMap !== undefined)
    {
      this.lineOption.visualMap = this.initData.option.visualMap;
    }                 

  }

  ngOnChanges(changes: SimpleChanges) {

    
    // 当有实例的时候再执行，相当于第一次不执行下面方法
    if (this.chartIntance) {
      this.chartDataChange()
    }
  }
  // 初始化图形高度
  initOpts: any;
  // 折线图配置
  lineOption: any;
  // 实例对象
  chartIntance: any;
  // 数据变化
  updateOption: any;
  chartDataChange() {
    this.updateOption = this.chartData;
  }
  chartInit(chart) {
    this.chartIntance = chart;    
  }



}
