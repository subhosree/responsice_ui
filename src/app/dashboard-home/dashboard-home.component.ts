import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'app-dashboard-home',
  templateUrl: './dashboard-home.component.html',
  styleUrls: ['./dashboard-home.component.less']
})
export class DashboardHomeComponent implements OnInit {
  array = ["banner1"];
  isMobile:boolean = false;
  slideIndex:number=0;
  slides:string;

  constructor(private router:Router) { }

  ngOnInit() {
    this.isMobile = screen.width < 575 ? true : false;
  }

  goToSDwanOrderservice(){
    if(this.isMobile == true){
      this.router.navigate(["/ccvpn/os/orderService"]);
    }
    else {
      document.getElementById('orderService').click();
      this.router.navigate(["/ccvpn/os/orderService"]);
    }
   
  }
  goToSotnOrderservice(){
  if(this.isMobile == true){
    this.router.navigate(["/ccvpn/os/sotn-orderService"]);
  }
  else{
    document.getElementById('orderService').click();
    this.router.navigate(["/ccvpn/os/sotn-orderService"]);
  }
  }
  goToOrderedService(){
    if(this.isMobile == true){
    this.router.navigate(["/ccvpn/os/viewOrderedService"]);
    }
    else{
    document.getElementById('manageService').click();
    this.router.navigate(["/ccvpn/os/viewOrderedService"]); 
    }
  }
  goToActivatesite(){
    if(this.isMobile == true){
      this.router.navigate(["/ccvpn/ms/activateSite"]);
    }
    else{
      document.getElementById('manageService').click();
      this.router.navigate(["/ccvpn/ms/activateSite"]);
    }
  }
  goToDatamonitor(){
    if(this.isMobile == true){
    this.router.navigate(["/ccvpn/ms/data-monitor"]);
    }
    else{
    document.getElementById('monitorService').click();
    this.router.navigate(["/ccvpn/ms/data-monitor"]); 
    }
  }

  goToNetworkTopology(){
    if(this.isMobile == true){
      this.router.navigate(["/ccvpn/ms/topology"]);
    }
    else{
      document.getElementById('monitorService').click();
      this.router.navigate(["/ccvpn/ms/topology"]);
    }
   
  }
  goToResourceTopology(){
    if(this.isMobile == true){
    this.router.navigate(["/ccvpn/treeTopology"]);
  }
  else{
    document.getElementById('monitorService').click();
    this.router.navigate(["/ccvpn/treeTopology"]);
  }
}




}
