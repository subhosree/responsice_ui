import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router'

import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import { OrderserviceeService } from './../orderservicee.service';
import { ConfigurevpnComponent } from '../configurevpn/configurevpn.component';
import { ConfiguresiteComponent } from './../configuresite/configuresite.component';
import { VasComponent } from './../vas/vas.component';
import { BandwidthComponent } from '../bandwidth/bandwidth.component';
import { compareAsc } from 'date-fns';


@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.less']
})
export class OrderComponent implements OnInit {

  configSite = true;
  configVPN = false;
  configBandwidth = false;
  isMobile: boolean = false;




  panels = [
    {
      active: true,
      disabled: false
    }, {
      active: false,
      disabled: false
    }, {
      active: false,
      disabled: false
    }, {
      active: false,
      disabled: false
    }, {
      active: false,
      disabled: false
    }]
    ;



  orderServiceData = {} as finalOrderService;

  configSiteFlag = false;
  configVpnFlag = false;
  ngOnInit() {

    this.isMobile = screen.width < 575 ? true : false

  }

  @ViewChild(ConfiguresiteComponent) sitechild;
  @ViewChild(ConfigurevpnComponent) vpnchild;
  @ViewChild(VasComponent) vaschild;
  @ViewChild(BandwidthComponent) bandchild;

  constructor(private myhttp: OrderserviceeService, private router:Router) {
    this.nextTip = "Please Enter All field Marked in red";
  }

  current = 0;
  nextTip: string = "";

  enableNext(idx): void {
    if (idx == 1) {
      this.selectOrderSiteData();
    }
  }

  pre(): void {
  
    this.current -= 1;
    if (this.current == 0) {
      this.selectVpnData();
    }
    if (this.current == 1) {
      this.selectBandwidthData();
    }
    if (this.current == 2) {
      this.selectVasData();
      //this.postOrderServiceData();
    }
  }

  next(): void {

    this.current += 1;

    if (this.current == 1) {
      this.selectOrderSiteData();
    }
    if (this.current == 2) {
      this.selectVpnData();
    }
    if (this.current == 3) {
      this.selectBandwidthData();
    }
    if (this.current == 4) {
      this.selectVasData();
      //this.postOrderServiceData();
    }

  }
  saveData(evt, idx): void {

    if (evt) {
      this.panels[idx].active = true;

    }
    else {
      this.panels[idx].active = false;
      switch (idx) {
        case 0:
          this.selectOrderSiteData();
          break;
        case 1:
          this.selectVpnData();
          break;
        case 2:
          this.selectBandwidthData();
          break;
        case 3:
          this.selectVasData();
          break;

      }
    }

  }

  displayMsg: boolean = false;
  displayMsg2: boolean = false;

  selectOrderSiteData(): void {
    this.orderServiceData.wanAccess = this.sitechild.wanArr;
    this.orderServiceData.serviceBandwidth = this.sitechild.serviceBandwidth;
    this.orderServiceData.sites = this.sitechild.products;

    //alert("hello"+ this.orderServiceData.sites);

    if (this.orderServiceData.wanAccess.length == 0 || this.orderServiceData.sites.length == 0) {
      this.displayMsg = true;
      var comp = this;
      setTimeout(function () {
        comp.displayMsg = false;
      }, 2000);
      this.current -= 1;
    }
   

  }

  selectVpnData(): void {
    this.orderServiceData.vpnArray = this.vpnchild.vpnArray;
    if(this.orderServiceData.vpnArray.length != 0){
     
    }
  }

  selectBandwidthData(): void {
    this.orderServiceData.orderBandwidth = this.bandchild.dataSet;

    if(this.orderServiceData.orderBandwidth.length != 0){
      
    }
  }

  selectVasData(): void {
    this.orderServiceData.numOfCamera = this.vaschild.demoValue;
    if(this.orderServiceData.numOfCamera.length != 0){
      
    }
  }

  mapPostObject(): any {
    let obj = {} as postObjInterface;
    obj.wlanAccess = this.orderServiceData.wanAccess;
    obj.bandWidth = this.orderServiceData.serviceBandwidth;
    obj.vpnInformations = [];

    let vpnObj;
    for (var i = 0; i < this.orderServiceData.vpnArray.length; i++) {
      vpnObj = this.orderServiceData.vpnArray[i];
      delete vpnObj.expand;
      vpnObj.cameras = this.orderServiceData.numOfCamera;
      //obj.vpnInformation.push(vpnObj);

      for (var j = 0; j < this.orderServiceData.orderBandwidth.length; j++) {
        if (this.orderServiceData.orderBandwidth[j].vpnId == vpnObj.vpnId) {
          vpnObj.vpnBandwidth = this.orderServiceData.orderBandwidth[j].vpnBandwidth;
          //vpnObj.vpnThreshold = this.orderServiceData.orderBandwidth[j].Threshold;

        }
      }
      obj.vpnInformations.push(vpnObj);
    }

    return obj;
  }

  finalPostOrderServiceData(): void {
    //this.isVisible = true;
    // this.displayMsg2 = true;

    //   setTimeout(function () {
    //     this.displayMsg2= false;
    //     console.log("hello mannu");
    //   }, 2000); 
    this.displayMsg2 = true;
    var comp = this;
    setTimeout(function () {
      comp.displayMsg2 = false;
    }, 1000);
    //this.goToPage();
    //delete this.orderServiceData.sites;
    let finalObj = this.mapPostObject();
    this.myhttp.finalPostOrderServiceData(finalObj)
      .subscribe((data) => {
      
        console.log("final post");
        console.log("final order service data is===>", finalObj);
        this.goToPage();
      }, (err) => {
        this.goToPage();
        console.log(err);
      }
      )

  }
  isVisible = false;
  isOkLoading = false;

  handleOk(): void {
    this.isOkLoading = true;
    setTimeout(() => {
      this.isVisible = false;
      this.isOkLoading = false;
    }, 1000);
  }

  handleCancel(): void {
    this.isVisible = false;
  }



  array = [];

  //move this code to bandwidth component
  addBandwidthData(): void {
   
    console.log("addBandwidthData");
    this.sitechild.configFinalOrderServicePostData.orderBandwidth = this.array;
  }

  //move tgus code to order component;
  noOfCamera;
  addVasData(): void {
  
    console.log("addVasData");
    this.sitechild.configureFinalOrderServicePostData.noOfCamera = this.noOfCamera;
  }

  private doneRoute;
  done(): void {
    console.log('done');
    this.doneRoute = "os/manage-service";
  
  }
  
  goToPage(): void {
    var comp = this;
    setTimeout(function () {
      if(comp.isMobile == true){
        comp.router.navigate(['ccvpn/os/viewOrderedService']);
      }
     
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }, 1000);

  }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      

}                                                                       
