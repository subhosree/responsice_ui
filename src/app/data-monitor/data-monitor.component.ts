import { Component, OnInit, Input, Output, EventEmitter, HostBinding, OnDestroy } from '@angular/core';
import { MyhttpService } from '../myhttp.service';
import { slideToRight } from '../animates';
import { ThemeService } from '../theme';
import { orderService } from '../Order.service'
import { OrderserviceeService } from './../orderservicee.service';

@Component({
  selector: 'app-data-monitor',
  templateUrl: './data-monitor.component.html',
  styleUrls: ['./data-monitor.component.less']

})
export class DataMonitorComponent implements OnInit, OnDestroy {

  constructor(private myhttp: MyhttpService, private themeService: ThemeService, private order: OrderserviceeService) { }
  intervalData: any;
  setIntervalVal: any = "1800";
  returnResponse: boolean = true;
 
  sotnImg: any;

  serviceSubscriptionList = [] as Array<any>;
  serviceInstanceList = [] as Array<any>;
  selectedSubscriptionType: string;
  selectedServiceInstance: string = "";


  //Get SubscriptionType
  getSubscribeTypes() {
    this.order.getSubscriptionType().subscribe((data) => {
      this.serviceSubscriptionList = data.subscriptions;
    }, (err) => {
      console.log(err);
    });
  }

  //Get subscription instanceID by calling With Subscription Type
  getServiceInstanceList(subscriptionType) {
    this.serviceInstanceList = [];
    this.selectedServiceInstance = "";
  
    this.order.getServiceInstance(subscriptionType).subscribe((data) => {
      this.serviceInstanceList = data.serviceInstanceList;
    }, (err) => {
      console.log(err);
    });
  }

  setIntervalToGetData(service) {
    if (this.intervalData) {
      clearInterval(this.intervalData);
    }
    if (this.returnResponse) {
      if (this.selectedSubscriptionType.toLowerCase() == "sdwan")
      {
        this.getBandwidthChartData();
      }  
      else
      {
        this.getBandWidthChartImgName();
      }  
    }
    this.returnResponse = false;
    
    this.intervalData = setInterval(() => {
      if (this.returnResponse) {
        if (this.selectedSubscriptionType.toLowerCase() == "sdwan")
        {
          this.getBandwidthChartData();
        }  
        else
        {
          this.getBandWidthChartImgName();
        }  
      }
      this.returnResponse = false;
    }, 1000);
  }


  ngOnInit() {
    this.getSubscribeTypes()
  }

  ngOnDestroy() {
    if (this.intervalData) {
      clearInterval(this.intervalData);
    }

  }

  ngOnDelete() {
    if (this.intervalData) {
      clearInterval(this.intervalData);
    }
  }

  onChange(item) {
    this.setIntervalVal = item;
  }

  sourceNameSelected: String = "day";
  bandwidthLineChartInit: Object = {
    height: screen.width < 575 ? 300 : 420,
    width: screen.width < 575 ? (screen.width - 10) : window.innerWidth - 400,
    option: {
      tooltip: {
        trigger: 'item',
        formatter: '<b>Time:</b>{b}<br/><b>{a}:</b>{c}KB'
      },
      legend: {
        data: ['BandWidth'],
        textStyle: {
          color: this.themeService.getActiveTheme().name == 'light' ? '#54657e' : '#00fffc'
        }

      },
      xAxis: {
        type: 'time',
        boundaryGap: true,
        axisTick: {
          show: false,
        },
        axisLine: {
          show: false
        },
        axisLabel: {
          color: this.themeService.getActiveTheme().name == 'light' ? '#54657e' : '#00fffc'
        }
      },
      yAxis: {
        min: 0,
        max: 10000,
        splitNumber: 10,
        type: 'value',
        axisLabel: {
          formatter: '{value} KB',
          textStyle: {
            color: this.themeService.getActiveTheme().name == 'light' ? '#54657e' : '#00fffc'
          }
        },
        axisPointer: {
          snap: true
        }
      },
      visualMap: {
        show: false,
        dimension: 1,
        pieces: [{
          lte: 10000,
          gt: 0,
          color: this.themeService.getActiveTheme().name == 'light' ? 'green' : '#7CFC00'
        }]
      },
      series: [
        {
          name: 'BandWidth',
          type: 'line',
          step: 'end',
          data: []
        }
      ]
    }
  };

  bandwidthLineChartData: Object = this.bandwidthLineChartInit;
  onResize(event) {
    if(screen.width > 575)
    {
      this.bandwidthLineChartData = {
        width: event.target.innerWidth - 400
      };
    }  
  }

  getBandWidthChartImgName()
  {
    this.myhttp.getBandWidthChartImgName(this.selectedSubscriptionType,this.selectedServiceInstance)
    .subscribe((data) => {
      this.sotnImg = "./assets/images/" + data.file.fileName + "?t=" + new Date().getTime();;
      this.returnResponse = true;
    }, (err) => {
      console.log(err);
      this.returnResponse = true;
      console.log("return");
    })
  }

  getBandwidthChartData() {
    let obj = {
      vpnId: "vpnid",
      duration: this.setIntervalVal
    }

    this.myhttp.getBandwidthChartData(obj)
      .subscribe((data) => {
        let yListNewDate = [];
        data.forEach(function (d, i) {
          var dateSelected = d.changeTime.split(' ');
          var dateArray = dateSelected[0].split("-");
          var dateF = "20" + dateArray[2] + "-" + dateArray[1] + "-" + dateArray[0] + " " + dateSelected[1];

          yListNewDate.push(
            {
              name: dateF,
              value: [dateF, d.bandwidth]

            }
          )
        });
        this.bandwidthLineChartData = {
          series: [
            {
              data: yListNewDate
            }
          ]
        }
        this.returnResponse = true;
        console.log("return");
      }, (err) => {
        console.log(err);
        this.returnResponse = true;
        console.log("return");
      })
  }

}