import { Component, OnInit } from '@angular/core';
import { MyhttpService } from '../myhttp.service';

@Component({
  selector: 'app-activate-site',
  templateUrl: './activate-site.component.html',
  styleUrls: ['./activate-site.component.less']
})
export class ActivateSiteComponent implements OnInit {
  sites:any=[];
  deviceWidth:any = screen.width; 
  isMobile:boolean = false;
  constructor(private myhttp: MyhttpService) { }
  ngOnInit() {
    this.deviceWidth = screen.width; 
    this.isMobile = this.deviceWidth < 575 ? true : false;
    this.getSitesData();
  }

  getSitesData() {
    this.myhttp.getSitesData().subscribe((data) => {          
      this.sites = data.body.filter(item => (item.role.toLowerCase().indexOf("hub")<0)&& (item.siteStatus.toLowerCase()=="online"));
     
      this.sites.forEach(item => {        
          item.expand = false;                      
      });
    }, (err) => {
      console.log(err);
    }
    );
    
  }
  // deleteSite(siteId,siteName){
  //   this.myhttp.getSitesData().subscribe((data) => {          
  //     this.sites = data.body.filter(item => (item.siteId==siteId && item.siteName==siteName));
  //   }, (err) => {
  //     console.log(err);
  //   }
  //   );
    
  // }
 
  deleteSiteInfo(siteId,siteName) {
    let obj = {
      siteId: siteId,
      siteName: siteName
    }

    this.myhttp.deleteSiteInfo(obj)
      .subscribe((data) => {
        console.log("delete data uis ", data);
        this.getSitesData();
      }, (err) => {
        console.log(err);
      })
  }

  activateSite(id,siteName) {
    var obj = {siteId:id,siteName:siteName};
    let comp = this;
    this.myhttp.activateSite(obj)
      .subscribe((data) => {
        this.getSitesData();
      }, (err) => {
        console.log(err);
      }
      );

  }

  

}
