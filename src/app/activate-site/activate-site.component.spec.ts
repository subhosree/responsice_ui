import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivateSiteComponent } from './activate-site.component';

describe('ActivateSiteComponent', () => {
  let component: ActivateSiteComponent;
  let fixture: ComponentFixture<ActivateSiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivateSiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivateSiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
