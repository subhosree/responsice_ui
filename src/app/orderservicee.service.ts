import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { baseUrl } from './dataInterface';

import { Observable } from 'rxjs/Observable';
const httpOptions = {
  headers: new HttpHeaders({
   'Content-Type': 'application/json',
   
  })
};

@Injectable()
export class OrderserviceeService {


  constructor(private http: HttpClient ) { } 

  
  baseUrl = baseUrl.baseUrl;
  //baseUrl = '/api/usecaseui-server/v1';
  url = {
    sotnSite:this.baseUrl + "/uui-lcm/Sotnservices",
    site: this.baseUrl + "/service/site",    
    postOrderService: this.baseUrl + "/service/orderService",
    finalPostOrderServiceData: this.baseUrl + "/sdwan/finalPostOrderedServiceData",
    getOrderConfigureVpn: this.baseUrl + "/order/vpn",
    getOrderSiteData: this.baseUrl + "order/sites",
    ms_siteData: this.baseUrl + "/ccvpn/sites",
    ms_vpnData: this.baseUrl + "/ccvpn/vpn",
    ms_vasData: this.baseUrl + "/ccvpn/vas",
    ms_bandwidthData: this.baseUrl + "/ccvpn/bandwidth",
    modelData: "/uui-modelservice/modelData",
    ms_bandwidthPost: this.baseUrl + "bandwidth/editBandwidthData",
    ms_postVasData: this.baseUrl + "vas/postVas",
    ms_bandwidthPut: this.baseUrl + "bandwidth/editBandwidthData",
    ms_camData: this.baseUrl + "/ccvpn/camera", 
    ms_sotOrderInfo:this.baseUrl+"/sotn/orderInfo",
    ms_subscribeType:this.baseUrl+"/uui-lcm/customers/service-subscriptions",
   
    ms_summaryData: this.baseUrl + "/service/summary",
    ms_serviceList: this.baseUrl + "/ccvpncustomer/services",
    ms_vpninfodata: this.baseUrl + "/ccvpn/vpnInfo",
   // ms_resourceTopology: this.baseUrl + "/service/resourceTopology",
    ms_resourceTopology: this.baseUrl + "/sdwan/resourceTopology5G",


    ms_subscriptionInstance:this.baseUrl+"/uui-lcm/Sotnservices/ServiceInstances/",

     ms_sotn_summaryData: this.baseUrl + "/uui-lcm/Sotnservices/servicesubscription/",
     ms_sotn_cost_summaryData: this.baseUrl + "/uui-lcm/Sotnservices/cost",
     ms_sotn_instantiation_status: this.baseUrl + "/uui-lcm/Sotnservices/serviceStatus/service-instance/",
     ms_sotn_resourceTopology: this.baseUrl + "/uui-lcm/Sotnservices/resourceTopology/service/service-subscriptions/service-subscription/",
     ms_sotn_delete:this.baseUrl + "/uui-lcm/Sotnservices/servicesubscription/"
    
  }
  deleteServiceInstance(serviceSubscriptionType,serviceInstance)
  {
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Basic SW5mcmFQb3J0YWxDbGllbnQ6cGFzc3dvcmQxJA=='
      }),
      body: {}
    };
    return this.http.delete<any>(this.url["ms_sotn_delete"]+serviceSubscriptionType+'/serviceinstance/'+serviceInstance,{});
  }

  getSiteData(requestBody) {
    let body = JSON.stringify(requestBody);
    return this.http.post<any>(this.url["site"],body,httpOptions);
  }

  postSotnData(requestBody) {
    let body = JSON.stringify(requestBody);
    return this.http.post<any>(this.url["sotnSite"],body,httpOptions);
  }


  getServiceInstantiationStatus(serviceId):Observable<any>{
    return this.http.get<any>(this.url.ms_sotn_instantiation_status+serviceId, {});
  }

  postOrderServiceData(requestBody){
    let body = JSON.stringify(requestBody);
    return this.http.post<any>(this.url["postOrderService"],body,httpOptions);
  }

  finalPostOrderServiceData(requestBody){
    let body = JSON.stringify(requestBody);
    return this.http.post<any>(this.url["finalPostOrderServiceData"],body,httpOptions);
  }  
  
  getSotnOrderInfo(){
    return this.http.get<summary>(this.url.ms_sotOrderInfo, {});
  }
  
  getSummaryData():Observable<any>{
    return this.http.get<any>(this.url.ms_summaryData, {});
  }

  getSotnSummaryData(subscriptionType,instanceId):Observable<summary>{
    return this.http.get<summary>(this.url.ms_sotn_summaryData+subscriptionType+'/serviceinstance/'+instanceId, {});
  }

  postSotnDataToGetCost(requestBody) {
    let body = JSON.stringify(requestBody);
    return this.http.post<any>(this.url["ms_sotn_cost_summaryData"],body,httpOptions);
  }
 
  getServiceList():Observable<serviceList>{
    return this.http.get<serviceList>(this.url.ms_serviceList, {});
  }
  
  getResourceTopology(subscriptionType , serviceInstace):Observable<summary>{
    if(subscriptionType == 'sdwan')
    {
      return this.http.get<summary>(this.url.ms_resourceTopology, {});
    }
    else
    {
      return this.http.get<summary>(this.url.ms_sotn_resourceTopology+subscriptionType+'/service-instances/service-instance/'+serviceInstace, {});
    }  
  }
  getSubscriptionType():Observable<any>{
    return this.http.get<any>(this.url.ms_subscribeType, {});
  }
  
  // getServiceInstance(subscriptionType):Observable<any>{
  //   let body = JSON.stringify(subscriptionType);
  //   return this.http.post<any>(this.url["ms_subscriptionInstance"],body,httpOptions);
  // }
  
  getServiceInstance(paramsObj) {
    //let params = new HttpParams({ fromObject: paramsObj });
    //console.log("post id to backend",paramsObj)
    return this.http.get<any>(this.url.ms_subscriptionInstance+paramsObj,{});
    //console.log("post id to backend",paramsObj)
  }
  getSelectedSitesData(servicetypeparam,serviceInstanceIdparam): Observable<any> {
  
    //let params = new HttpParams({ fromObject: serviceInstanceIdparam });
    //console.log("post id to backend",serviceInstanceIdparam)
    return this.http.get<any>(this.baseUrl+"/uui-lcm/Sotnservices/servicesubscription/"+ servicetypeparam +"/serviceinstance/"+serviceInstanceIdparam  )
    
    //console.log("post id to backend",paramsObj)
  }
  getSotnBandwidthData(servicetypeparam,serviceInstanceIdparam): Observable<any> {
   
    //let params = new HttpParams({ fromObject: serviceInstanceIdparam });
    //console.log("post id to backend",serviceInstanceIdparam)
    return this.http.get<any>(this.baseUrl+"/uui-lcm/Sotnservices/bandwidth/service-subscriptions/service-subscription/"+ servicetypeparam +"/service-instances/service-instance/"+serviceInstanceIdparam  )
    
    //console.log("post id to backend",paramsObj)
  }





 
 









   




}