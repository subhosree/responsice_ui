import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopologyDashboardComponent } from './topology-dashboard.component';

describe('TopologyDashboardComponent', () => {
  let component: TopologyDashboardComponent;
  let fixture: ComponentFixture<TopologyDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopologyDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopologyDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
