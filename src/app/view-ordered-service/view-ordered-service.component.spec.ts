import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewOrderedServiceComponent } from './view-ordered-service.component';

describe('ViewOrderedServiceComponent', () => {
  let component: ViewOrderedServiceComponent;
  let fixture: ComponentFixture<ViewOrderedServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewOrderedServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewOrderedServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
