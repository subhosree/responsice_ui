import { Component, OnInit } from '@angular/core';
import { OrderserviceeService } from './../orderservicee.service';


@Component({
  selector: 'app-view-ordered-service',
  templateUrl: './view-ordered-service.component.html',
  styleUrls: ['./view-ordered-service.component.less']
})
export class ViewOrderedServiceComponent implements OnInit {

  serviceSubscriptionList = [] as Array<any>;
  serviceInstanceList = [] as Array<any>;
  selectedSubscriptionType: string;
  selectedServiceInstance: string = "";

  constructor(private order: OrderserviceeService) { }

  ngOnInit() {
    this.getSubscribeTypes();
  }


  //Get SubscriptionType
  getSubscribeTypes() {
    this.order.getSubscriptionType().subscribe((data) => {
      this.serviceSubscriptionList = data.subscriptions;
    }, (err) => {
      console.log(err);
    });
  }

  //Get subscription instanceID by calling With Subscription Type
  getServiceInstanceList(subscriptionType) {
    this.serviceInstanceList = [];
    this.selectedServiceInstance="";
    this.order.getServiceInstance(subscriptionType).subscribe((data) => {
      this.serviceInstanceList = data.serviceInstanceList;      
    }, (err) => {
      console.log(err);
    });    
  }

  getSubscribedSites(selectedServiceInstance) {}

  deleteSelectedService()
  {
    this.order.deleteServiceInstance(this.selectedSubscriptionType, this.selectedServiceInstance).subscribe((data) => {
      //this.serviceSubscriptionList = [] ;
      this.serviceInstanceList = [];
      //this.selectedSubscriptionType = "";
      this.selectedServiceInstance = "";
      this.getServiceInstanceList(this.selectedSubscriptionType);
    }, (err) => {
      console.log(err);
    });
  }

}
