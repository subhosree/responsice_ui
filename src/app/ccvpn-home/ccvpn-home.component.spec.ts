import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcvpnHomeComponent } from './ccvpn-home.component';

describe('CcvpnHomeComponent', () => {
  let component: CcvpnHomeComponent;
  let fixture: ComponentFixture<CcvpnHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcvpnHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcvpnHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
