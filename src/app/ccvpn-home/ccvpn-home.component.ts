import { Component, OnInit, Input, Output, EventEmitter, HostBinding, ViewChild } from '@angular/core';
import { slideToRight } from '../animates';
import {Router} from "@angular/router";


@Component({
  selector: 'app-ccvpn-home',
  templateUrl: './ccvpn-home.component.html',
  styleUrls: ['./ccvpn-home.component.less'],
  animations: [slideToRight]
})
export class CcvpnHomeComponent implements OnInit {
  @HostBinding('@routerAnimate') routerAnimateState;

  @ViewChild('carousel') carousel: any;
  movies: Object[] = []
  slides: Array<Object> = []
  options: Object = {
    clicking: true,
    sourceProp: 'src',
    visible: 3,
    perspective: 1,
    startSlide: 0,
    border: 0,
    dir: 'ltr',
    width: 500,
    height: 300,
    space: 400,
    autoRotationSpeed: 2000000,
    loop: true
  };

  title: String = 'Cross Domain and Cross Layer Virtual Private Network';

  srcPath = {
    manageService: './assets/images/home/manageService.png',
    orderService: './assets/images/home/orderService.png',
    monitorService: './assets/images/home/monitorService.png',
    manageServiceTop: './assets/images/home/manageServiceTop.png',
    orderServiceTop: './assets/images/home/orderServiceTop.png',
    monitorServiceTop: './assets/images/home/monitorServiceTop.png',
    manageServiceHover: './assets/images/home/manageServiceHover.png',
    orderServiceHover: './assets/images/home/orderServiceHover.png',
    monitorServiceHover: './assets/images/home/monitorServiceHover.png',

  }
  constructor(private router: Router) { }

  ngOnInit() {
    this.slides.push({ src: this.srcPath.manageServiceTop });
    this.slides.push({ src: this.srcPath.orderService });
    this.slides.push({ src: this.srcPath.monitorService });
  }


  prevNextClick(sequence: String) {
    let idx = 0;
    if (sequence == "prev") {
      idx = this.carousel.carousel3d.currentIndex + 1;
      idx = idx > 2 ? 0 : idx;
    }
    else {
      switch (this.carousel.carousel3d.currentIndex) {
        case 2:
          idx = 1;
          break;
        case 1:
          idx = 0;
          break;
        case 0:
          idx = 2;
          break;
      }
    }
    this.slideClicked(idx);
  };


  slideClicked(index) {
    this.carousel.slideClicked(index);
    this.carousel.slides[0].src = this.srcPath.manageService;
    this.carousel.slides[1].src = this.srcPath.orderService;
    this.carousel.slides[2].src = this.srcPath.monitorService;
    switch (index) {
      case 0:
        this.carousel.slides[index].src = this.srcPath.manageServiceTop;
        break;
      case 1:
        this.carousel.slides[index].src = this.srcPath.orderServiceTop;
        break;
      case 2:
        this.carousel.slides[index].src = this.srcPath.monitorServiceTop;
        break;
    }
  }

  onMouseInOut(index, inout) {
    switch (index) {
      case 0:
        this.carousel.slides[index].src = inout == "in" ? this.srcPath.manageServiceHover :
          this.carousel.carousel3d.currentIndex == index ? this.srcPath.manageServiceTop : this.srcPath.manageService;
        break;
      case 1:
        this.carousel.slides[index].src = inout == "in" ? this.srcPath.orderServiceHover :
          this.carousel.carousel3d.currentIndex == index ? this.srcPath.orderServiceTop : this.srcPath.orderService;
        break;
      case 2:
        this.carousel.slides[index].src = inout == "in" ? this.srcPath.monitorServiceHover :
          this.carousel.carousel3d.currentIndex == index ? this.srcPath.monitorServiceTop : this.srcPath.monitorService;
        break;
    }
  }

  gotoPage(index) {
    switch (index) {
      case 0:
        document.getElementById('manageService').click();
        //this.router.navigate(['os/ordered-service']);               
        break;
      case 1:
        document.getElementById('orderService').click();
        //this.router.navigate(['orderService']); 
        break;
      case 2:
        document.getElementById('monitorService').click();
        //this.router.navigate(['ms/data-monitor']); 
        break;
    }      
  }

}
