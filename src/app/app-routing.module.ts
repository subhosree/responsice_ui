import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import { HomeComponent } from './home/home.component';
import { ServicesComponent } from './services/services.component';
import { ServicesListComponent } from './services/services-list/services-list.component';
import { OnboardVnfVmComponent } from './services/onboard-vnf-vm/onboard-vnf-vm.component';
import { AlarmComponent } from './alarm/alarm.component';
import { PerformanceComponent } from './performance/performance.component';
import { PerformanceVnfComponent } from './performance/performance-vnf/performance-vnf.component';
import { PerformanceVmComponent } from './performance/performance-vm/performance-vm.component';

import { CcvpnNetworkComponent } from './ccvpn-network/ccvpn-network.component';

// import { DetailsComponent } from './details/details.component';

import { CcvpnHomeComponent } from './ccvpn-home/ccvpn-home.component';
import { DashboardHomeComponent } from './dashboard-home/dashboard-home.component';
import { DataMonitorComponent } from './data-monitor/data-monitor.component';
import { DataMonitorCmccComponent } from './data-monitor-cmcc/data-monitor-cmcc.component';
import { OrderComponent } from './order/order.component';
import { OrderedServiceComponent } from './ordered-service/ordered-service.component';
import { BandwidthComponent } from './bandwidth/bandwidth.component';
import { VasComponent } from './vas/vas.component';
import { ModelComponent } from './model/model.component';
import { ProfileComponent } from './profile/profile.component';
import { CameraComponent } from './camera/camera.component';
import { CcvnLayoutComponent } from './_layout/ccvn-layout/ccvn-layout.component';
import { CcvnMobileLayoutComponent } from './_layout/ccvpn-mobile-layout/ccvn-mobile-layout.component';
import { MobileLayoutComponent } from './_layout/mobile-layout/mobile-layout.component';
import { AppLayoutComponent } from './_layout/app-layout/app-layout.component';
import { ConfigureVasComponent } from './configure-vas/configure-vas.component';
import { TopologyComponent } from './topology/topology.component';
import { ActivateSiteComponent } from './activate-site/activate-site.component';
import { TreeTopologyComponent } from './tree-topology/tree-topology.component';
import { SelectServiceComponent } from './select-service/select-service.component';
import { ConfigureSotnSiteComponent } from './configure-sotn-site/configure-sotn-site.component';
import { SotnOrderComponent } from './sotn-order/sotn-order.component';
//import {ConfiguresiteComponentservice} from './configuresite.1/configure-sotn-vpn.component';
import { SotnOrderServiceComponent } from './sotn-order-service/sotn-order-service.component';
import { ViewOrderedServiceComponent } from './view-ordered-service/view-ordered-service.component';
import { MobileCcvpnHomeComponent } from './mobile-ccvpn-home/mobile-ccvpn-home.component'
import { SiteInformationComponent } from './site-information/site-information.component';
import { DashboardSummaryComponent } from './dashboard-summary/dashboard-summary.component';

const ServicesChildRoutes: Routes = [
  { path: 'services-list', component: ServicesListComponent },
  { path: 'onboard-vnf-vm', component: OnboardVnfVmComponent },
  { path: '**', redirectTo: 'services-list' }
]

const routes: Routes = [
  {
    path: 'ccvpn',
    component: CcvnMobileLayoutComponent,
    children: [
      { path: '', component: DashboardHomeComponent, pathMatch: 'full' },
      { path: 'home', component: DashboardHomeComponent },
      { path: 'treeTopology', component: TreeTopologyComponent },
      { path: 'ms/data-monitor', component: DataMonitorComponent },
      { path: 'ms/topology', component: TopologyComponent },
      { path: 'os/orderService', component: OrderComponent },
      { path: 'os/orderedService', component: OrderedServiceComponent },
      { path: 'os/bandwidth', component: BandwidthComponent },
      { path: 'ms/vas', component: VasComponent },
      { path: 'ms/data-monitor-cmcc', component: DataMonitorCmccComponent },
      { path: 'ms/activateSite', component: ActivateSiteComponent },
      { path: 'ms/profile', component: ProfileComponent },
      { path: 'ms/camera', component: CameraComponent },
      { path: 'ms/configureVas', component: ConfigureVasComponent },
      { path: 'selectOrderservice', component: SelectServiceComponent },
      { path: 'os/sotn-orderService', component: SotnOrderComponent },
      { path: 'os/SotnOrder', component: SotnOrderServiceComponent },
      { path: 'os/viewOrderedService', component: ViewOrderedServiceComponent },
      { path: 'os/sitenformation', component: SiteInformationComponent },
      { path: 'os/dashboard', component: DashboardSummaryComponent }
    ]
  },
  {
    path: 'mobile',
    component: MobileLayoutComponent,
    children: [
      { path: '', component: DashboardHomeComponent, pathMatch: 'full' },
      { path: 'home', component: DashboardHomeComponent },
      { path: 'treeTopology', component: TreeTopologyComponent },
      { path: 'ms/data-monitor', component: DataMonitorComponent },
      { path: 'ms/topology', component: TopologyComponent },
      { path: 'os/orderService', component: OrderComponent },
      { path: 'os/orderedService', component: OrderedServiceComponent },
      { path: 'os/bandwidth', component: BandwidthComponent },
      { path: 'ms/vas', component: VasComponent },
      { path: 'ms/data-monitor-cmcc', component: DataMonitorCmccComponent },
      { path: 'ms/activateSite', component: ActivateSiteComponent },
      { path: 'ms/profile', component: ProfileComponent },
      { path: 'ms/camera', component: CameraComponent },
      { path: 'ms/configureVas', component: ConfigureVasComponent },
      { path: 'selectOrderservice', component: SelectServiceComponent },
      { path: 'os/sotn-orderService', component: SotnOrderComponent },
      { path: 'os/SotnOrder', component: SotnOrderServiceComponent },
      { path: 'os/viewOrderedService', component: ViewOrderedServiceComponent }
    ]
  },
  {
    path: 'appHome',
    component: AppLayoutComponent,
    children: [
      { path: 'home', component: HomeComponent },
      // { path: 'services', component: ServicesComponent, children:ServicesChildRoutes}, //暂时不是子路由结构
      { path: 'services/services-list', component: ServicesListComponent },
      { path: 'services/onboard-vnf-vm', component: OnboardVnfVmComponent },
      { path: 'alarm', component: AlarmComponent },
      { path: 'performance', component: PerformanceComponent },
      { path: 'performance/performance-vnf', component: PerformanceVnfComponent },
      { path: 'performance/performance-vm', component: PerformanceVmComponent },
      { path: 'network', component: CcvpnNetworkComponent },
      { path: '**', redirectTo: 'home', pathMatch: 'full' }
    ]
  },
  { path: 'ms/data-monitor-cmcc', component: DataMonitorCmccComponent },
  { path: '**', redirectTo: 'appHome/home', pathMatch: 'full' }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }