import { Component, OnInit, OnDestroy } from '@angular/core';
import { Network, DataSet, Node, Edge, IdType } from 'vis';
import { MyhttpService } from '../myhttp.service';
import { OrderserviceeService } from './../orderservicee.service';

@Component({
    selector: 'app-tree-topology',
    templateUrl: './tree-topology.component.html',
    styleUrls: ['./tree-topology.component.less']
})

export class TreeTopologyComponent implements OnInit, OnDestroy {
    title = 'Network';
    public nodes: Node;
    public edges: Edge;
    public network: Network;
    public serviceList: any;
    public tempNode: any;
    public tempEdge: any;
    public selectedNode: any;
    public selectedNodeIds: any;
    public x: any;
    public abc = [];
    serviceSubscriptionList = [] as Array<any>;
    serviceInstanceList = [] as Array<any>;
    selectedSubscriptionType: string;
    selectedServiceInstance: string = "";
    container: any;
    networkOptions = {

        layout: { 
            randomSeed: 15 
        
            // hierarchical: {
            //     // enabled: true,
            //     // levelSeparation: 80,
            //     // nodeSpacing: 50,
            //     // treeSpacing: 10,
            //     // blockShifting: true,
            //     // edgeMinimization: true,
            //     // parentCentralization: true,
            //     // direction: 'UD',
            //     },
        
        },
        nodes: {
            borderWidth: 13,
            size: 30,
            color: {
                border: '#54bd55',
                background: '#666666'
            },
            font: { color: '#eeeeee' }
        },
        edges: {
            color: 'lightgray'
        },

        interaction: {
            tooltipDelay: 200,
            hideEdgesOnDrag: true,
            navigationButtons: false,
            keyboard: true,
            hover: true
        },

    };


    constructor(private myhttp: MyhttpService, private orderService: OrderserviceeService) { }
    intervalData: any;
    returnResponse: boolean = true;

    //Get SubscriptionType
    getSubscribeTypes() {
        this.orderService.getSubscriptionType().subscribe((data) => {
            this.serviceSubscriptionList = data.subscriptions;
        }, (err) => {
            console.log(err);
        });
    }

    //Get subscription instanceID by calling With Subscription Type
    getServiceInstanceList(subscriptionType) {
        this.serviceInstanceList = [];
        this.selectedServiceInstance = "";


        this.orderService.getServiceInstance(subscriptionType).subscribe((data) => {
            this.serviceInstanceList = data.serviceInstanceList;
        }, (err) => {
            console.log(err);
        });
    }
    ngOnInit() {
        this.container = document.getElementById('mynetwork');
        this.getSubscribeTypes();
    }

    refreshData() {

        var data1 = {
            nodes: this.serviceList.nodes,
            edges: this.serviceList.edges
        };

        var network = new Network(this.container, data1, this.networkOptions);


        network.on('select', function (selection) {
            this.selectedNodeIds = selection.nodes[0]; // array of selected node's ids
            var filteredNode = data1.nodes.filter(item => (
                item.id == this.selectedNodeIds
            ));
            var t1 = '<div class="tblDiv">\
            <nz-form-label class="lblCls">Node Information</nz-form-label>\
            <table class="table table-striped table-bordered">\
                <thead>\
                    <tr>\
                        <th class="clr-primary padding-2p">Specification</th>\
                        <th class="clr-primary padding-2p">Value</th>\
                    </tr>\
                </thead>\
                <tbody>\
            ';
            Object.entries(filteredNode[0].dataNode).forEach(entry => {                
               if( entry[1] !== "null")
               {
                    t1 += '<tr class="popup-table-row">\
                        <td class="popup-table-header clr-primary padding-2p">'+ entry[0] + ':</td>\
                        <td class="popup-table-data  clr-primary padding-2p">'+ entry[1] + '</td>\
                    </tr>\
                    ';
               }    
            });
            t1 += '</tbody>\
            </table>\
            </div>\
            ';
            document.getElementById('nodeDetails').innerHTML = t1;
        });
    }

    getData ()
    {
        var comp = this;
        console.log("getdata");
        this.orderService.getResourceTopology(this.selectedSubscriptionType.toLowerCase(), this.selectedServiceInstance).subscribe((data) => {
            this.serviceList = data;
            comp.refreshData();
        }, (err) => {
            console.log(err);
        });
    }
    // Getting sitedata Based On Type and ID
    getSelectedsubscriptionInfo(s) {       
        this.getData();
        if (this.intervalData) {
            clearInterval(this.intervalData);
        }        
        this.intervalData = setInterval(() => {
            this.getData(); 
        }, 5000);
    }

    ngOnDestroy() {
        console.log('clear interval');
        if (this.intervalData) {
            clearInterval(this.intervalData);
        }

    }

    ngOnDelete() {
        console.log('clear interval');
        if (this.intervalData) {
            clearInterval(this.intervalData);
        }
    }

}
