import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreeTopologyComponent } from './tree-topology.component';

describe('TreeTopologyComponent', () => {
  let component: TreeTopologyComponent;
  let fixture: ComponentFixture<TreeTopologyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreeTopologyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeTopologyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
