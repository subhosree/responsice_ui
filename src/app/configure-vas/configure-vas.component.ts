import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-configure-vas',
  templateUrl: './configure-vas.component.html',
  styleUrls: ['./configure-vas.component.less']
})
export class ConfigureVasComponent implements OnInit {

  tabs = [
    {
      id: 1,
      name: 'Camera Information',
      disabled: false
    },
    {
      id: 2,
      name: 'VIP Information',
      disabled: false
    },
    {
      id: 3,
      name: 'Voice Information',
      disabled: true
    }
  ];
  constructor() { }

  ngOnInit() {
  }

}
