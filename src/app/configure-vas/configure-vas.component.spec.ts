import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigureVasComponent } from './configure-vas.component';

describe('ConfigureVasComponent', () => {
  let component: ConfigureVasComponent;
  let fixture: ComponentFixture<ConfigureVasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigureVasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigureVasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
