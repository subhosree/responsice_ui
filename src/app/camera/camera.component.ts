import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import { MyhttpService } from '../myhttp.service';


@Component({
  selector: 'app-camera',
  templateUrl: './camera.component.html',
  styleUrls: ['./camera.component.less']
})


export class CameraComponent implements OnInit {

  VpnInfoData = [] as Array<vpn>;
  editCache = {};
  isVisibleMiddle:boolean = false;

  constructor(private myhttp: MyhttpService) { }
  ngOnInit(): void {
//     this.VpnInfoData = [{
//       "vpnName": "Default Vpn",
//       "vpnId": "1",
//       "vpnType": "Hub-Spoke",
//       "vpnBandwidth": "100",
//       "vpnThreshold": "10",
//       "cameras":"3",      
//       "sites": [{
//           "siteId": "s1",
//           "description": "xxxxx",
//           "siteName": "sitename1",
//           "isCloudSite": "1",
//           "location": "Bangalore",
//           "zipCode": "500001",
//           "role": "",
//           "capacity": "12",
//           "siteinterface": "123",
//           "subnet": "1.1.1.1",
//           "price": "200",
//           "checked":false,
//           "expand":false,                           
//           "cameras": [{                                
//               "cameraId": "c1",
//               "cameraName": 'cam1',
//               "cameraLocation": 'Bangalore',
//               "cameraIP": '10.20.30.40',
//               "cameraSite": 'Bangalore'
//           },
//           {                                
//               "cameraId": "c2",
//               "cameraName": 'cam2',
//               "cameraLocation": 'Bangalore',
//               "cameraIP": '10.20.30.40',
//               "cameraSite": 'Bangalore'
//           }
//       ]
//       },
//       {
//           "siteId": "s2",
//           "description": "xxxxx",
//           "siteName": "sitename2",
//           "isCloudSite": "2",
//           "location": "Bangalore",
//           "zipCode": "500001",
//           "role": "",
//           "capacity": "12",
//           "siteinterface": "123",
//           "subnet": "1.1.1.1",
//           "price": "200", 
//           "checked":false,
//           "expand":false,                            
//           "cameras": [{                                
//               "cameraId": "c4",
//               "cameraName": 'cam4',
//               "cameraLocation": 'Bangalore',
//               "cameraIP": '10.20.30.40',
//               "cameraSite": 'Bangalore'
//           },
//           {                                
//               "cameraId": "c5",
//               "cameraName": 'cam5',
//               "cameraLocation": 'Bangalore',
//               "cameraIP": '10.20.30.40',
//               "cameraSite": 'Bangalore'
//           }
//       ]
//       },
//   ]
//   } 
// ];
    this.updateEditCache();
    this.getVpnInfoData();
  }

  startEdit(key: string): void {
    this.editCache[key].edit = true;
  }
  cancelEdit(key: string): void {
    this.editCache[key].edit = false;
  }
  saveEdit(key: string): void {
    var vpnId,siteId,cameraId;
    for (var i = 0; i < this.VpnInfoData.length; i++) {
      for (var j = 0; j < this.VpnInfoData[i].sites.length; j++) {
        for (var k = 0; k < this.VpnInfoData[i].sites[j].cameras.length; k++) {
          if (this.VpnInfoData[i].sites[j].cameras[k].cameraId == key) {
            this.VpnInfoData[i].sites[j].cameras[k] = { ...this.editCache[key].data };
            vpnId = this.VpnInfoData[i].vpnId;
            siteId = this.VpnInfoData[i].sites[j].siteId;
            cameraId = this.VpnInfoData[i].sites[j].cameras[k].cameraId;
            break;
          }
        }
      }
    }
    this.editCache[key].edit = false;
    this.updateCameraData(vpnId,siteId,cameraId,this.editCache[key].data);
  }

  updateEditCache(): void {
    for (var i = 0; i < this.VpnInfoData.length; i++) {
      for (var j = 0; j < this.VpnInfoData[i].sites.length; j++) {
        for (var k = 0; k < this.VpnInfoData[i].sites[j].cameras.length; k++) {
          if (!this.editCache[this.VpnInfoData[i].sites[j].cameras[k].cameraId]) {
            this.editCache[this.VpnInfoData[i].sites[j].cameras[k].cameraId] = {
              edit: false,
              data: { ...this.VpnInfoData[i].sites[j].cameras[k] }
            };
          }
        }
      }
    }
  }

  getVpnInfoData() {
    this.myhttp.getVPNInfoData().subscribe((data) => {
      this.VpnInfoData = data.body.vpnInformations;
      this.updateEditCache();
    })
  }

  updateCameraData(vpnId,siteId,cameraId,cameraData) {
    var obj = {
       vpnId: vpnId,
       siteId: siteId,
       cameraId: cameraId
    };    
    this.myhttp.updateCameraData(obj,cameraData)
      .subscribe((data) => {
        // pass
      }, (err) => {
        console.log(err);
      }
      );
  }


  viewSite()
  {
    this.isVisibleMiddle = true;
  }

  close()
  {
    this.isVisibleMiddle = false;
  }







}