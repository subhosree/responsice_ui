import { Theme } from './symbols';

export const darkTheme: Theme = {
  name: 'dark',
  properties: {
    '--primary': '#000930',
    '--menu-primary': '#0b2b4c',
    '--on-primary': '#00fffc',
    '--tablecolor': '#000930',
    '--th-background':'#303751',
    '--btn-color': '#fff',
    '--btn-bk-color': '#1890ff',
    '--btn-border-color': '#1890ff',
    '--stepper-item-finish':'rgba(148, 204, 92, 1)',
    '--on-disabled-button-bgcolor': '#57595b',
    '--on-disabled-button-fntcolor': '#000000',
    '--on-pagination-bgcolor': '#031466',
    '--on-lables': '#00fffc',
    '--on-disabled-font':' #b7e2eca1',
    '--check-bk':'#000930',
        '--icon-color': '#00fffc',
    '--disabled-tab-color': '#c5dadd',
    '--collapse-color-info' : '#485F77',
    '--collapse-header': '#00fffc',
    '--dashboard-color-out': '#004D72',
    '--loop-bottom-header-color': 'white',
    '--portal-loop-bottom': '#010717',
    '--portal-loop-main-mobile': '#00BFFF',
    '--modal-background': 'linear-gradient(#006666, #336699)',
    '--primary_pagination': '#006666',
    '--modal-header-color':'#004d4d',
    '--menu-border' : '#00b8e6'
  }
};