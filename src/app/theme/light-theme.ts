import { Theme } from './symbols';

export const lightTheme: Theme = {
  name: 'light',
  properties: {
    '--primary': '#fff',
    '--tablecolor': '#fff',
    '--on-primary': '#333333',
    '--th-background':'#F7F8FC',
    '--btn-color': '#fff',
    '--btn-bk-color': '#1890ff',
	
'--on-lables': '#000000', 
    '--on-disabled-font': ' #515c6d',
    '--btn-border-color': '#1890ff',
    '--stepper-item-finish': 'rgba(148, 204, 92, 1)',
    '--on-disabled-button-bgcolor': '#c3c6cc',
    '--on---on-disabled-button-fntcolor': ' #edf0f7',
    '--on-pagination-bgcolor': '#fff',
    '--check-bk':'#1890ff',
    '--icon-color': 'none',
    '--disabled-tab-color': '#989ba0',
    '--collapse-color-info': '#f2f2f2',
    '--collapse-header': '#333333',
    '--dashboard-color-out': '#ffffff',
    '--loop-bottom-header-color': 'black',
    '--portal-loop-bottom': 'white',
    '--portal-loop-main-mobile': '#0e6496',
    '--modal-background': 'linear-gradient(#white, #336699)',
    '--primary_pagination': 'white',
    '--modal-header-color':'#33cccc',
    '--menu-border' : 'white'
  }
};