import { Component, OnInit, Input } from '@angular/core';
import { MyhttpService } from '../myhttp.service';
import { OrderserviceeService } from './../orderservicee.service';

import { orderService } from './../Order.service'

import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';


@Component({
  selector: 'app-select-subscription',
  templateUrl: './select-subscription.component.html',
  styleUrls: ['./select-subscription.component.less']
})
export class SelectSubscriptionComponent implements OnInit {

  Orders: String[] = ["SD-WAN", "SOTN"];
  InstanceID: String[] = ["SD-WAN", "SOTN"]
 
  @Input() orderServiceData;
  expandDataSet = [
    { rowIdx: 1, name: 'siteInformation', quantity: 'Quantity', prize: 'Price ($ K)', expand: true },
    { rowIdx: 2, name: 'vpnInformation', quantity: 'Quantity', prize: 'Price ($ K)', expand: true },
    { rowIdx: 3, name: 'bandWidthPolicy', quantity: 'Quantity', prize: 'Price ($ K)', expand: true },
    { rowIdx: 4, name: 'vasInformation', quantity: 'Quantity', prize: 'Price ($ K)', expand: true },

  ];

  summaryInfo={} as summary;
  serviceList={} as serviceList;

  sotnOrderInfo:any
  


  camerasLength:number=0;
  camerasCost:number=0;
  bandWidthLength:number=0;
  bandWidthCost:number=0;
  vpnDataLength:number=0;
  vpnDataCost:number=0;
  siteDataLength:number=0;
  siteDataCost:number=0;
  expandTrue:boolean=true;
  wlanAccess:string="";
  orderedServiceDetails:boolean = false;
  listOfParentData: any[] = [];
  listOfChildrenData: any[] = [];
  OrderData:orderService[];

  instanceId:string = "";
  selectedOrder:any;

  mapped:any;

  SOTNinstanceId = ["500","501"]

  serviceType:any

  ////////
  


  constructor(private myhttp: OrderserviceeService , private OrderType: HttpClient) { }
  


  getSelectedOrder(order:any){
    this.selectedOrder = order;
        if(this.selectedOrder == "600"){
            this.instanceId = "600";
          console.log('orderId', order)
        }
        else if(this.selectedOrder == "601"){
            this.instanceId = "601";
            
        }
        else if(this.selectedOrder == "500"){
          this.instanceId = "500";
        }
        else if(this.selectedOrder == "501"){
          this.instanceId = "501";
      }
    }


    getInstanceIdservice(type){

      if(type == "SD-WAN"){
        this.serviceType = type;
        this.myhttp.getServiceList().subscribe((data)=>{     
          this.serviceList = data;
          console.log("service list info load  ====>", this.serviceList,this.serviceType);     
        }, (err) => {
          console.log(err);
        });
      } 
      else if(type == "SOTN"){
        this.serviceType = type;
        this.SOTNinstanceId
        console.log("hhhh ====>", this.SOTNinstanceId);
      }


    }

    getSotnOrderserviceInfo(){
      this.myhttp.getSotnOrderInfo().subscribe((data)=>{     
        this.sotnOrderInfo = data;
        this.mapped = Object.entries(this.sotnOrderInfo.service).map(([type, value]) => ({type, value}));
        console.log("sotn arr  ====>", this.mapped);     
        console.log("sotn info load  ====>", this.sotnOrderInfo);     
      }, (err) => {
        console.log(err);
      });
    }
    


  

  // getOrderDetails(){
  //   this.OrderType.get("http://localhost:3000/Mobiles").
  //   subscribe((data) => console.log( "orderType", data))

  //  }
  //  displaydata(data) {this.OrderData = data;}

   
  
  ngOnInit() {
    //this.instanceId = "id"

    this.getSotnOrderserviceInfo();

   this.OrderData = [
      {serviceInstanceId:"1", serviceType:"SD-WAN"},
      {serviceInstanceId:"2", serviceType:"SOTN"},
   ];


    if(this.orderServiceData && this.orderServiceData.numOfCamera)
    {
      this.orderedServiceDetails = true;
      this.postOrderServiceData();      
    }
    else
    {
      this.orderedServiceDetails = false;
      this.loadSummaryData();
      
    }
  
  }


  mapPostObject():any{
    let obj = {} as postObjInterface;
    obj.wlanAccess = this.orderServiceData.wanAccess;
    obj.bandWidth = this.orderServiceData.serviceBandwidth;
    obj.vpnInformations = [];

    let vpnObj;
    for(var i=0;i<this.orderServiceData.vpnArray.length;i++)
    {
      vpnObj = this.orderServiceData.vpnArray[i];
      delete vpnObj.expand;
      vpnObj.cameras = this.orderServiceData.numOfCamera;

      for(var j=0;j<this.orderServiceData.orderBandwidth.length;j++)
      {
        if(this.orderServiceData.orderBandwidth[j].vpnId == vpnObj.vpnId)
        {
          vpnObj.vpnBandwidth = this.orderServiceData.orderBandwidth[j].vpnBandwidth;       

        }      
      }
      obj.vpnInformations.push(vpnObj);
    }
    return obj;
  }

  postOrderServiceData(): void {
    let finalObj = this.mapPostObject();
    this.myhttp.postOrderServiceData(finalObj)
        .subscribe((data)=> {
          this.summaryInfo = data;
          console.log("summary info post ====>", this.summaryInfo);
          console.log("post order service bean ====>", finalObj);
          this.wlanAccess = this.summaryInfo.wlanAccess.join(",");
          this.getCameraData(this.summaryInfo);
          this.getBandwidthData(this.summaryInfo);
          this.getVpnData(this.summaryInfo);
          this.getSiteData(this.summaryInfo);
          console.log("post");
        }, (err) => {
          console.log(err);
        }
      )
  }

  loadSummaryData(){

    // for (let i = 0; i < 3; ++i) {
    //   this.listOfParentData.push({
    //     key: i,
    //     "serviceInstanceId": "600",
    //     "serviceInstanceName": "600",
    //     "serviceType": "example-service-type-val-52265",
    //     "serviceRole": "example-service-role-val-24156",
    //     "resourceVersion": "1556088536117",
    //     expand: false
    //   });
    // }
    // this.myhttp.getServiceList().subscribe((data)=>{     
    //   this.serviceList = data;
    //   console.log("service list info load  ====>", this.serviceList);     
    // }, (err) => {
    //   console.log(err);
    // });
    
    this.myhttp.getSummaryData().subscribe((data)=>{     
      this.summaryInfo = data;
      console.log("summary info load  ====>", this.summaryInfo);
      this.wlanAccess= this.summaryInfo.wlanAccess.join(",");     
      this.getCameraData(this.summaryInfo);
      this.getBandwidthData(this.summaryInfo);
      this.getVpnData(this.summaryInfo);
      this.getSiteData(this.summaryInfo);      
    }, (err) => {
      console.log(err);
    });
  }
 
  getCameraData(summaryInfoData){
    if(summaryInfoData && summaryInfoData.vpnInformations &&  summaryInfoData.vpnInformations.length)
    {
      this.camerasLength = Number(summaryInfoData.vpnInformations[0].cameras);     
    } 
    this.camerasCost = summaryInfoData.cameraCost;
  }

  getBandwidthData(summaryInfoData){
    this.bandWidthLength=summaryInfoData.vpnInformations.length;
    this.bandWidthCost = summaryInfoData.bandwidthCost;
  }

  getVpnData(summaryInfoData){
    this.vpnDataLength=summaryInfoData.vpnInformations.length;
    this.vpnDataCost = summaryInfoData.vpnCost;
  }

  getSiteData(summaryInfoData){
    this.siteDataLength = summaryInfoData.vpnInformations[0].sites.length;
    var cost = 0;
    for(var i=0;i< summaryInfoData.vpnInformations[0].sites.length;i++)
    {
      cost+=Number(summaryInfoData.vpnInformations[0].sites[i].price);
      
    }
    console.log(summaryInfoData.vpnInformations[0].sites)
    this.siteDataCost = cost;           
  }


}
