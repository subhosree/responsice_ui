import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigureSotnSiteComponent } from './configure-sotn-site.component';

describe('ConfigureSotnSiteComponent', () => {
  let component: ConfigureSotnSiteComponent;
  let fixture: ComponentFixture<ConfigureSotnSiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigureSotnSiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigureSotnSiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
