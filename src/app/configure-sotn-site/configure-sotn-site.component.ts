import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import { OrderserviceeService } from './../orderservicee.service';
import { MyhttpService } from '../myhttp.service';
@Component({
  selector: 'app-configure-sotn-site',
  templateUrl: './configure-sotn-site.component.html',
  styleUrls: ['./configure-sotn-site.component.less']
})
export class ConfigureSotnSiteComponent implements OnInit {

  sotnService = [];

  sotnProducts = [] as Array<sotnSites>;
  @Input() orderServiceData;
  siteTitle = "Add Site Data";

  siteData = {} as sotnSites;
  tempData = {} as sotnSites;
 
  listOfSearchName = [];
  searchAddress: string;
  isVisible = false;
  isMobile:boolean = false;

  validateForm: FormGroup;
  constructor(private fb: FormBuilder, private myhttp: OrderserviceeService, private myhttp2: MyhttpService) {

  }

  ngOnInit() {
   this.isMobile = screen.width < 575 ? true : false;

    this.sotnService = this.orderServiceData && this.orderServiceData.service ? this.orderServiceData.service : {};    
    this.validateForm = this.fb.group({
      sotnSiteName: [null, [Validators.required]],
      address: [null, [Validators.required]],
      description: [null, [Validators.required]],
      vlan: [null, [Validators.required]],
      zipCode: [null, [Validators.required]],
    });

  }

  showModalMiddle(): void {
    this.isVisible = true;
    this.validateForm = this.fb.group({
      sotnSiteName: [null, [Validators.required]],
      address: [null, [Validators.required]],
      vlan: [null, [Validators.required]],
      description: [null, [Validators.required]],
      zipCode: [null, [Validators.required]],
    });
  }

  handleOk(): void {
    this.addSiteData();
    this.isVisible = false;
  }

  handleCancel(): void {
    this.isVisible = false;
  }

  addSiteData() {
    this.sotnProducts.push({ ...this.siteData });
    this.sotnProducts = this.sotnProducts.splice(0, this.sotnProducts.length);
    this.orderServiceData.sites = this.sotnProducts; 
  }

  deleteSite(idx){   
  
    this.sotnProducts.splice(idx, 1);
    this.sotnProducts = this.sotnProducts.splice(0, this.sotnProducts.length);
    this.orderServiceData.sites = this.sotnProducts;
  }









}
