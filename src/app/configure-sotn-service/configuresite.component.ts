import { Component, OnInit,  Input } from '@angular/core';
import {
  FormBuilder,  
  FormGroup,
  Validators
} from '@angular/forms';
import { OrderserviceeService } from './../orderservicee.service';
import { MyhttpService } from '../myhttp.service';

@Component({
  selector: 'app-configure-sotn-service',
  templateUrl: './configuresite.component.html',
  styleUrls: ['./configuresite.component.less']
})
export class ConfiguresiteComponentservice implements OnInit {
  
  @Input() orderServiceData;
  @Input() checkAllValue:boolean=false;
  isVpnVisible = false;
  isMobile:boolean = false;

  siteData = {} as sotnService;
  tempData= {} as sotnService;
  validateForm: FormGroup;

  constructor(private fb: FormBuilder, private myhttp: OrderserviceeService, private myhttp2: MyhttpService) {
  }

  ngOnInit() { 
    this.isMobile = screen.width < 575 ? true : false;
    
    this.validateForm = this.fb.group({
      serviceName:[null, [Validators.required]],
      description:[null, [Validators.required]],
      highAvailability:[null, [Validators.required]],
      cos:[null, [Validators.required]],
      cir:[null, [Validators.required]],
      eir:[null, [Validators.required]],
      serviceClassificationType:[null, [Validators.required]],
      SLAType:[null, [Validators.required]],
    });
  }


 
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        }
    return true;
  }

 

}