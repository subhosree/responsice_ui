import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { NZ_I18N, en_US } from 'ng-zorro-antd';
import { NgxEchartsModule } from 'ngx-echarts';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient, './assets/i18n/', '.json');
}

import { AppRoutingModule } from './app-routing.module';

//注册语言包
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
registerLocaleData(en);

//自定义组件
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ServicesComponent } from './services/services.component';
import { ServicesListComponent } from './services/services-list/services-list.component';
import { OnboardVnfVmComponent } from './services/onboard-vnf-vm/onboard-vnf-vm.component';
import { AlarmComponent } from './alarm/alarm.component';
import { PerformanceComponent } from './performance/performance.component';
import { PerformanceVnfComponent } from './performance/performance-vnf/performance-vnf.component';
import { PerformanceVmComponent } from './performance/performance-vm/performance-vm.component';
import { CcvpnNetworkComponent } from './ccvpn-network/ccvpn-network.component';
import { CcvpnDetailComponent } from './ccvpn-detail/ccvpn-detail.component';
import { CcvpnCreationComponent } from './ccvpn-creation/ccvpn-creation.component';

import { DetailsComponent } from './components/details/details.component';
import { GraphiclistComponent } from './components/graphiclist/graphiclist.component';
import { E2eCreationComponent } from './components/e2e-creation/e2e-creation.component';

import { BarComponent } from './components/charts/bar/bar.component';
import { LineComponent } from './components/charts/line/line.component';
import { PieComponent } from './components/charts/pie/pie.component';
import { OrderComponent } from './order/order.component';
import { BandwidthComponent } from './bandwidth/bandwidth.component';
import { VasComponent } from './vas/vas.component';


import { PathLocationStrategy, LocationStrategy, HashLocationStrategy } from '@angular/common';
// 自定义服务
import { MyhttpService } from './myhttp.service';
import { HomesService } from './homes.service';
import { onboardService } from './onboard.service';
import { networkHttpservice } from './networkHttpservice.service';
import { PerformanceDetailsComponent } from './components/performance-details/performance-details.component';
import { CcvpnHomeComponent } from './ccvpn-home/ccvpn-home.component';

import { TimeLineComponent } from './components/charts/time-line/time-line.component';
import { TopologyComponent } from './topology/topology.component';


import { DataMonitorComponent } from './data-monitor/data-monitor.component';
import { DataMonitorCmccComponent } from './data-monitor-cmcc/data-monitor-cmcc.component';
import { OrderedServiceComponent } from './ordered-service/ordered-service.component'
import { CameraComponent } from './camera/camera.component';
import { OrderserviceeService } from './orderservicee.service';

import { NgxCarousel3dModule } from './components/ngx-carousel-3d/ngx-carousel-3d.module';

import { fakeBackendProvider } from '../helper/FakeBackendInterceptor';
import { ThemeModule, lightTheme, darkTheme } from './theme';
import { ModelComponent } from './model/model.component';
import { UploadFileService } from './profile/upload-file.service';
import { ProfileComponent } from './profile/profile.component';
import { CcvnLayoutComponent } from './_layout/ccvn-layout/ccvn-layout.component';
import { MobileLayoutComponent } from './_layout/mobile-layout/mobile-layout.component';
import { CcvnMobileLayoutComponent } from './_layout/ccvpn-mobile-layout/ccvn-mobile-layout.component';

import { AppLayoutComponent } from './_layout/app-layout/app-layout.component';
import { ConfiguresiteComponent } from './configuresite/configuresite.component';
import { ConfigurevpnComponent } from './configurevpn/configurevpn.component';
import { ConfigureVasComponent } from './configure-vas/configure-vas.component';
import { ActivateSiteComponent } from './activate-site/activate-site.component';
import { TreeTopologyComponent } from './tree-topology/tree-topology.component';
import { SelectServiceComponent } from './select-service/select-service.component';
import { SotnOrderComponent } from './sotn-order/sotn-order.component';
import { ConfigureSotnSiteComponent } from './configure-sotn-site/configure-sotn-site.component';
import { ConfiguresiteComponentservice } from './configure-sotn-service/configuresite.component';
import { AssosiateSiteVpnComponent } from './assosiate-site-vpn/assosiate-site-vpn.component';
import { SotnOrderServiceComponent } from './sotn-order-service/sotn-order-service.component';
import { SelectSubscriptionComponent } from './select-subscription/select-subscription.component';
import { ViewOrderedServiceComponent } from './view-ordered-service/view-ordered-service.component';
import { MobileCcvpnHomeComponent } from './mobile-ccvpn-home/mobile-ccvpn-home.component';
//import { SiteInformationComponent } from './site-information/site-information.component';
import { DashboardHomeComponent } from './dashboard-home/dashboard-home.component';
import { SiteInformationComponent } from './site-information/site-information.component';
import { DashboardSummaryComponent } from './dashboard-summary/dashboard-summary.component';
import { TopologyDashboardComponent } from './topology-dashboard/topology-dashboard.component';
//import { ResourceTopologyDashboardComponent } from './resource-topology-dashboard/resource-topology-dashboard.component';




@NgModule({
  providers: [
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    { provide: NZ_I18N, useValue: en_US },
    MyhttpService,
    HomesService,
    onboardService,
    networkHttpservice,
    UploadFileService,
   fakeBackendProvider,
    OrderserviceeService
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    CcvpnHomeComponent,
    TimeLineComponent,
    ServicesComponent,
    ServicesListComponent,
    OnboardVnfVmComponent,
    AlarmComponent,
    PerformanceComponent,
    PerformanceVnfComponent,
    PerformanceVmComponent,
    DetailsComponent,
    PieComponent,
    LineComponent,
    BarComponent,
    GraphiclistComponent,
    E2eCreationComponent,

    CcvpnNetworkComponent,
    CcvpnDetailComponent,
    CcvpnCreationComponent,
    PerformanceDetailsComponent,
    OrderComponent,
    DataMonitorComponent,
    OrderedServiceComponent,    
    CameraComponent,
    VasComponent,
    BandwidthComponent,
    TopologyComponent,
    ModelComponent,
    ProfileComponent,
    CcvnLayoutComponent,
    MobileLayoutComponent,
    CcvnMobileLayoutComponent,
    AppLayoutComponent,
    ConfiguresiteComponent,
    ConfigurevpnComponent,
    ConfigureVasComponent,
    ActivateSiteComponent,
    TreeTopologyComponent,
    SelectServiceComponent,
    SotnOrderComponent,
    ConfiguresiteComponentservice,
    ConfigureSotnSiteComponent,
    DataMonitorCmccComponent,
    AssosiateSiteVpnComponent,
    SelectSubscriptionComponent,
     ViewOrderedServiceComponent,
    MobileCcvpnHomeComponent,
    //SiteInformationComponent,
    SotnOrderServiceComponent,
    DashboardSummaryComponent,
    TopologyDashboardComponent,
   // ResourceTopologyDashboardComponent,
    SiteInformationComponent,
    //ConfiguresiteComponentservice
    DashboardHomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxCarousel3dModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    ThemeModule.forRoot({
      themes: [lightTheme, darkTheme],
      active: 'light'
    }),
    BrowserAnimationsModule,
    NgZorroAntdModule.forRoot(),
    NgxEchartsModule,
    AppRoutingModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }