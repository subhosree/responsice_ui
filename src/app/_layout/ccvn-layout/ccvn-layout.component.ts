import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ThemeService } from './../../theme';

@Component({
  selector: 'app-ccvn-layout',
  templateUrl: './ccvn-layout.component.html',
  styleUrls: ['./ccvn-layout.component.less']
})
export class CcvnLayoutComponent {

  router: string;
 
  constructor(private translate: TranslateService, private themeService: ThemeService) {
    this.router = window.location.hash.replace('#', "");    
    //console.log(this.router);
    translate.addLangs(['en', 'zh']);
    translate.setDefaultLang('en');
    this.themeService.setTheme('light');
  }

  changeTheme(item) {
    this.themeService.setTheme(item);
    this.selectedTheme = item;
    
    
      var arr = document.getElementsByClassName('ant-select-dropdown-ccvpn');
      for(var i=0;i<arr.length;i++)
      {
        if(item == 'dark')
        {
          arr[i].className += ' ant-select-dropdown-dark';
        }
        else
        {
          arr[i].classList.remove("ant-select-dropdown-dark")
        }  
      }
    
  }

  itemToLink = {
    'DataMonitor': '/ccvpn/ms/data-monitor',
    'networkTopology': '/ccvpn/ms/topology',
    'resourceTopology': '/ccvpn/treeTopology',
    'OrderedService': '/ccvpn/os/viewOrderedService',
    'View Service Instantiation':'/ccvpn/os/serviceInstance',
    'ConfigureVAS': '/ccvpn/ms/configureVas',
    'ActivateSite': '/ccvpn/ms/activateSite',
    'pushModel': '/ccvpn/ms/model'
  };

  // 多语言
  Language: String[] = ["zh", "en"];
  Themes: String[] = ["light", "dark"];
  selectedTheme = "light";
  selectLanguage = "en";
  subMenu: string[] = ["home"];
  siderCollapse: boolean = true;
  siderWidth: Number = 0;
  menuHeader: String = "";
  iconCls: String = "";

  changeLanguage(item) {
    this.selectLanguage = item;
    this.translate.use(item);
  }

  changeSubMenu(item) {
    switch (item) {
      case "home":
      case "orderService":
        this.menuHeader = "";
        this.iconCls = "";
        this.subMenu = [];
        this.siderCollapse = true;
        this.siderWidth = 0;
        break;
      case "manageService":
        this.menuHeader = "ManageService";
        this.iconCls = "anticon-appstore";
        this.subMenu = ['OrderedService', 'ActivateSite'];
        this.siderCollapse = false;
        this.siderWidth = 260;
        break;
      case "monitorService":
        this.menuHeader = "MonitorService";
        this.iconCls = "anticon-setting";
        this.subMenu = ['DataMonitor', 'networkTopology', 'resourceTopology'];
        this.siderCollapse = false;
        this.siderWidth = 260;
        break;

    }
  }

}
