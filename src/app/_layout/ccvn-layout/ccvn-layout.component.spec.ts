import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcvnLayoutComponent } from './ccvn-layout.component';

describe('CcvnLayoutComponent', () => {
  let component: CcvnLayoutComponent;
  let fixture: ComponentFixture<CcvnLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcvnLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcvnLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
