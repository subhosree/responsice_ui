import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ccvn-mobile-layout',
  templateUrl: './ccvn-mobile-layout.component.html',
  styleUrls: ['./ccvn-mobile-layout.component.less']
})
export class CcvnMobileLayoutComponent {

  isMobile:boolean = false
  constructor() {
    this.isMobile = screen.width < 575 ? true : false;
  } 

}
