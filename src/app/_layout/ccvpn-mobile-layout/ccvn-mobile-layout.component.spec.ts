import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcvnMobileLayoutComponent } from './ccvn-mobile-layout.component';

describe('CcvnLayoutComponent', () => {
  let component: CcvnMobileLayoutComponent;
  let fixture: ComponentFixture<CcvnMobileLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcvnMobileLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcvnMobileLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
