import { Component} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-app-layout',
  templateUrl: './app-layout.component.html',
  styleUrls: ['./app-layout.component.less']
})
export class AppLayoutComponent  {
  constructor(private translate:TranslateService){
    translate.addLangs(['en', 'zh']);
    translate.setDefaultLang('en');
    // translate.use('en');
  }

  // 多语言
  Language:String[] = ["zh","en"];
  selectLanguage = "en";

  changeLanguage(item){
    this.selectLanguage = item;
    this.translate.use(item);
  }

}
