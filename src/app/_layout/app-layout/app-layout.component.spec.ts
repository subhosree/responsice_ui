import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { TranslateModule, TranslateLoader, TranslateService, TranslateFakeLoader} from '@ngx-translate/core';

import { AppLayoutComponent } from './app-layout.component';

describe('AppLayoutComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppLayoutComponent
      ],
      imports: [
        RouterTestingModule,
        NgZorroAntdModule,
        TranslateModule.forRoot({loader: { provide: TranslateLoader, useClass: TranslateFakeLoader }})
      ],
      providers: [
        TranslateService
      ]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppLayoutComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('should change Language', async(() => {
    const fixture = TestBed.createComponent(AppLayoutComponent);
    const component = fixture.debugElement.componentInstance;
    component.changeLanguage("zh");
    expect(component.selectLanguage).toBe("zh");
  }));
});
