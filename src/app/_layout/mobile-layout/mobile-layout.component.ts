import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ThemeService } from '../../theme';

@Component({
  selector: 'app-mobile-layout',
  templateUrl: './mobile-layout.component.html',
  styleUrls: ['./mobile-layout.component.less']
})
export class MobileLayoutComponent {

  router: string;
  menuData = [{ name: "OrderService", subMenu: ["SDWAN", "SOTN"] },
  { name: "ManageService", subMenu: ["OrderedService", "ActivateSite"] },
  { name: "MonitorService", subMenu: ["DataMonitor", "networkTopology", "resourceTopology"] }
  ];
  btnTitle:string = 'zh';
  themeTitle:string ='dark';
  itemToLink = {
    'DataMonitor': '/ccvpn/ms/data-monitor',
    'networkTopology': '/ccvpn/ms/topology',
    'resourceTopology': '/ccvpn/treeTopology',
    'OrderedService': '/ccvpn/os/viewOrderedService',
    'View Service Instantiation': '/ccvpn/os/serviceInstance',
    'ConfigureVAS': '/ccvpn/ms/configureVas',
    'ActivateSite': '/ccvpn/ms/activateSite',
    'pushModel': '/ccvpn/ms/model',
    'SDWAN': '/ccvpn/os/orderService',
    'SOTN': '/ccvpn/os/sotn-orderService'
  };
  constructor(private translate: TranslateService, private themeService: ThemeService) {
    this.router = window.location.hash.replace('#', "");
    //console.log(this.router);
    translate.addLangs(['en', 'zh']);
    translate.setDefaultLang('en');
    this.themeService.setTheme('light');
  }





  // 多语言
  Language: String[] = ["zh", "en"];
  Themes: String[] = ["light", "dark"];
  selectedTheme = "light";
  selectLanguage = "en";
  subMenu: string[] = ["home"];
  siderCollapse: boolean = true;
  siderWidth: Number = 0;
  menuHeader: String = "";
  iconCls: String = "";

  changeLanguage() {
    this.selectLanguage = this.btnTitle;
    this.translate.use(this.selectLanguage);
    this.btnTitle = this.btnTitle == 'zh' ? 'en' :'zh';
  }
  
  changeTheme() {
    this.themeService.setTheme(this.themeTitle);
    this.themeTitle =  this.themeTitle == 'dark' ? 'light' :'dark';;
  }

}
