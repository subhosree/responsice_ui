import { Component, OnInit,  Input } from '@angular/core';
import {
  FormBuilder,  
  FormGroup,
  Validators
} from '@angular/forms';
import { OrderserviceeService } from './../orderservicee.service';
import { MyhttpService } from '../myhttp.service';

@Component({
  selector: 'app-configuresite',
  templateUrl: './configuresite.component.html',
  styleUrls: ['./configuresite.component.less']
})
export class ConfiguresiteComponent implements OnInit {
  showed_sites:any=[];
  onlineSites:any=[];
  offlineSites:any=[];
  onlineSitesIndex=0;
  offlineSitesIndex=0;
  newAddedSites:any=[];
  siteId;
  
  
  products = [] as Array<site>;
  private productsData = [] as Array<site>;
  private displayData = [];
  @Input() orderServiceData;
  @Input() checkAllValue:boolean=false;
  indeterminate = false;
  allChecked = false;
  serviceBandwidth = "10";
  total;
  wanArr = [];
  siteTitle;
  isVpnVisible = false;

  siteData = {} as site;
  editSiteData = {} as site;
  tempData= {} as site;

  sortName = null;
  sortValue = null;
  listOfSearchName = [];
  searchAddress: string;
  isMobile:boolean = false;

  wanCheckOptions = [
    { label: 'Internet', value: 'internet', checked: false },
    { label: 'MPLS', value: 'mpls', checked: false }
  ];

  band = [
    { label: '<100 Users', value: '1' },
    { label: '100-1000 Users', value: '10' },
    { label: '>1000 Users', value: '100' }
  ];

  siteType=['Hub','Spoke'];


  validateForm: FormGroup;
  constructor(private fb: FormBuilder, private myhttp: OrderserviceeService, private myhttp2: MyhttpService) {

  }


  onChange(value): void {
    this.wanArr = [];
    value.forEach(item => {
      if (item.checked)
        this.wanArr.push(item.value);
    });

    //this.configPostData.wanAccess = this.wanArr;
  }

  onSelectBandwidth(serviceBandwidth) {
    //no update
  }

  ngOnInit() {
    this.isMobile = screen.width < 575  ? true : false;
    //get Values from orderServiceData
   
    //this.getSitesData();
    this.setSelectedVal();

    this.validateForm = this.fb.group({

      site_name: [null, [Validators.required]],
      cloud_site: [null, [Validators.required]],
      zip_code: [null, [Validators.required]],
      address: [null, [Validators.required]],
      capacity: [null, [Validators.required]],
      // role_: [null, [Validators.required]],
      subnet_: [null, [Validators.required]],
      interface_: [null, [Validators.required]],
      description: [null, [Validators.required]],
      site_type: [null, [Validators.required]],
     // site_address: [null, [Validators.required]],
     // site_email: [null, [Validators.required]],
      role: [null, [Validators.required]],
      

    });

  }
  // getSitesData() {
  //   this.myhttp2.getSitesData().subscribe((data) => {          
  //     //this.showed_sites = data.body.filter(item => (item.role.toLowerCase().indexOf("hub")<0));
  //     this.showed_sites = data.body;
  //     this.setSelectedVal();
  //   }, (err) => {
  //     console.log(err);
  //   }
  //   );
  // }

  setSelectedVal() {
    // this.onlineSites = [];
    // this.offlineSites = [];
    // for(i=0;i<this.showed_sites.length;i++){
    //   (this.showed_sites[i].role.toLowerCase().indexOf("hub")>=0 || this.showed_sites[i].siteStatus.toLowerCase()=="online")  ? this.onlineSites.push(this.showed_sites[i]) : this.offlineSites.push(this.showed_sites[i]);   
    // }
    
    let wanChecked = this.orderServiceData && this.orderServiceData.wanAccess ? this.orderServiceData.wanAccess : [];
    if (wanChecked.length) {
      for (var i = 0; i < wanChecked.length; i++) {
        for (var j = 0; j < this.wanCheckOptions.length; j++) {
          if (wanChecked[i] == this.wanCheckOptions[j].value) {
            this.wanCheckOptions[j].checked = true;
          }
        }
      }
    }
    this.wanArr = this.orderServiceData && this.orderServiceData.wanAccess ? this.orderServiceData.wanAccess : [];
    this.serviceBandwidth = this.orderServiceData && this.orderServiceData.serviceBandwidth ? this.orderServiceData.serviceBandwidth : "10";

    this.products = this.orderServiceData && this.orderServiceData.sites ? this.orderServiceData.sites : [];
    
    for(var i=0;i<this.products.length;i++)
    {
      this.products[i].checked = false;
    }
    this.displayData = [...this.products];
  }





  // dialog

  isVisibleMiddle = false;


  showModalMiddle(): void {
    this.validateForm = this.fb.group({

      site_name: [null, [Validators.required]],
      cloud_site: [null, [Validators.required]],
      zip_code: [null, [Validators.required]],
      address: [null, [Validators.required]],
      capacity: [null, [Validators.required]],
      // role_: [null, [Validators.required]],
      subnet_: [null, [Validators.required]],
      interface_: [null, [Validators.required]],
      description: [null, [Validators.required]],
      site_type: [null, [Validators.required]],
      //site_address: [null, [Validators.required]],
     // site_email: [null, [Validators.required]],
      role: [null, [Validators.required]],

    });
    this.siteTitle = "addSite";
    this.isVisibleMiddle = true;
  }



  commonModalFunc() {
    if (this.siteTitle == "addSite") {
      this.addSiteData();
    }
    else {
      this._editSiteData();
    }

  }


  addSiteData(): boolean {
   
    if (this.siteData.siteName == null || this.siteData.location == null||this.siteData.description == null || this.siteData.isCloudSite == null || this.siteData.zipCode == null) {
      for (const i in this.validateForm.controls) {
        this.validateForm.controls[i].markAsDirty();
        this.validateForm.controls[i].updateValueAndValidity();
      }
      return false;
    }
    else {
      this.isVisibleMiddle = false;
      this.putnewSiteData(this.siteData,false);
      return true;
    }

  }


  _editSiteData(): boolean {
  
    // if (this.editSiteData != this.siteData) {

    if (this.siteData.role == "" ||this.siteData.siteName == "" || this.siteData.location == ""|| this.siteData.description == "" || this.siteData.isCloudSite == "" || this.siteData.zipCode == "") {
      return false;
    }
    else {
      this.putnewSiteData(this.siteData,true);
      this.isVisibleMiddle = false;
      this.siteData.capacity = null;
      this.siteData.isCloudSite = null;
      this.siteData.location = null;
      this.siteData.siteName = null;
      this.siteData.description=null;
     // this.siteData.site_address=null;
      this.siteData.site_type=null;
     // this.siteData.site_email=null;
      this.siteData.role=null;

      return true;
    }

    // }
  }


  arrayVar = 0;
  putnewSiteData(siteData, fromEdit) {

    this.tempData = {...siteData };

    this.myhttp.getSiteData(siteData)
      .subscribe((data) => {        
        this.tempData.interface = data.interface;
        this.tempData.price = data.price;
        this.tempData.subnet = data.subnet;
        this.tempData.expand = false;
        this.tempData.checked = false;
        
        this.tempData.role = data.role;         
        if(fromEdit)
        { 
          var objIndex = this.products.findIndex((obj => obj.siteId == this.tempData.siteId));
          this.products[objIndex]  =  this.tempData ;     
        }
        else
        {
          //var firstData = this.offlineSites.splice(0,1)[0];
          //this.tempData.siteId = this.offlineSites.splice(0,1)[0].siteId + ""; 
          //this.tempData = {...firstData};   
          this.tempData.siteId = (this.products.length + 1) + "";      
          this.products.push({...this.tempData});
        }

        this.products = this.products.splice(0, this.products.length);  
        this.siteData = {} as site;
        this.total = this.products.length;
        this.checkAll(false);
        console.log("products===>", this.products);
        console.log("site data===>", this.siteData);

      }, (err) => {
        console.log(err);
      }
      );
    
    this.total = this.products.length;
  }


  siteJsonDataArray = [];
  _editSite(num) {

    this.isVisibleMiddle = true;
    this.siteTitle = "Edit Site";
    let editSiteVar = this.products.filter((d) => {
      return d.siteId == num;
    });


    this.siteJsonDataArray = editSiteVar;
    this.siteData = Object.assign({}, ...this.siteJsonDataArray);


  }

  deleteSite(siteId) {
    this.products = this.products.filter(item => item.siteId !== siteId);
    this.total = this.products.length;
  }  

  handleOkMiddle(): void {
    this.isVisibleMiddle = false;
  }

  handleCancelMiddle(): void {
    this.isVisibleMiddle = false;
  }

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
  }

  deleteSelectedSite() {
    this.products = this.products.filter(item => item.checked == false);
    this.total = this.products.length;
  }

  currentPageDataChange($event: Array<site>): void {
    this.displayData = $event;
    this.refreshStatus();
  }

  refreshStatus(): void {
    const allChecked = this.displayData.filter(value => !value.disabled).every(value => value.checked === true);
    const allUnChecked = this.displayData.filter(value => !value.disabled).every(value => !value.checked);
    this.allChecked = allChecked;
    this.indeterminate = (!allChecked) && (!allUnChecked); 
    if(allChecked){
    this.checkAllValue=allChecked; 
    }
  }

  checkAll(value: boolean): void {

    if(value==false){
    this.displayData = [...this.products];
    }
    this.displayData.forEach(data => {
      if (!data.disabled) {
        data.checked = value;
      }
    });
    this.refreshStatus();
  }

  sort(sort: { key: string, value: string }): void {
    this.sortName = sort.key;
    this.sortValue = sort.value;
    this.search();
  }

  filter(listOfSearchName: string[], searchAddress: string): void {
    this.listOfSearchName = listOfSearchName;
    this.searchAddress = searchAddress;
    this.search();
  }

  search(): void {
    /** filter data **/
    const filterFunc = item => (this.searchAddress ? item.address.indexOf(this.searchAddress) !== -1 : true) && (this.listOfSearchName.length ? this.listOfSearchName.some(name => item.name.indexOf(name) !== -1) : true);
    const data = this.products.filter(item => filterFunc(item));
    /** sort data **/    
    if (this.sortName && this.sortValue) {
      this.products = data.sort((a, b) => (this.sortValue === 'ascend') ? (a[ this.sortName ] > b[ this.sortName ] ? 1 : -1) : (b[ this.sortName ] > a[ this.sortName ] ? 1 : -1));
    } else {
      this.products = data;
    }
  }


}
