import { Component, OnInit, Input  } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';

import { OrderserviceeService } from './../orderservicee.service';


@Component({
  selector: 'app-configurevpn',
  templateUrl: './configurevpn.component.html',
  styleUrls: ['./configurevpn.component.less']
})
export class ConfigurevpnComponent implements OnInit {
  
  @Input() orderServiceData;
  configVpnArray = [] as Array<site>;
  vpnArray = [] as Array<vpn>;
  products = [] as Array<site>;
  selectedArray = [] as Array<site>;
  modalTitle: any;
  isVisible: boolean = false;
  total;
  allChecked = false;
  indeterminate = false;
  displayData = [];
  checkedId = [];  
  _vpnArray = [] ;

  vpnId:any;
  sortName = null;
  sortValue = null;
  listOfSearchName = [];
  searchAddress: string;
  arrayVarProd = 0;
  isMobile:boolean=true;


  constructor(private myhttp: OrderserviceeService) {}

  ngOnInit()  
  {
   
    this.getTableData();
    this.isMobile = screen.width < 575 ? true : false;
  }

  showModalMiddle(vpnId) {
     this.isVisible = true;
     this.vpnId = vpnId;
    
  }

  setChecked() { 
    this.products.forEach((e1) => this.configVpnArray.forEach((e2) => {
      if (e1.siteName === e2.siteName) {
        e1.checked = true;
      }
    }
    ));
  }

  handleOk() {
    this.configVpnArray = this.selectedArray;
    this.vpnArray.forEach(item => {
      if(this.vpnId == item.vpnId)
      {
        item.sites = this.selectedArray;
      }              
    });

    this.isVisible = false;
  }

  handleCancel() {
    this.isVisible = false;
  }


  getTableData() {
   // console.log(this.orderServiceData);
  
    this.products = this.orderServiceData.sites || []; 
    //console.log("products array is ==>", this.products);  
    this.setChecked();
    this.total = this.products.length;
    this.modalTitle = "Select Site";
    if(this.orderServiceData && this.orderServiceData.vpnArray)
    {
      this.vpnArray = this.orderServiceData.vpnArray;
      console.log("vpn arrrya is ===>", this.vpnArray);
     
      // fdfs
    }
    else
    {
      
      this.vpnArray =[
        {
            "vpnId": "1",
            "vpnName": "Default VPN",
            "vpnType": "Hub-Spoke",
            "sites": [],
            "vpnBandwidth": "100",
            "vpnThreshold": "50",
            "cameras": "3"            
        }];  

    }  
    console.log("vpn arrrya is ===>", this.vpnArray);
  }

  

  checkAll(value: boolean) {
    this.products.forEach(data => {      
        data.checked = value;
    });
    this.refreshStatus();
  }

  refreshStatus() {
    const allChecked = this.products.every(value => value.checked === true);
    console.log("all check value===>", allChecked);
    const allUnChecked = this.products.every(value => !value.checked);
    this.allChecked = allChecked;
    this.indeterminate = (!allChecked) && (!allUnChecked);
    this.selectedArray = this.products.filter(value => value.checked === true);
  }

  sort(sort: { key: string, value: string }) {
    this.sortName = sort.key;
    this.sortValue = sort.value;
    this.search();
  }

  filter(listOfSearchName: string[], searchAddress: string){
    this.listOfSearchName = listOfSearchName;
    this.searchAddress = searchAddress;
    this.search();
  }

  search() {
    /** filter data **/
    const filterFunc = item => (this.searchAddress ? item.address.indexOf(this.searchAddress) !== -1 : true) && (this.listOfSearchName.length ? this.listOfSearchName.some(name => item.name.indexOf(name) !== -1) : true);
    const data = this.products.filter(item => filterFunc(item));
    /** sort data **/
    if (this.sortName && this.sortValue) {
      this.products = data.sort((a, b) => (this.sortValue === 'ascend') ? (a[ this.sortName ] > b[ this.sortName ] ? 1 : -1) : (b[ this.sortName ] > a[ this.sortName ] ? 1 : -1));
    } else {
      this.products = data;
    }
  }

  currentPageDataChange($event: Array<site>) {
    this.products = $event;
    this.refreshStatus();
  }

 

}
