import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigurevpnComponent } from './configurevpn.component';

describe('ConfigurevpnComponent', () => {
  let component: ConfigurevpnComponent;
  let fixture: ComponentFixture<ConfigurevpnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigurevpnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigurevpnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
