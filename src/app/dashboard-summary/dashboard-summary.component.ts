import { Component, OnInit } from '@angular/core';
import {TopologyComponent} from '../topology/topology.component';
import { TreeTopologyComponent } from '../tree-topology/tree-topology.component';
import { DataMonitorComponent } from '../data-monitor/data-monitor.component';
@Component({
  selector: 'app-dashboard-summary',
  templateUrl: './dashboard-summary.component.html',
  styleUrls: ['./dashboard-summary.component.less']
})
export class DashboardSummaryComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
