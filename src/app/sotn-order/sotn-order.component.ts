import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import { OrderserviceeService } from './../orderservicee.service';
import { ConfigureSotnSiteComponent } from './../configure-sotn-site/configure-sotn-site.component';
import { ConfiguresiteComponentservice } from './../configure-sotn-service/configuresite.component';
import { RouterModule, Router } from '@angular/router';

@Component({
  selector: 'app-sotn-order',
  templateUrl: './sotn-order.component.html',
  styleUrls: ['./sotn-order.component.less']
})
export class SotnOrderComponent implements OnInit, OnDestroy {





  orderServiceData = {} as sotnServiceList;
  sotnProducts = [] as Array<sotnSites>;

  siteData = {} as sotnSites;
  tempData = {} as sotnSites;

  configSiteFlag = false;
  configVpnFlag = false;
  intervalData: any;
  buttonDisabled: boolean = false;
  isMobile: boolean = false;

  panels = [
    {
      active: true,
      disabled: false
    }, {
      active: false,
      disabled: false
    }, {
      active: false,
      disabled: false

    }]
    ;

  ngOnInit() {

    this.isMobile = screen.width < 575 ? true : false
    if(this.sotnProducts.length != 0)
    {
      
    }
  }


  ngOnDestroy() {
    if (this.intervalData) {
      clearInterval(this.intervalData);
    }

  }

  ngOnDelete() {
    if (this.intervalData) {
      clearInterval(this.intervalData);
    }
  }

  @ViewChild(ConfiguresiteComponentservice) sitechild;
  @ViewChild(ConfigureSotnSiteComponent) vpnchild;


  constructor(private myhttp: OrderserviceeService, private router: Router) { }
  

  current = 0;
  tipMsg: String = "serviceCreationInitiated";


  pre(): void {
    this.current -= 1;
  }

  next(): void {
    this.current += 1;
    if (this.current == 1) {
      this.selectOrderSiteData();
    }
  }

  displayMsg: boolean = false;
  displayMsg2: boolean = false;

  
  saveData(evt, idx): void {
        
        if (evt) {
          this.panels[idx].active = true;
          
         
        }
        else {
             
          this.panels[idx].active = false;
         
          
         // this.displayMsg = true;

           switch (idx) {
            case 0:
          
              this.selectOrderSiteData();
            
             
              break;
            case 1:
              this.sotnSiteData();
              break;
           }

          if(idx)
          {
            this.selectOrderSiteData();
          }
        }
    
      }


 
  selectOrderSiteData(): void {
    
    this.orderServiceData.service = this.sitechild.siteData;

    if (this.orderServiceData.service.serviceName == null ||
      this.orderServiceData.service.description == null ||
      this.orderServiceData.service.cos == null ||
      this.orderServiceData.service.serviceClassificationType == null ||
      this.orderServiceData.service.SLAType == null ||
      this.orderServiceData.service.highAvailability == null ||
      this.orderServiceData.service.cir == null ||
      this.orderServiceData.service.eir == null) {
        
      this.displayMsg = true;
      var comp = this;
      setTimeout(function () {
        comp.displayMsg = false;
      }, 5000);
      this.current -= 1;
    }
   

  }
  sotnSiteData(){
    this.orderServiceData.sites = this.vpnchild.sotnProducts; 
    if(this.orderServiceData.sites.length != 0){
      
    }

  }
 



  private doneRoute;
  putnewSotnSiteData() {
    
    console.log("order service data===>", this.orderServiceData)
    this.tipMsg = "Instantiation In Progress";
    this.displayMsg2 = true;
    this.buttonDisabled = true;
    this.myhttp.postSotnData(this.orderServiceData)
      .subscribe((data) => { 
              
        let comp = this;
        comp.tipMsg = "Instantiation In Progress";
       
          
          comp.displayMsg2 = true;
         
                  
        this.intervalData = setInterval(() => {
          this.myhttp.getServiceInstantiationStatus(data.service.serviceId).subscribe((data) => {
            if (data.status == "1") {
             
              clearInterval(comp.intervalData);
              comp.displayMsg2 = true;
              comp.tipMsg = "Service Created";
             
              if(this.isMobile==false)
              {
              comp.goToPage();
              }
              else
              {
                
                comp.goToPage_mobile();
               

              }
             
            }
            else {
              comp.tipMsg = "Instantiation In Progress";
              comp.displayMsg2 = true;
              
            }
          }, (err) => {
            console.log(err);
          }
          );
        }, 1000);


      }, (err) => {
        console.log(err);
      }
      );
  }
  goToPage(): void {
   
    setTimeout(function () {
      document.getElementById('manageService').click();
    }, 1000);
  }

 goToPage_mobile(): void {

 
      this.router.navigate(['ccvpn/os/viewOrderedService']);
  
  }

}