import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SotnOrderComponent } from './sotn-order.component';

describe('SotnOrderComponent', () => {
  let component: SotnOrderComponent;
  let fixture: ComponentFixture<SotnOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SotnOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SotnOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
