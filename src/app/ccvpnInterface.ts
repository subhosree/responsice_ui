interface site{
    siteId : String;
    siteNo:String;
    siteName : String;
    isCloudSite : String;
    location : String;  
    
    role : String;
    site_name:String;
    capacity : String;
    interface: String;
    subnet : String;
    price : String;
    cameras:any[];
    checked: boolean;
    expand: boolean;
   
    site_type:String;
    site_address:String;
    site_email:String;
    site_startTime:String;
    site_EndTime:String;
    reRoute:String;
    colorAware:String;
    couplingFlag:String;    
    sotn_COS:String;
    sotn_EBS:String;
    sotn_CIR:String;
    sotn_EIR:String;
    sotn_CBS:String;
    sotn_Spec:String;
    sotn_VLAN:String;
    sotn_Action:String;
    
    sotnSiteName:String;
    address:String;
    vlan:String;
    description: String;
    zipCode : String;
}
interface sotnServiceList{
        service: sotnService;
        sites: sotnSites[];
      }
      
interface sotnService{
    serviceName:String;
    description:String;
    startTime:String;
    endTime:String;
    cos:String;
    reRoute:String;
    specification:String;
    duealLink:String;
    cir:String;
    eir:String;
    cbs:String;
    ebs:String;
    colorAware:String;
    couplingFlag:String; 
}

interface sotnSites {
    sotnSiteName:String;
    description:String;
    zipCode:String;
    address:String;
    vlan:String;
}
    



// interface sotnServiceList{
//     service: sotnService;
//     sites: sotnSites[];
//   }
  
//   interface sotnService{
//     serviceName:String;
//     description:String;
//     startTime:String;
//     endTime:String;
//     cos:String;
//     reRoute:String;
//     specification:String;
//     cir:String;
//     eir:String;
//     cbs:String;
//     ebs:String;
//     colorAware:String;
//     couplingFlag:String; 
//   }
  
//   interface sotnSites {
//     sotnSiteName:String;
//     description:String;
//     zipCode:String;
//     address:String;
//     vlan:String;
//   }


interface serviceList{
    serviceInstanceId: Number;
    serviceInstanceName: Number;
    serviceType: String;
    serviceRole: String;
    resourceVersion: String;
}

interface vpn {
    vpnId: String;
    vpnName: String;
    vpnType: String;
    sites: site[];
    vpnBandwidth: String,
    vpnThreshold: String,
    cameras : String;
}

interface serviceData {
    bandWidth : String;
    wlanAccess : String[];
    serviceCost : String;    
    cameraCost : String;
    vpnCost: String,
    bandwidthCost:String,
    vpnInformations: vpn[];
}

interface sotnCost{
    serviceCost: string;
    vpnCost: string;
    siteCost: string;
}

/* start order service interface */

interface summary {
    siteArray: site[];
    vpnInfo: vpn;
    vpnBandwidth: orderBandwidthModel;
    wlanAccess:string[];
    serviceCost:string;
    filename:string;
    cost:sotnCost;
}

interface networkTopologySummary {
    vpnInformations: vpnArray[];
}

interface vpnArray {
    sites: site[];
}


interface resourceTopologyService {
    sites: site[];
    vpnArray:vpn[];
}

interface orderBandwidthModel {
    vpnId: string;
    vpnName: string;
    vpnBandwidth: number;
    Threshold: number;
}

interface finalOrderService {
    serviceBandwidth : String;
    wanAccess: string[];
    sites: site[];
    vpnArray:vpn[];
    orderBandwidth: orderBandwidthModel[];
    numOfCamera: String;
}


interface postObjInterface{
    bandWidth:String;
    wlanAccess:string[];
    vpnInformations:vpn[];
    totalCost:String;
    totalQuantity:String;
}

/* End order service interface */


interface modelInfo {    
    edgeNameList: any[];
    modelInfoList: any[];
}