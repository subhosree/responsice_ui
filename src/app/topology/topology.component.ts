import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MyhttpService } from '../myhttp.service';
import { OrderserviceeService } from './../orderservicee.service';
import { ThemeService } from '../theme';
import { Router } from '@angular/router';


import * as L from 'leaflet';

@Component({
  selector: 'app-topology',
  templateUrl: './topology.component.html',
  styleUrls: ['./topology.component.less']
})

// Implémenter OnInit
export class TopologyComponent implements OnInit {

  serviceSubscriptionList = [] as Array<any>;
  serviceInstanceList = [] as Array<any>;
  selectedSubscriptionType: string;
  selectedServiceInstance: string = "";

  constructor(private myhttp: MyhttpService, private themeService: ThemeService, private order: OrderserviceeService, private router:Router) { } 
  returnResponse: boolean = true;
  status = 0;
  siteData: any;

  summaryInfo = {} as networkTopologySummary;

  showDetails: boolean = false;
  sitesData: any[];
  sitesLatLng: any[];
  hubIcon = L.icon({
    iconUrl: './assets/images/topology/hub.png',
    iconSize: [40, 40],
  });
  SubscriptionService: any
  saveSubscriptiontype: any
  serviceInstanceId: any
  startlat = 30.28552;
  startlon = 114.15769;
  startlatSotn = 51.5073219;
  startlonSotn = -0.1276474;
  map: any;
  logLatArr = [];
  ll;
  logLatGlobalArray = [];
  link_status = "up";

  internet: boolean = false;
  mpls: boolean = false;


  ngOnInit() {
    this.getSubscribeTypes();
    this.map = L.map('map', { minZoom: 1, maxZoom: 20 }).setView([this.startlat, this.startlon], 4);
    L.tileLayer('https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png', { attribution: 'OSM' }).addTo(this.map);
  }

  //Get SubscriptionType
  getSubscribeTypes() {
    this.order.getSubscriptionType().subscribe((data) => {
      this.serviceSubscriptionList = data.subscriptions;
    }, (err) => {
      console.log(err);
    });
  }

  //Get subscription instanceID by calling With Subscription Type
  getServiceInstanceList(subscriptionType) {
    this.serviceInstanceList = [];
    this.selectedServiceInstance = "";
    if (this.map) {
      let comp = this;
      this.map.eachLayer(function (layer) {
        comp.map.removeLayer(layer);
      });
      L.tileLayer('https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png', { attribution: 'OSM' }).addTo(this.map);
    }
    this.hubData = [];
    this.hubData2 = [];
    this.spokePointData = [];
    this.spokePointData2 = [];

    this.order.getServiceInstance(subscriptionType).subscribe((data) => {
      this.serviceInstanceList = data.serviceInstanceList;
    }, (err) => {
      console.log(err);
    });
  }

  // Getting sitedata Based On Type and ID
  getSelectedsubscriptionInfo(s) {
    if (this.map) {
      let comp = this;
      this.map.eachLayer(function (layer) {
        comp.map.removeLayer(layer);
      });
      L.tileLayer('https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png', { attribution: 'OSM' }).addTo(this.map);
    }
    this.hubData = [];
    this.hubData2 = [];
    this.spokePointData = [];
    this.spokePointData2 = [];

    if (this.selectedSubscriptionType.toLowerCase() == "sdwan") {
      this.map.setView([this.startlat, this.startlon], 4);      
      this.myhttp.getSitesData().subscribe((data) => {
        this.sitesData = data.body;
        this.internet = true;
        this.DrawMap();
      });
    }
    else {
      this.map.setView([this.startlatSotn, this.startlonSotn], 4);
      this.myhttp.getSelectedTopologySitesData(this.selectedSubscriptionType, this.selectedServiceInstance).subscribe((data) => {
        this.sitesData = data;
        this.DrawMap();
      }, (err) => {
        console.log(err);
      });
    }
  }


  DrawMap(): void {
    this.logLatArr = [];
    for (var i = 0; i < this.sitesData.length; i++) {
      var zipCode = this.sitesData[i].zipCode;
      var siteLocation = this.sitesData[i].location;
      var fullAddress = zipCode + "," + siteLocation;
      var siteData = this.sitesData[i];
      var loglatDt = this.logLatGlobalArray.filter(value => (value.zipCode === this.sitesData[i].zipCode && value.siteLocation == this.sitesData[i].siteLocation));
      if (loglatDt.length) {
        this.logLatArr.push({ loglat: loglatDt[0].loglat, siteData: siteData });
        if (this.logLatArr.length == this.sitesData.length) {
          this.drawLinesIcons();
        }
      }
      else {
        this.myhttp.getLongLat(fullAddress, siteData)
          .subscribe((data) => {
            var sitesData = data.siteData;
            data.dataRet.subscribe((data) => {
              this.logLatArr.push({ loglat: data, siteData: sitesData });
              this.logLatGlobalArray.push({ loglat: data, 'zipCode': sitesData.zipCode, 'siteLocation': siteLocation.siteLocation });
              if (this.logLatArr.length == this.sitesData.length) {
                this.drawLinesIcons();
              }
            });
          });
      }
    }

  }

  hubData = [];
  hubData2 = [];
  spokePointData = [];
  spokePointData2 = [];
  drawLinesIcons() {
   
    for (var i = 0; i < this.logLatArr.length; i++) {
      this.drawIconHub(this.logLatArr[i].loglat, this.logLatArr[i].siteData);
    }

    for (var i = 0; i < this.logLatArr.length; i++) {
      this.drawIcon(this.logLatArr[i].loglat, this.logLatArr[i].siteData);
    }

    console.log("hub data array===>", this.hubData);

    this.drawLines();
  }

  drawLines() {
    if (this.hubData.length == 1) {
      for (var i = 0; i < this.spokePointData.length; i++) {
        var polylineGreen = new L.Polyline([
          this.hubData[0],
          this.spokePointData[i],
        ], {
            color:  '#1890ff',
            weight: 3,
            opacity: 0.5
          }).addTo(this.map);
      }
    }
    else {
      for (var i = 0; i < this.hubData.length; i++) {
        var polylineGreen = new L.Polyline([
          this.hubData[i],
          this.hubData[i + 1],
        ], {
            color: this.link_status == 'up' ? '#1890ff' : 'red',
            weight: 3,
            opacity: 0.5
          }).addTo(this.map);
      }
    }

  }

  drawIconHub(myArr: any[], sitesData) {
    if (sitesData.role.toLowerCase().indexOf("hub") >= 0) {
      var hubMarker = L.marker([myArr[0].lat, myArr[0].lon], { icon: this.hubIcon });
      this.hubData.push({ 'lat': myArr[0].lat, 'lon': myArr[0].lon });
      this.hubData2.push({ 'lat': Number(myArr[0].lat) + 0.6, 'lon': myArr[0].lon });
      hubMarker.bindTooltip((this.selectedSubscriptionType.toLowerCase() == 'sotn' ? '' : 'ONAP ') + sitesData.siteName, { permanent: true, direction: "bottom", offset: [0, 30] });
      var template = '<div id="popup-div">\
      <table style="font-size:1em">\
        <tr style="font-size:1em">\
          <th style="font-size:1em" colspan="2">Details:</th>\
        </tr>\
        <tr style="font-size:1em">\
        <th style="font-size:1em">Role:</th>\
        <td style="font-size:1em">Hub</td>\
      </tr>\
        <tr style="font-size:1em">\
          <th style="font-size:1em">Interface:</th>\
          <td style="font-size:1em">'+ sitesData.interface + this.SubscriptionService + '</td>\
        </tr>\
        <tr style="font-size:1em">\
          <th style="font-size:1em">Subnet:</th>\
          <td style="font-size:1em">'+ sitesData.subnet + '</td>\
        </tr>\
        </table>\
    </div>';
      var t1;
      if (this.selectedSubscriptionType.toLowerCase() == 'sotn') {
        let attributes = sitesData.attribute;
        t1 = '<div id="popup-div">\
      <table style="font-size:1em">\
        <tr style="font-size:1em">\
          <th style="font-size:1em" colspan="2">Details:</th>\
        </tr>\
        ';
        Object.entries(attributes).forEach(entry => {          
          t1 += '<tr style="font-size:1em">\
          <th style="font-size:1em">'+ entry[0] + ':</th>\
          <td style="font-size:1em">'+ entry[1] + '</td>\
        </tr>\
          ';
        });

        this.link_status = attributes['Link Status'].toLowerCase();

        t1 += '</table>\
        </div>';
      }

      hubMarker.bindPopup(this.selectedSubscriptionType.toLowerCase() == 'sotn' ? t1 : template).openPopup();
      hubMarker.addTo(this.map);
      hubMarker.on('mouseover', this.onMouseOverHub).on('mouseout', this.onMouseOutHub);
    }

  }

  onMouseOver(evt) {
    var spokeIcon = L.icon({
      iconUrl: './assets/images/topology/' + evt.target.options.siteData.statEnum.toLowerCase() + '.png',
      iconSize: [50, 50]
    });

    evt.target.setIcon(spokeIcon);
  }

  onMouseOut(evt) {
    var spokeIcon = L.icon({
      iconUrl: './assets/images/topology/' + evt.target.options.siteData.statEnum.toLowerCase() + '.png',
      iconSize: [41, 41]
    });
    evt.target.setIcon(spokeIcon);
  }

  onMouseOverHub(evt) {
    var hubIcon = L.icon({
      iconUrl: './assets/images/topology/hub.png',
      iconSize: [50, 50],
    });

    evt.target.setIcon(hubIcon);
  }

  onMouseOutHub(evt) {
    var hubIcon = L.icon({
      iconUrl: './assets/images/topology/hub.png',
      iconSize: [40, 40],
    });
    evt.target.setIcon(hubIcon);
  }



  drawIcon(myArr: any[], sitesData) {
    if (sitesData.role.toLowerCase().indexOf("hub") < 0) {
      var t = new L.latLngBounds;
      if (Math.abs(Number(this.hubData[0].lon) - Number(myArr[0].lon)) > 180) {
        if (Number(this.hubData[0].lon) < Number(myArr[0].lon)) {
          myArr[0].lon = Number(myArr[0].lon) - 360;
        }
        else {
          myArr[0].lon = Number(myArr[0].lon) + 360;
        }

      }


      this.spokePointData.push({ 'lat': myArr[0].lat, 'lon': myArr[0].lon, 'activeStatus': sitesData.activeStatus });
      this.spokePointData2.push({ 'lat': Number(myArr[0].lat) + 0.5, 'lon': myArr[0].lon });

      var spokeIcon = L.icon({
        iconUrl: './assets/images/topology/' + sitesData.statEnum.toLowerCase() + '.png',
        iconSize: [41, 41]
      });

      var spokeMarker = L.marker([myArr[0].lat, myArr[0].lon], {
        icon: spokeIcon,
        siteData: sitesData
      });

      spokeMarker.bindTooltip(sitesData.siteName, { permanent: true, direction: "bottom", offset: [0, 20] });
      var edgeName = sitesData.edgeName || "-";
      var edgeIp = sitesData.edgeIP || "-";
      var template = '<div id="popup-div">\
        <table style="font-size:1em">\
          <tr style="font-size:1em">\
            <th style="font-size:1em" colspan="2">Details:</th>\
          </tr>\
          <tr style="font-size:1em">\
          <th style="font-size:1em">Role:</th>\
          <td style="font-size:1em">Spoke</td>\
        </tr>\
          <tr cstyle="font-size:1em">\
            <th style="font-size:1em">Device Name:</th>\
            <td style="font-size:1em">'+ sitesData.deviceName + '</td>\
          </tr>\
          <tr style="font-size:1em">\
            <th style="font-size:1em">Device Desc:</th>\
            <td style="font-size:1em">'+ sitesData.deviceDesc + '</td>\
          </tr>\
          <tr style="font-size:1em">\
            <th style="font-size:1em" colspan="2">Edge Details:</th>\
          </tr>\
          <tr style="font-size:1em">\
            <th style="font-size:1em">Edge Name:</th>\
            <td style="font-size:1em">'+ edgeName + '</td>\
          </tr>\
          <tr style="font-size:1em">\
            <th style="font-size:1em"">Edge Ip:</th>\
            <td style="font-size:1em">'+ edgeIp + '</td>\
          </tr>\
             </table>\
      </div>';

      spokeMarker.bindPopup(template).openPopup();
      spokeMarker.addTo(this.map);
      spokeMarker.on('mouseover', this.onMouseOver).on('mouseout', this.onMouseOut);
    }
  }



}
