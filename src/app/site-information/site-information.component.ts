import { Component, OnInit } from '@angular/core';
import { MyhttpService } from '../myhttp.service';
import { OrderserviceeService } from './../orderservicee.service';

@Component({
  selector: 'app-site-information',
  templateUrl: './site-information.component.html',
  styleUrls: ['./site-information.component.less']
})
export class SiteInformationComponent implements OnInit {

  expandDataSet = [
    { rowIdx: 1, name: 'Site Information', quantity: 'Quantity', prize: 'Price ($ K)', expand: true, quantityNo:1,cost:1, mobileExpand:false },
    { rowIdx: 2, name: 'VPN Information', quantity: 'Quantity', prize: 'Price ($ K)', expand: true, quantityNo:1,cost:1 , mobileExpand:false},
    { rowIdx: 3, name: 'BandWidth Policy', quantity: 'Quantity', prize: 'Price ($ K)', expand: true, quantityNo:1,cost:1, mobileExpand:false},
    { rowIdx: 4, name: 'Vas Information', quantity: 'Quantity', prize: 'Price ($ K)', expand: true, quantityNo:1,cost:1, mobileExpand:false },

  ];

  summaryInfo={} as summary;
  serviceList={} as serviceList;
 
  camerasLength:number=0;
  camerasCost:number=0;
  bandWidthLength:number=0;
  bandWidthCost:number=0;
  vpnDataLength:number=0;
  vpnDataCost:number=0;
  siteDataLength:number=0;
  siteDataCost:number=0;
  expandTrue:boolean=true;
  wlanAccess:string="";
  constructor(private myhttp: OrderserviceeService) { }

  ngOnInit() {
    this.getSite()
  }

  getSite(){
    this.myhttp.getSummaryData().subscribe((data)=>{     
      this.summaryInfo = data;
      console.log("summary info load  ====>", this.summaryInfo);
      this.wlanAccess= this.summaryInfo.wlanAccess.join(",");     
      
      this.getVpnData(this.summaryInfo);
    
    }, (err) => {
      console.log(err);
    });

  }
 


  getVpnData(summaryInfoData){
    this.vpnDataLength=summaryInfoData.vpnInformations.length;
    this.expandDataSet[1].quantityNo = this.vpnDataLength;
    this.vpnDataCost = summaryInfoData.vpnCost;
    this.expandDataSet[1].cost = this.vpnDataCost;
  }

  


}
