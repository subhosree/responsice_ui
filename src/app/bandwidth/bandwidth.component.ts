import { Component, OnInit, Input } from '@angular/core';
import { OrderserviceeService } from './../orderservicee.service';
@Component({
  selector: 'app-bandwidth',
  templateUrl: './bandwidth.component.html',
  styleUrls: ['./bandwidth.component.less']
})
export class BandwidthComponent implements OnInit {
  // // validateForm: FormGroup;
  editCache = {};
  dataSet = [];
  @Input() orderServiceData;
  
  siteData = {} as sotnSites;
  tempData = {} as sotnSites;

  constructor(private myHttp: OrderserviceeService, private myhttp: OrderserviceeService) { }

  startEdit(key: string): void {
    this.editCache[key].edit = true;
  }
  cancelEdit(key: string): void {
    this.editCache[key].edit = false;
  }
  saveEdit(key: string): void {
    const index = this.dataSet.findIndex(item => item.vpnId === key);
    Object.assign(this.dataSet[index], this.editCache[key].data);

    if (this.dataSet[0].vpnBandwidth > "100") {
      // alert("Please enter Bandwidth between 1 adn 100");
      this.dataSet[0].vpnBandwidth = "100";
    }
    this.editCache[key].edit = false;
  }

  updateEditCache(): void {
    this.dataSet.forEach(item => {
      if (!this.editCache[item.vpnId]) {
        this.editCache[item.vpnId] = {
          edit: false,
          data: { ...item }
        };
      }
    });
  }
  ngOnInit() {
    if (this.orderServiceData && this.orderServiceData.orderBandwidth) {
      this.dataSet = this.orderServiceData.orderBandwidth;
      console.log("bandwidht orderserviceData==>", this.orderServiceData);
      this.updateEditCache();
    }
    else {
      console.log("bandwidht orderserviceData==>", this.orderServiceData);
      this.dataSet = [
        {
          "vpnId": "1",
          "vpnName": "Default VPN",
          "vpnType": "Hub-Spoke",
          "sites": [],
          "vpnBandwidth": "100",
          "vpnThreshold": "50",
          "cameras": "3"
        }];
      this.updateEditCache();
    }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

}