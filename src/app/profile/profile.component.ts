import { Component, OnInit } from '@angular/core';
import { NzMessageService, UploadFile, NzModalRef, NzModalService } from 'ng-zorro-antd';
import { HttpClient, HttpResponse, HttpEventType } from '@angular/common/http';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { UploadFileService } from './upload-file.service';
import { Observable } from 'rxjs/Observable';
import { baseUrl } from './../dataInterface';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.less']
})
export class ProfileComponent implements OnInit {
  isVisible = false;
  isConfirmLoading = false;
  validateForm: FormGroup;
  vipName: string;
  vipRole: string;
  vipImage: UploadFile[] = [];
  vipPath: string;
  imageToShow: any = [];
  isImageLoading: boolean;
  selectedFiles: FileList
  currentFileUpload: File
  progress: { percentage: number } = { percentage: 0 }
  goals: any[] = [];
  imgSrc;




  constructor(private fb: FormBuilder, private uploadService: UploadFileService) {

  }
  ngOnInit() {
    this.imgSrc = './assets/images/manage_service/add.svg';

    this.getVipDetails();
    //this.getImageFromService('http://localhost:4200/assets/images/VNF.png',0);

    this.validateForm = this.fb.group({
      vip_name: [null, [Validators.required]],
      vip_role: [null, [Validators.required]],
      vip_image: [null, [Validators.required]],
      vip_path: [null, [Validators.required]]
    });


  }

  getVipDetails() {
    this.goals = [];
    this.uploadService.getFiles()
      .subscribe((data) => {
        for (var i = 0; i < data.length; i++) {
          var x = data[i].imgUrl.split("v1");
          var url = baseUrl.baseUrl + x[1];
          data[i].imgUrl = url;
          this.goals.push(data[i]);
        }
      }, (err) => {
        console.log(err);
      });
  }

  showModal(): void {
    this.isVisible = true;
  }

  handleCancel(): void {
    this.isVisible = false;
  }


  selectFile(event) {
    const file = event.target.files.item(0);
    if (file.type.match('image.*')) {
      this.selectedFiles = event.target.files;
    } else {
      //alert('invalid format!');
    }
  }

  upload() {
    if(!(this.selectedFiles && this.vipName && this.vipRole &&this.vipPath))
    {
      //alert("please input all the field marked *");
    }
    else
    {
      this.progress.percentage = 0;
      this.currentFileUpload = this.selectedFiles.item(0)
      this.uploadService.pushFileToStorage(this.currentFileUpload, this.vipName, this.vipRole, this.vipPath).subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progress.percentage = Math.round(100 * event.loaded / event.total);
        } else if (event instanceof HttpResponse) {
          console.log('File is completely uploaded!');
          this.isVisible = false;
          this.getVipDetails();
        }
      });
      this.selectedFiles = undefined;
      return true;
    }  
  }




}



