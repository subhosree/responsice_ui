import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEvent, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { baseUrl } from './../dataInterface';

@Injectable()
export class UploadFileService {

  baseUrl = baseUrl.baseUrl; //'/api/usecaseui-server/v1';
  url = {
    vip: this.baseUrl + "/uui-vipservice/vipService"
  }
  constructor(private http: HttpClient) { }

  pushFileToStorage(file: File, vipName: string, vipRole: string, vipPath: string): Observable<HttpEvent<{}>> {
    let formdata: FormData = new FormData();

    formdata.append('file', file);
    formdata.append('vipName', vipName);
    formdata.append('vipRole', vipRole);
    formdata.append('vipPath', vipPath);

    const req = new HttpRequest('POST', this.url.vip, formdata, {
      reportProgress: true,
      responseType: 'text'
    });

    return this.http.request(req);
  }

  getFiles(): Observable<any[]> {
    return this.http.get<any[]>(this.url.vip);
  }
}
