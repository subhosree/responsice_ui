import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileCcvpnHomeComponent } from './mobile-ccvpn-home.component';

describe('MobileCcvpnHomeComponent', () => {
  let component: MobileCcvpnHomeComponent;
  let fixture: ComponentFixture<MobileCcvpnHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MobileCcvpnHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileCcvpnHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
