import { Component, OnInit, Input } from '@angular/core';
import { OrderserviceeService } from './../orderservicee.service';

@Component({
  selector: 'app-vas',
  templateUrl: './vas.component.html',
  styleUrls: ['./vas.component.less']
})
export class VasComponent implements OnInit{

  
  isInputVisible = true;
  checked = true;
  checkedv =false;
  checked1 = true;

  demoValue: number=3;
  @Input() orderServiceData;
  
  constructor(private myhttp: OrderserviceeService) { }

  ngOnInit(){;
    this.setSelectedVal();
  }

  demoValueVar = {
    status: "edit"
  }

  viewItem(): void {
    this.isInputVisible = !this.isInputVisible;
  }

  testCamera(demoValue1): void {
    this.demoValue = demoValue1;
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

   setSelectedVal(): void {
     this.demoValue = this.orderServiceData && this.orderServiceData.numOfCamera ? this.orderServiceData.numOfCamera : 3;
   
   }
}