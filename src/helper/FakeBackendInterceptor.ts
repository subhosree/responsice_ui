import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/materialize';
import 'rxjs/add/operator/dematerialize';

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {

    constructor() { }
    loopcnt: any = 0;
    bodyData = [];
    statusCnt:any =10;

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // array in local storage for registered users
        let sites: any[] = JSON.parse(localStorage.getItem('sites')) || [];

        // wrap in delayed observable to simulate server api call
        return Observable.of(null).mergeMap(() => {

            // create site
            if (request.url.endsWith('order/newSiteData') && request.method === 'POST') {
                // get new user object from post body
                let newSite = request.body;

                // validation
                // let duplicateUser = sites.filter(site => { return site.siteName === newSite.siteName; }).length;
                // if (duplicateUser) {
                //     return Observable.throw('SiteName "' + newSite.siteName + '" is already taken');
                // }

                // save new user
                // newSite.siteId = sites.length + 1;
                sites.push(newSite);
                localStorage.setItem('sites', JSON.stringify(sites));
                console.log(sites);
              
                // respond 200 OK
                return Observable.of(new HttpResponse({ status: 200 }));
            }

            if (request.url.endsWith('/ccvpn/vpnInfo') && request.method === 'GET') {
                let body = {
                    "bandWidth": "120 GBPS",
                    "wlanAccess": ["one", "two"],
                    "serviceCost": "$5000",
                    "cameras": "10",
                    "cameraCost": "$21",

                    "vpnInformations": [{
                        "vpnName": "Default Vpn",
                        "vpnId": "1",
                        "vpnType": "Hub-Spoke",
                        "vpnBandwidth": "100",
                        "vpnThreshold": "10",
                        "vpnCost": "$30",
                        "bandwidthCost": "$198",
                        "sites": [{
                            "siteId": "s1",
                            "siteName": "sitename1",
                            "isCloudSite": "1",
                            "location": "Bangalore",
                            "zipCode": "500001",
                            "role": "",
                            "capacity": "12",
                            "interface": "123",
                            "subnet": "1.1.1.1",
                            "price": "200",                           
                            "cameras": [{                                
                                "cameraId": "c1",
                                "cameraName": 'cam1',
                                "cameraLocation": 'Bangalore',
                                "cameraIP": '10.20.30.40',
                                "cameraSite": 'Bangalore'
                            },
                            {                                
                                "cameraId": "c2",
                                "cameraName": 'cam2',
                                "cameraLocation": 'Bangalore',
                                "cameraIP": '10.20.30.40',
                                "cameraSite": 'Bangalore'
                            },
                            {                                
                                "cameraId": "c3",
                                "cameraName": 'cam3',
                                "cameraLocation": 'Bangalore',
                                "cameraIP": '10.20.30.40',
                                "cameraSite": 'Bangalore'
                            },
                        ]
                        },
                        {
                            "siteId": "s2",
                            "siteName": "sitename2",
                            "isCloudSite": "2",
                            "location": "Bangalore",
                            "zipCode": "500001",
                            "role": "",
                            "capacity": "12",
                            "interface": "123",
                            "subnet": "1.1.1.1",
                            "price": "200",                           
                            "cameras": [{                                
                                "cameraId": "c4",
                                "cameraName": 'cam4',
                                "cameraLocation": 'Bangalore',
                                "cameraIP": '10.20.30.40',
                                "cameraSite": 'Bangalore'
                            },
                            {                                
                                "cameraId": "c5",
                                "cameraName": 'cam5',
                                "cameraLocation": 'Bangalore',
                                "cameraIP": '10.20.30.40',
                                "cameraSite": 'Bangalore'
                            },
                            {                                
                                "cameraId": "c6",
                                "cameraName": 'cam6',
                                "cameraLocation": 'Bangalore',
                                "cameraIP": '10.20.30.40',
                                "cameraSite": 'Bangalore'
                            },
                        ]
                        },
                    ]
                    } 
                ]
                };
                return Observable.of(new HttpResponse({ status: 200, body: body }));

            }

            //SotnInfo
            if (request.url.endsWith('/uui-lcm/Sotnservices/servicesubscription/SOTN/serviceinstance/500') && request.method === 'GET') {
                 let body = 
                //[
                //     {  
                //         "siteId":"256b1494-ebc7-41f8-b8c4-11210f3ee337",
                //         "siteName":"LondonONSDEMOBJHKCustomer",
                //         "description":"Beijing site",
                //         "isCloudSite":"null",
                //         "location":"London, Greater London, England, United Kingdom",
                //         "zipCode":"",
                //         "role":"dsvpn-hub",
                //         "capacity":"null",
                //         "interface":"HongKongInterface",
                //         "subnet":"192.168.0.0",
                //         "price":"12",
                //         "status":"Online",
                //         "statEnum":"GREEN_GREEN",
                //         "edgeName":"Beijing-lab-Edge",
                //         "edgeStatus":"Online",
                //         "edgeIP":"192.168.15.5",
                //         "deviceDesc":"Beijing spoke Device",
                //         "deviceName":"ONSDEMOBJHKCustomerBeijingDevice",
                //         "siteStatus":"Online",
                //         "type":"single-gateway",
                //         "address":"London, Greater London, England, United Kingdom",
                //         "email":"defaultEmail@gmail.com"
                //     },
                //     {  
                //         "siteId":"3801b392-f596-4f7b-93d6-4d0a33b014bc",
                //         "siteName":"HkHubONSDEMOBJHKCustomer",
                //         "description":"HongkongLabDesc",
                //         "isCloudSite":"null",
                //         "location":"Hongkong",
                //         "zipCode":"1000953",
                //         "role":"dsvpn-hub",
                //         "capacity":"null",
                //         "interface":"HongKongInterface",
                //         "subnet":"192.168.0.0",
                //         "price":"10",
                //         "status":"Activate",
                //         "statEnum":"GREY_GREY",
                //         "edgeName":"null",
                //         "edgeStatus":"Offline",
                //         "edgeIP":"null",
                //         "deviceDesc":"Hub Device",
                //         "deviceName":"ONSDEMOBJHKCustomerHkgHubDevice",
                //         "siteStatus":"Online",
                //         "type":"single-gateway",
                //         "address":"htiplBangalore",
                //         "email":"defaultEmail@gmail.com"
                //     }
                // ]
                {
                    "ethtSvcName": "vpn022",
                    "vpnInformations": {
                        "vpnThreshold": "1000",
                        "vpnBandwidth": "100",
                        "vpnType": "Hub-Spoke",
                        "sites": [{
                            "zipCode": "100008",
                            "address": "beijing",
                            "siteId": "d5b530fe-2271-4ed6-8c09-b7ce368a1f59",
                            "siteName": "ISAAC01-hub1",
                            "description": "desc"
                        }],
                        "vpnId": "a8529dee-523e-4c6b-bc83-e26d68a9911c",
                        "vpnName": "vpn022"
                    },
                    "colorAware": "true",
                    "cbs": "100",
                    "couplingFlag": "true",
                    "ebs": "EVPL",
                    "cir": "98900",
                    "eir": "1000"
                }
                return Observable.of(new HttpResponse({ status: 200, body: body }));

            }
            //topology
             //SotnInfo
             if (request.url.endsWith('/uui-lcm/Sotnservices/topology/service/service-subscriptions/service-subscription/SOTN/service-instances/service-instance/500') && request.method === 'GET') {
                let body = 
            //    [
            //        {  
            //            "siteId":"256b1494-ebc7-41f8-b8c4-11210f3ee337",
            //            "siteName":"LondonONSDEMOBJHKCustomer",
            //            "description":"Beijing site",
            //            "location":"London, Greater London, England, United Kingdom",
            //            "zipCode":"",
            //            "accessltpid": "151",
            //            "address":"London, Greater London, England, United Kingdom",
            //            "role":"dsvpn-hub",
            //            "accessnodeid": "10.10.10.10",
            //             "cvlan": "5"   
            //        },
            //        {  
            //            "siteId":"3801b392-f596-4f7b-93d6-4d0a33b014bc",
            //            "siteName":"HkHubONSDEMOBJHKCustomer",
            //            "description":"HongkongLabDesc",
            //            "address":"htiplBangalore",
            //            "accessltpid": "151",
            //            "location":"Hongkong",
            //            "zipCode":"1000953",
            //            "role":"dsvpn-hub",
            //             "accessnodeid": "10.10.10.10",
            //             "cvlan": "5" 

                       
            //        }
            //    ]
               
            [{
                "zipCode": "",
                "accessltpid": "155",
                "sotnSiteName": "ISAAC01-hub1",
                "role": "dsvpn-hub",
                "address": "London, Greater London, England, United Kingdom",
                "siteId": "d5b530fe-2271-4ed6-8c09-b7ce368a1f59",
                "description": "desc",
                "location": "London, Greater London, England, United Kingdom",
                "accessnodeid": "10.10.10.10",
                "cvlan": "200"
            }, {
                "accessltpid": "155",
                "zipCode": "1000953",
                "sotnSiteName": "SiteLondon",
                "role": "dsvpn-hub",
                "address": "htiplBangalore",
                "siteId": "12345",
                "description": "",
                "accessnodeid": "10.10.10.10",
                "location": "Hongkong",
                "cvlan": "cvlan"
            }];

            
                   
               return Observable.of(new HttpResponse({ status: 200, body: body }));

           }
             // getScription
            if (request.url.endsWith('/uui-lcm/customers/service-subscriptions') && request.method === 'GET') {
                let body = {
                    "subscriptions": [
                        {
                            "serviceType": "SDWAN",

                        },
                        {
                            "serviceType": "SOTN",

                        }
                    ]
                }
                return Observable.of(new HttpResponse({ status: 200, body: body }));

            }
            //SOTNDatamonitor

            if (request.url.indexOf('/uui-lcm/Sotnservices/bandwidth') > 0 && request.method === 'GET') {
                let body = {
                    "file":{  
                        "fileName":request.url.indexOf('02') > 0  ? 'sotn-1511-VS-monitored.png' : 'out-domain.png'
                     }
                };
                return Observable.of(new HttpResponse({ status: 200, body: body }));

            }

            //SotnInfo
            if (request.url.endsWith('/sotn/orderInfo') && request.method === 'GET') {
                let body = {
                    "service":{
                         "name": "serviceName1",
                         "description": "description1",
                         "startTime": "1/1/2001",
                         "endTime": "1/1/2001",
                         "cos": "cos1",
                         "reRoute": "reRoute1",
                         "specification": "specification1",
                         "dualLink": "duealLink",
                         "cir": "cir1",
                         "eir": "eir1",
                         "cbs": "specification1",
                         "ebs": "duealLink",
                         "colorAware": "cir1",
                         "couplingFlag": "eir1"
                    },
                    
                         "sites": [{
                         "siteId": "siteid2",
                         "sotnSiteName": "HTIPL",
                         "description": "1",
                         "zipCode": "500001",
                         "address": "Bangalore",
                         "vlan": "vlan1"
                         },
                         {
                         "siteId": "siteid2",
                         "sotnSiteName": "HTIPL",
                         "description": "2",
                         "zipCode": "500001",
                         "address": "Bangalore",
                         "vlan": "vlan2"
                         }
                    
                         ]
                        
                    };
                return Observable.of(new HttpResponse({ status: 200, body: body }));

            }
                //ServiceInstance
            if ((request.url.endsWith('/uui-lcm/Sotnservices/ServiceInstances/SOTN') || request.url.endsWith('/uui-lcm/Sotnservices/ServiceInstances/SDWAN'))  && request.method === 'GET') {
                let body = {
                    "serviceInstanceList": [
                        {
                            "serviceInstance": "ISAAC-IS02",
                            "serviceInstancename":"SiteService-5011"
                        },
                        {
                            "serviceInstance": "ISAAC-IS03",
                            "serviceInstancename":"ISAAC-IS0333"
                        }
                    ]
                }
                return Observable.of(new HttpResponse({ status: 200, body: body }));

            }
            if (request.url.endsWith('/ccvpn/sotn/data-monior') && request.method === 'GET') {
                let body = [  
                    {  
                       "sotnServcie":"sotn"
                    }
                 ]
                return Observable.of(new HttpResponse({ status: 200, body: body }));

            }

           
           
           
            // get Sites
            if (request.url.endsWith('/api/usecaseui-portal/order/sites') && request.method === 'GET') {
                return Observable.of(new HttpResponse({ status: 200, body: sites }));

            }

            if (request.url.endsWith('uui-vipservice/vipService') && request.method === 'GET') {
                var objD1 =[{vipName:'anjali',vipRole:'SE',imgUrl:"http://usecase-server/api/usecaseui/server/v1/uui-vipservice/vipImage/tets.png"},{vipName:'walsatwar',vipRole:'SE',imgUrl:"http://usecase-server/api/usecaseui/server/v1/uui-vipservice/vipImage/tets1.png"}];
                return Observable.of(new HttpResponse({ status: 200, body: objD1 }));

            }

            if (request.url.endsWith('uui-modelservice/modelData') && request.method === 'POST') {
               
                return Observable.of(new HttpResponse({ status: 200 }));
            }
           
            if (request.url.endsWith('/topology/sites') && request.method === 'GET') {
                var sites1=[
                    {  
                        "siteId":"a73a4c49-d7bd-46b7-9caa-226baeecb633",
                        "siteName":"Xian-labONSDEMOBJHKCustomer",
                        "description":"Xian-lab site",
                        "isCloudSite":"null",
                        "location":"Xian",
                        "zipCode":"710000",
                        "role":"dsvpn-hub",
                        "capacity":"null",
                        "interface":"HongKongInterface",
                        "subnet":"192.168.0.0",
                        "price":"12",
                        "status":"Online",
                        "statEnum":"GREEN_GREEN",
                        "edgeName":"Xian-lab-Edge",
                        "edgeStatus":"Online",
                        "edgeIP":"192.168.11.11",
                        "deviceDesc":"Shanghai-1 spoke Device",
                        "deviceName":"ONSDEMOBJHKCustomerShanghai-1",
                        "siteStatus":"Online",
                        "type":"single-gateway",
                        "address":"htiplBangalore",
                        "email":"defaultEmail@gmail.com"
                     },
                     {  
                        "siteId":"3801b392-f596-4f7b-93d6-4d0a33b014bc",
                        "siteName":"Hongkong",
                        "description":"HongkongLabDesc",
                        "isCloudSite":"null",
                        "location":"Hongkong",
                        "zipCode":"1000953",
                        "role":"sd-wan-edge",
                        "capacity":"null",
                        "interface":"HongKongInterface",
                        "subnet":"192.168.0.0",
                        "price":"10",
                        "status":"Activate",
                        "statEnum":"GREY_GREY",
                        "edgeName":"null",
                        "edgeStatus":"Offline",
                        "edgeIP":"null",
                        "deviceDesc":"Hub Device",
                        "deviceName":"ONSDEMOBJHKCustomerHkgHubDevice",
                        "siteStatus":"Online",
                        "type":"single-gateway",
                        "address":"htiplBangalore",
                        "email":"defaultEmail@gmail.com"
                     },
                     {  
                        "siteId":"256b1494-ebc7-41f8-b8c4-11210f3ee337",
                        "siteName":"Beijing",
                        "description":"Beijing site",
                        "isCloudSite":"null",
                        "location":"Beijing",
                        "zipCode":"100097",
                        "role":"sd-wan-edge",
                        "capacity":"null",
                        "interface":"HongKongInterface",
                        "subnet":"192.168.0.0",
                        "price":"12",
                        "status":"Online",
                        "statEnum":"GREEN_GREEN",
                        "edgeName":"Beijing-lab-Edge",
                        "edgeStatus":"Online",
                        "edgeIP":"192.168.15.5",
                        "deviceDesc":"Beijing spoke Device",
                        "deviceName":"ONSDEMOBJHKCustomerBeijingDevice",
                        "siteStatus":"Online",
                        "type":"single-gateway",
                        "address":"htiplBangalore",
                        "email":"defaultEmail@gmail.com"
                     },
                     {  
                        "siteId":"1652269c-6a96-464c-bdc8-f035f146b933",
                        "siteName":"Shanghai",
                        "description":"Shanghai-lab2 site",
                        "isCloudSite":"null",
                        "location":"Shanghai",
                        "zipCode":"200001",
                        "role":"sd-wan-edge",
                        "capacity":"null",
                        "interface":"HongKongInterface",
                        "subnet":"192.168.0.0",
                        "price":"12",
                        "status":"Online",
                        "statEnum":"GREEN_GREY",
                        "edgeName":"null",
                        "edgeStatus":"Offline",
                        "edgeIP":"null",
                        "deviceDesc":"Shanghai-2 spoke Device",
                        "deviceName":"ONSDEMOBJHKCustomerShanghai-2",
                        "siteStatus":"Online",
                        "type":"single-gateway",
                        "address":"htiplBangalore",
                        "email":"defaultEmail@gmail.com"
                     }
                ];
                return Observable.of(new HttpResponse({ status: 200, body: sites1 }));
            }

            if (request.url.indexOf('/uui-lcm/Sotnservices/topology') >0 && request.method === 'GET') {
                var sites1=[
                    {  
                        "siteId":"a73a4c49-d7bd-46b7-9caa-226baeecb633",
                        "siteName":"Xian-labONSDEMOBJHKCustomer",
                        "description":"Xian-lab site",
                        "isCloudSite":"null",
                        "location":"Xian",
                        "zipCode":"710000",
                        "role":"dsvpn-hub",
                        "capacity":"null",
                        "interface":"HongKongInterface",
                        "subnet":"192.168.0.0",
                        "price":"12",
                        "status":"Online",
                        "statEnum":"GREEN_GREEN",
                        "edgeName":"Xian-lab-Edge",
                        "edgeStatus":"Online",
                        "edgeIP":"192.168.11.11",
                        "deviceDesc":"Shanghai-1 spoke Device",
                        "deviceName":"ONSDEMOBJHKCustomerShanghai-1",
                        "siteStatus":"Online",
                        "type":"single-gateway",
                        "address":"htiplBangalore",
                        "email":"defaultEmail@gmail.com"
                     },
                     {  
                        "siteId":"3801b392-f596-4f7b-93d6-4d0a33b014bc",
                        "siteName":"Hongkong",
                        "description":"HongkongLabDesc",
                        "isCloudSite":"null",
                        "location":"Hongkong",
                        "zipCode":"1000953",
                        "role":"dsvpn-hub",
                        "capacity":"null",
                        "interface":"HongKongInterface",
                        "subnet":"192.168.0.0",
                        "price":"10",
                        "status":"Activate",
                        "statEnum":"GREY_GREY",
                        "edgeName":"null",
                        "edgeStatus":"Offline",
                        "edgeIP":"null",
                        "deviceDesc":"Hub Device",
                        "deviceName":"ONSDEMOBJHKCustomerHkgHubDevice",
                        "siteStatus":"Online",
                        "type":"single-gateway",
                        "address":"htiplBangalore",
                        "email":"defaultEmail@gmail.com"
                     }
                ];

                var site1 =[
                    {
                        "zipCode": "1000953",
                        "role": "dsvpn-hub",
                        "siteName": "hubtravel",
                        "description": "desc",
                        "location": "Hongkong",
                        "attribute": {
                            "cvlan": "100",
                            "Link Status" : "down"
                        }
                    },
                    {
                        "zipCode": "L2 2DP",
                        "role": "dsvpn-hub",
                        "address": "Liverpool",
                        "siteId": "5db5d32d-7329-44c1-b8ea-2a9498437f46",
                        "siteName": "Liverpool",
                        "description": "desc",
                        "location": "Liverpool",
                        "attribute": {
                            "accessltpid": "155",
                            "accessnodeid": "10.10.10.10",
                            "cvlan": "100",
                            "Link Status" : "down"                            
                        }
                    }
                ]

                var site2 = [{"zipCode":"28013","role":"dsvpn-hub","siteName":"Madrid","description":"Madrid","location":"Madrid",
                "attribute":{"Zip Code":"28013","Site Name":"Madrid","cvlan":"400","Link Status":"down","Location":"Madrid"}},
                {"zipCode":"NE1 4LF","role":"dsvpn-hub","address":"Newcastle","siteId":"2f4b3e25-bdd1-404d-bed5-381ced380318",
                "siteName":"Newcastle","description":"Newcastle","location":"Newcastle",
                "attribute":{"Zip Code":"NE1 4LF","Site Name":"Newcastle","cvlan":"400","Access node Id":"10.10.10.10","Access LTP Id":"149","Link Status":"down","Location":"Newcastle"}}]
                return Observable.of(new HttpResponse({ status: 200, body: site2 }));
            }            

            
            if (request.url.endsWith('ccvpncustomer/services') && request.method === 'GET') {
                var serviceData=[
                                    {
                                    "serviceInstanceId": "600",
                                    "serviceInstanceName": "600",
                                    "serviceType": "example-service-type-val-52265",
                                    "serviceRole": "example-service-role-val-24156",
                                    "resourceVersion": "1556088536117"
                                    },
                                    {
                                    "serviceInstanceId": "601",
                                    "serviceInstanceName": "601",
                                    "serviceType": "example1-service-type-val-52265",
                                    "serviceRole": "example1-service-role-val-24156",
                                    "resourceVersion": "1556088536117"
                                    }
                                ]
                return Observable.of(new HttpResponse({ status: 200, body: serviceData }));
            }
            
            if (request.url.endsWith("/ccvpncustomer/servicesubscriptions/uui-lcm/Sotnservices/servicesubscription/SDWAN/serviceinstance/600" ) && request.method === 'GET') {
                debugger
                var data2={
                    "bandWidth": "100",
                    "wlanAccess": [
                        "Internet"                        
                    ],
                    "serviceCost": "$133,863.00",
                    "cameraCost": "$21,269.00",
                    "vpnCost": "$30,689.00",
                    "bandwidthCost":"$198.00",
                    "vpnInformations": [
                            {
                                "vpnName": "vpnname",
                                "vpnId": "vpnid",
                                "vpnType": "vpntype",
                                "vpnBandwidth": "100",
                                "vpnThreshold": "10",
                                "cameras": "10",
                                "sites": [
                                            {  
                            "siteId":"a73a4c49-d7bd-46b7-9caa-226baeecb633",
                            "siteName":"Xian-labONSDEMOBJHKCustomer",
                            "description":"Xian-lab site",
                            "isCloudSite":"null",
                            "location":"Xian",
                            "zipCode":"710000",
                            "role":"dsvpn-hub",
                            "capacity":"null",
                            "interface":"HongKongInterface",
                            "subnet":"192.168.0.0",
                            "price":"12",
                            "status":"Online",
                            "statEnum":"GREEN_GREEN",
                            "edgeName":"Xian-lab-Edge",
                            "edgeStatus":"Online",
                            "edgeIP":"192.168.11.11",
                            "deviceDesc":"Shanghai-1 spoke Device",
                            "deviceName":"ONSDEMOBJHKCustomerShanghai-1",
                            "siteStatus":"Online",
                            "type":"single-gateway",
                            "address":"htiplBangalore",
                            "email":"defaultEmail@gmail.com"
                        },
                        {  
                            "siteId":"3801b392-f596-4f7b-93d6-4d0a33b014bc",
                            "siteName":"HkHubONSDEMOBJHKCustomer",
                            "description":"HongkongLabDesc",
                            "isCloudSite":"null",
                            "location":"Hongkong",
                            "zipCode":"1000953",
                            "role":"sd-wan-edge",
                            "capacity":"null",
                            "interface":"HongKongInterface",
                            "subnet":"192.168.0.0",
                            "price":"10",
                            "status":"Activate",
                            "statEnum":"GREY_GREY",
                            "edgeName":"null",
                            "edgeStatus":"Offline",
                            "edgeIP":"null",
                            "deviceDesc":"Hub Device",
                            "deviceName":"ONSDEMOBJHKCustomerHkgHubDevice",
                            "siteStatus":"Online",
                            "type":"single-gateway",
                            "address":"htiplBangalore",
                            "email":"defaultEmail@gmail.com"
                        },
                        {  
                            "siteId":"256b1494-ebc7-41f8-b8c4-11210f3ee337",
                            "siteName":"BeijingONSDEMOBJHKCustomer",
                            "description":"Beijing site",
                            "isCloudSite":"null",
                            "location":"Beijing",
                            "zipCode":"100097",
                            "role":"sd-wan-edge",
                            "capacity":"null",
                            "interface":"HongKongInterface",
                            "subnet":"192.168.0.0",
                            "price":"12",
                            "status":"Online",
                            "statEnum":"GREEN_GREEN",
                            "edgeName":"Beijing-lab-Edge",
                            "edgeStatus":"Online",
                            "edgeIP":"192.168.15.5",
                            "deviceDesc":"Beijing spoke Device",
                            "deviceName":"ONSDEMOBJHKCustomerBeijingDevice",
                            "siteStatus":"Online",
                            "type":"single-gateway",
                            "address":"htiplBangalore",
                            "email":"defaultEmail@gmail.com"
                        },
                        {  
                            "siteId":"1652269c-6a96-464c-bdc8-f035f146b933",
                            "siteName":"Shanghai-lab2ONSDEMOBJHKCustomer",
                            "description":"Shanghai-lab2 site",
                            "isCloudSite":"null",
                            "location":"Shanghai",
                            "zipCode":"200001",
                            "role":"sd-wan-edge",
                            "capacity":"null",
                            "interface":"HongKongInterface",
                            "subnet":"192.168.0.0",
                            "price":"12",
                            "status":"Online",
                            "statEnum":"GREEN_GREY",
                            "edgeName":"null",
                            "edgeStatus":"Offline",
                            "edgeIP":"null",
                            "deviceDesc":"Shanghai-2 spoke Device",
                            "deviceName":"ONSDEMOBJHKCustomerShanghai-2",
                            "siteStatus":"Online",
                            "type":"single-gateway",
                            "address":"htiplBangalore",
                            "email":"defaultEmail@gmail.com"
                        }
                            ],
                            "cost": "600"
                        }
                    ]
                };
                return Observable.of(new HttpResponse({ status: 200, body: data2 }));
            }
            
            if (request.url.endsWith('service/summary') && request.method === 'GET') {
                var d= {"bandWidth":"10","wlanAccess":["internet"],"serviceCost":"914","bandwidthCost":"120","cameraCost":"120","vpnCost":"594","vpnInformations":[{"vpnName":"Default VPN","vpnId":"1","vpnType":"Hub-Spoke","vpnBandwidth":"100","vpnThreshold":"50","cameras":"3","sites":[{"siteId":"a73a4c49-d7bd-46b7-9caa-226baeecb633","siteName":"Xian-lab","isCloudSite":"hub","location":"Xian","zipCode":"710000","role":"sd-wan-edge","capacity":"10GB","interface":"interface1","subnet":"192.168.0.0","price":"12","siteStatus":"Online"},{"siteId":"3801b392-f596-4f7b-93d6-4d0a33b014bc","siteName":"Hongkong","isCloudSite":"spoke","location":"Hongkong","zipCode":"1000953","role":"dsvpn-hub","capacity":"100GB","interface":"interface2","subnet":"192.168.0.0","price":"10","siteStatus":"Online"},{"siteId":"256b1494-ebc7-41f8-b8c4-11210f3ee337","siteName":"Beijing","isCloudSite":"spoke","location":"Beijing","zipCode":"100097","role":"sd-wan-edge","capacity":"1GB","interface":"interface3","subnet":"192.168.0.0","price":"12","siteStatus":"Online"},{"siteId":"1652269c-6a96-464c-bdc8-f035f146b933","siteName":"Shanghai-lab","isCloudSite":"spoke","location":"Shanghai","zipCode":"200001","role":"sd-wan-edge","capacity":"10GB","interface":"interface4","subnet":"192.168.0.0","price":"12","siteStatus":"Online"}],"cost":"166"}]};
               
                return Observable.of(new HttpResponse({ status: 200, body: d }));
            }
            

            if (request.url.endsWith('/uui-lcm/Sotnservices/cost') && request.method === 'POST') {
                var sotnCostSummary= {  
                    "ethtSvcName":"vpn022",
                    "colorAware":"true",
                    "cbs":"100",
                    "couplingFlag":"true",
                    "ebs":"EVPL",
                    "cir":"98900",
                    "eir":"1000",
                    "serviceCost": "1000$",
                    "cost":{
                        "serviceCost":"123.00", "vpnCost":"123.00", "siteCost":"123.00" },
                    "vpnInformations":[{  
                       "vpnThreshold":"1000",
                       "vpnBandwidth":"100",
                       "vpnType":"Hub-Spoke",
                       "vpnId":"a8529dee-523e-4c6b-bc83-e26d68a9911c",
                       "vpnName":"vpn022",
                       "sites":[  
                          {  
                             "zipCode":"100008",
                             "address":"beijing",
                             "siteId":"d5b530fe-2271-4ed6-8c09-b7ce368a1f59",
                             "siteName":"ISAAC01-hub1",
                             "description":"desc"
                          }
                       ]
                    }]
                 };
            
               
                return Observable.of(new HttpResponse({ status: 200, body: sotnCostSummary }));                
            }
            
            if (request.url.endsWith('/sdwan/finalPostOrderedServiceData') && request.method === 'POST') {
                return Observable.of(new HttpResponse({ status: 200, body: {} }));                
            } 

            if (request.url.endsWith('/uui-lcm/Sotnservices/servicesubscription/SOTN/serviceinstance/ISAAC-IS02') && request.method === 'GET') {
                var sotnSummary= {  
                    "ethtSvcName":"vpn022",
                    "colorAware":"true",
                    "cbs":"100",
                    "couplingFlag":"true",
                    "ebs":"EVPL",
                    "cir":"98900",
                    "eir":"1000",                    
                    "vpnInformations":[{  
                       "vpnThreshold":"1000",
                       "vpnBandwidth":"100",
                       "vpnType":"Hub-Spoke",
                       "vpnId":"a8529dee-523e-4c6b-bc83-e26d68a9911c",
                       "vpnName":"vpn022",
                       "sites":[  
                          {  
                             "zipCode":"100008",
                             "address":"beijing",
                             "siteId":"d5b530fe-2271-4ed6-8c09-b7ce368a1f59",
                             "siteName":"ISAAC01-hub1",
                             "description":"desc"
                          }
                       ]
                    }]
                 };
            
               
                return Observable.of(new HttpResponse({ status: 200, body: sotnSummary }));                
            } 

            if (request.url.endsWith('/uui-lcm/Sotnservices/servicesubscription/SOTN/serviceinstance/ISAAC-IS03') && request.method === 'GET') {
                var sotnSummary1= {  
                    "ethtSvcName":"vpn022",  
                    "cir":"98900",
                    "eir":"1000",                    
                    "vpnInformations":[{  
                       "vpnThreshold":"1000",
                       "vpnBandwidth":"100",
                       "vpnType":"Hub-Spoke",
                       "vpnId":"a8529dee-523e-4c6b-bc83-e26d68a9911c",
                       "vpnName":"vpn022",
                       "sites":[  
                          {  
                             "zipCode":"100008",
                             "address":"beijing",
                             "siteId":"d5b530fe-2271-4ed6-8c09-b7ce368a1f59",
                             "siteName":"ISAAC01-hub1",
                             "description":"desc"
                          }
                       ]
                    }]
                 };
            
               
                return Observable.of(new HttpResponse({ status: 200, body: sotnSummary1 }));                
            }


            if (request.url.endsWith('sdwan/resourceTopology5G') && request.method === 'GET') {
                var DIR = './assets/treeTopology/';

                    var  node = {
                        "nodes": [{
                        "id": "S",
                        "shape": "circularImage",
                        "image": "./assets/treeTopology/edge.png",
                        "label": "VNF Edge",
                        "color": "green",
                        "dataNode": {
                        "name": "VNF Edge",
                        "description": "VNF Edge",
                        "globalSubscriberId": "subscriberId",
                        "serviceType": "service-ccvpn",
                        "role": "null",
                        "publicIP": "null",
                        "ipMode": "null",
                        "portType": "null",
                        "portNumber": "null",
                        "transportNetworkName": "null",
                        "version": "null",
                        "type": "null",
                        "class": "null",
                        "systemIp": "null"
                        }
                        }, {
                        "id": "R1",
                        "shape": "circularImage",
                        "image": "./assets/treeTopology/virtual_link.png",
                        "label": "VL",
                        "color": "green",
                        "dataNode": {
                        "name": "VL",
                        "description": "VL",
                        "globalSubscriberId": "null",
                        "serviceType": "null",
                        "role": "Spoke",
                        "publicIP": "null",
                        "ipMode": "null",
                        "portType": "null",
                        "portNumber": "null",
                        "transportNetworkName": "null",
                        "version": "null",
                        "type": "null",
                        "class": "null",
                        "systemIp": "null"
                        }
                        }, {
                        "id": "R2",
                        "shape": "circularImage",
                        "image": "./assets/treeTopology/device1.png",
                        "label": "PNF CPE",
                        "color": "green",
                        "dataNode": {
                        "name": "PNF CPE",
                        "description": "PNF CPE",
                        "globalSubscriberId": "null",
                        "serviceType": "null",
                        "role": "Spoke",
                        "publicIP": "null",
                        "ipMode": "null",
                        "portType": "null",
                        "portNumber": "null",
                        "transportNetworkName": "null",
                        "version": "null",
                        "type": "null",
                        "class": "null",
                        "systemIp": "null"
                        }
                        }, {
                        "id": "R1C1",
                        "shape": "circularImage",
                        "image": "./assets/treeTopology/vpn-icon.jpg",
                        "label": "internet",
                        "color": "green",
                        "dataNode": {
                        "name": "internet",
                        "description": "null",
                        "globalSubscriberId": "null",
                        "serviceType": "null",
                        "role": "null",
                        "publicIP": "119.3.7.113",
                        "ipMode": "DHCP",
                        "portType": "GE",
                        "portNumber": "0/0/1",
                        "transportNetworkName": "internet",
                        "version": "null",
                        "type": "null",
                        "class": "null",
                        "systemIp": "null"
                        }
                        }, {
                        "id": "R1C3",
                        "shape": "circularImage",
                        "image": "./assets/treeTopology/device1.png",
                        "label": "PNF CPE",
                        "color": "green",
                        "dataNode": {
                        "name": "PNF CPE",
                        "description": "null",
                        "globalSubscriberId": "null",
                        "serviceType": "null",
                        "role": "null",
                        "publicIP": "null",
                        "ipMode": "null",
                        "portType": "null",
                        "portNumber": "null",
                        "transportNetworkName": "null",
                        "version": "1.0",
                        "type": "AR1000V",
                        "class": "VNF",
                        "systemIp": "20.20.20.1"
                        }
                        }, {
                        "id": "R2C1",
                        "shape": "circularImage",
                        "image": "./assets/treeTopology/virtual_link.png",
                        "label": "VL",
                        "color": "green",
                        "dataNode": {
                        "name": "VL",
                        "description": "null",
                        "globalSubscriberId": "null",
                        "serviceType": "null",
                        "role": "null",
                        "publicIP": "119.3.7.113",
                        "ipMode": "DHCP",
                        "portType": "GE",
                        "portNumber": "0/0/1",
                        "transportNetworkName": "internet",
                        "version": "null",
                        "type": "null",
                        "class": "null",
                        "systemIp": "null"
                        }
                        }, {
                        "id": "R2C2",
                        "shape": "circularImage",
                        "image": "./assets/treeTopology/core.jpg",
                        "label": "VNF Core",
                        "color": "green",
                        "dataNode": {
                        "name": "VNF Core",
                        "description": "null",
                        "globalSubscriberId": "null",
                        "serviceType": "null",
                        "role": "null",
                        "publicIP": "119.3.7.113",
                        "ipMode": "DHCP",
                        "portType": "GE",
                        "portNumber": "0/0/1",
                        "transportNetworkName": "mpls",
                        "version": "null",
                        "type": "null",
                        "class": "null",
                        "systemIp": "null"
                        }
                        }, {
                        "id": "R1C2",
                        "shape": "circularImage",
                        "image": "./assets/treeTopology/vpn-icon.jpg",
                        "label": "5G-network-slice",
                        "color": "green",
                        "dataNode": {
                        "name": "5G-network-slice",
                        "description": "5G network slice creation and deletion",
                        "globalSubscriberId": "null",
                        "serviceType": "null",
                        "role": "null",
                        "publicIP": "null",
                        "ipMode": "null",
                        "portType": "null",
                        "portNumber": "null",
                        "transportNetworkName": "null",
                        "version": "null",
                        "type": "5G-network-slice",
                        "class": "null",
                        "systemIp": "null"
                        }
                        }],
                        "edges": [{
                        "from": "S",    
                        "to": "R1"
                        }, {
                        "from": "R1",
                        "to": "R2"
                        }, {
                        "from": "R1C1",
                        "to": "R2"
                        }, {
                        "from": "R1C1",
                        "to": "R1C3"
                        }, {
                        "from": "R1C3",
                        "to": "R2C1"
                        }, {
                        "from": "R2C1",
                        "to": "R2C2"
                        }, {
                        "from": "R1C2",
                        "to": "R1C3"
                        }, {
                        "from": "R2",
                        "to": "R1C2"
                        }]
                        }
                        
                        
                    return Observable.of(new HttpResponse({ status: 200, body: node }));
            }

            if ((request.url.indexOf('/uui-lcm/Sotnservices/resourceTopology/')>0) && request.method === 'GET') {
                var DIR = './assets/treeTopology/';

                    var  node1 = {
                        "nodes": [
                            {
                                "id": "IS01",
                                "shape": "circularImage",
                                "image": "./assets/treeTopology/service.png",
                                "label": "Infra Service",
                                "color": "Green",
                                "dataNode": {}
                            },
                            {
                                "id": "SC1",
                                "shape": "circularImage",
                                "image": "./assets/treeTopology/connectivity.png",
                                "label": "Connectivity",
                                "color": "Green",
                                "dataNode": {
                                    "ethtSvcName": "sotn-021-VS-monitored",
                                    "colorAware": "true",
                                    "cbs": "100",
                                    "couplingFlag": "true",
                                    "ebs": "evpl",
                                    "cir": "200000",
                                    "eir": "0"
                                }
                            },
                            {
                                "id": "IS_SP",
                                "shape": "circularImage",
                                "image": "./assets/treeTopology/partner.png",
                                "label": "Service Partner",
                                "color": "Green",
                                "dataNode": {
                                    "ethtSvcName": "sotn-021-VS-monitored",
                                    "colorAware": "true",
                                    "cbs": "100",
                                    "couplingFlag": "true",
                                    "ebs": "evpl",
                                    "cir": "200000",
                                    "eir": "0"
                                }
                            },
                            {
                                "id": "REMOTE_SITE",
                                "shape": "circularImage",
                                "image": "./assets/treeTopology/remotesite.png",
                                "label": "Remote Site",
                                "color": "Green",
                                "dataNode": {
                                    "zipcode": "100095",
                                    "siteName": "hubtravel",
                                    "description": "desc",
                                    "location": "laptop-5",
                                    "cvlan": "100"
                                }
                            },
                            {
                                "id": "LOCAL_SITE",
                                "shape": "circularImage",
                                "image": "./assets/treeTopology/site.png",
                                "label": "Local Site",
                                "color": "Green",
                                "dataNode": {
                                    "accessltpid": "155",
                                    "siteName": "hubtravel",
                                    "description": "desc",
                                    "accessnodeid": "10.10.10.10",
                                    "cvlan": "100"
                                }
                            },
                            {
                                "id": "LOCAL_ALLOT",
                                "shape": "circularImage",
                                "image": "./assets/treeTopology/allotted.png",
                                "label": "Allotted Resource",
                                "color": "Green",
                                "dataNode": {
                                    "aaccess-topology-id": "100",
                                    "vpn-name": "sotn-021-VS-monitored",
                                    "access-provider-id": "5555",
                                    "access-node-id": "10.10.10.10",
                                    "access-ltp-id": "19",
                                    "cvlan": "100",
                                    "access-client-id": "6666"
                                }
                            },
                            {
                                "id": "SITE_PINT",
                                "shape": "circularImage",
                                "image": "./assets/treeTopology/tpoint.png",
                                "label": "Termination Point",
                                "color": "Green",
                                "dataNode": {
                                    "interface-name": "nodeId-10.10.10.10-ltpId-19",
                                    "speed-value": "1000000",
                                    "equipment-identifier": "0.188.0.6",
                                    "resource-version": "1561093393411",
                                    "in-maint": true,
                                    "network-ref": "providerId-5555-clientId-6666-topologyId-33",
                                    "transparent": "true",
                                    "operational-status": "Down"
                                }
                            },
                            {
                                "id": "VPN_01",
                                "shape": "circularImage",
                                "image": "./assets/treeTopology/vpnbinding.png",
                                "label": "VPN Binding",
                                "color": "Green",
                                "dataNode": {
                                    "vpn-id": "10.10.10.10-sotn-021-VS-monitored",
                                    "vpn-name": "10.10.10.10-sotn-021-VS-monitored",
                                    "vpn-type": "ethernet",
                                    "vpn-region": "assured",
                                    "access-provider-id": "5555",
                                    "access-client-id": "6666",
                                    "access-topology-id": "100",
                                    "src-access-node-id": "10.10.10.10",
                                    "src-access-ltp-id": "19",
                                    "dst-access-node-id": "10.10.10.10",
                                    "dst-access-ltp-id": "155",
                                    "operational-status": "Created",
                                    "resource-version": "1561113264652"
                                }
                            },
                            {
                                "id": "VPN_PINT",
                                "shape": "circularImage",
                                "image": "./assets/treeTopology/tpoint.png",
                                "label": "Termination Pint",
                                "color": "Green",
                                "dataNode": {
                                    "interface-name": "nodeId-10.10.10.10-ltpId-155",
                                    "resource-version": "1561112085705",
                                    "in-maint": false,
                                    "operational-status": "overloaded"
                                }
                            },
                            {
                                "id": "LOG_LINK",
                                "shape": "circularImage",
                                "image": "./assets/treeTopology/logicallink.png",
                                "label": "Logical Link",
                                "color": "Green",
                                "dataNode": {
                                    "link-name": "cross-link-3",
                                    "in-maint": false,
                                    "link-type": "cross-link",
                                    "resource-version": "1561112085849",
                                    "operational-status": "overloaded"
                                }
                            },
                            {
                                "id": "EXT_AAI",
                                "shape": "circularImage",
                                "image": "./assets/treeTopology/extaai.png",
                                "label": "External AAI",
                                "color": "Green",
                                "dataNode": {
                                    "Name": "VDF-ext-net"
                                }
                            }
                        ],
                        "edges": [
                            {
                                "from": "IS01",
                                "to": "SC1"
                            },
                            {
                                "from": "IS01",
                                "to": "IS_SP"
                            },
                            {
                                "from": "IS01",
                                "to": "REMOTE_SITE"
                            },
                            {
                                "from": "IS01",
                                "to": "LOCAL_SITE"
                            },
                            {
                                "from": "LOCAL_SITE",
                                "to": "LOCAL_ALLOT"
                            },
                            {
                                "from": "LOCAL_ALLOT",
                                "to": "SITE_PINT"
                            },
                            {
                                "from": "SC1",
                                "to": "VPN_01"
                            },
                            {
                                "from": "VPN_01",
                                "to": "VPN_PINT"
                            },
                            {
                                "from": "VPN_PINT",
                                "to": "LOG_LINK"
                            },
                            {
                                "from": "LOG_LINK",
                                "to": "EXT_AAI"
                            }
                        ]
                    }
                    ;

                    var node2 ={  
                        "nodes":[  
                           {  
                              "id":"73022190-d634-4824-aaac-1435f73cbd47",
                              "shape":"circularImage",
                              "image":"./assets/treeTopology/connectivity.png",
                              "label":"Connectivity",
                              "color":"Green",
                              "dataNode":{  
                                 "ethtSvcName":"SOTN_9001_TS2_assured",
                                 "colorAware":"null",
                                 "cbs":"null",
                                 "couplingFlag":"null",
                                 "ebs":"evpl",
                                 "cir":"20000",
                                 "eir":"20000"
                              }
                           },
                           {  
                              "id":"10.10.10.10-SOTN_9001_TS2_assured",
                              "shape":"circularImage",
                              "image":"./assets/treeTopology/vpnbinding.png",
                              "label":"VPN Binding",
                              "color":"Green",
                              "dataNode":{  
                                 "vpn-id":"10.10.10.10-SOTN_9001_TS2_assured",
                                 "vpn-name":"10.10.10.10-SOTN_9001_TS2_assured",
                                 "vpn-type":"ethernet",
                                 "vpn-region":"assured",
                                 "access-provider-id":"5555",
                                 "access-client-id":"6666",
                                 "access-topology-id":"100",
                                 "src-access-node-id":"10.10.10.10",
                                 "src-access-ltp-id":"58",
                                 "dst-access-node-id":"10.10.10.10",
                                 "dst-access-ltp-id":"149",
                                 "operational-status":"Created",
                                 "resource-version":"1561389584516"
                              }
                           },
                           {  
                              "id":"10.10.10.10",
                              "shape":"circularImage",
                              "image":"./assets/treeTopology/accessnode.png",
                              "label":"Access Node",
                              "color":"Green",
                              "dataNode":{  
                                 "pnf-name":"10.10.10.10",
                                 "pnf-id":"10.10.10.10",
                                 "in-maint":true,
                                 "resource-version":"1561303054088",
                                 "admin-status":"up",
                                 "operational-status":"up"
                              }
                           },
                           {  
                              "id":"nodeId-10.10.10.10-ltpId-58",
                              "shape":"circularImage",
                              "image":"./assets/treeTopology/tpoint.png",
                              "label":"Termination Point",
                              "color":"Green",
                              "dataNode":{  
                                 "interface-name":"nodeId-10.10.10.10-ltpId-58",
                                 "speed-value":"1000000",
                                 "equipment-identifier":"0.188.0.8",
                                 "resource-version":"1561303102525",
                                 "in-maint":true,
                                 "network-ref":"providerId-5555-clientId-6666-topologyId-33",
                                 "transparent":"true",
                                 "operational-status":"down"
                              }
                           },
                           {  
                              "id":"cross-link-5",
                              "shape":"circularImage",
                              "image":"./assets/treeTopology/logicallink.png",
                              "label":"Logical Link",
                              "color":"Green",
                              "dataNode":{  
                                 "link-name":"cross-link-5",
                                 "in-maint":false,
                                 "link-type":"cross-link",
                                 "resource-version":"1561284481346",
                                 "operational-status":"up"
                              }
                           },
                           {  
                              "id":"EXT_AAI",
                              "shape":"circularImage",
                              "image":"./assets/treeTopology/extaai.png",
                              "label":"External AAI",
                              "color":"Green",
                              "dataNode":{  
                                 "Name":"VDF-ext-net"
                              }
                           }
                        ],
                        "edges":[  
                           {  
                              "from":"73022190-d634-4824-aaac-1435f73cbd47",
                              "to":"10.10.10.10-SOTN_9001_TS2_assured"
                           },
                           {  
                              "from":"10.10.10.10-SOTN_9001_TS2_assured",
                              "to":"nodeId-10.10.10.10-ltpId-149"
                           },
                           {  
                              "from":"10.10.10.10-SOTN_9001_TS2_assured",
                              "to":"nodeId-10.10.10.10-ltpId-58"
                           },
                           {  
                              "from":"nodeId-10.10.10.10-ltpId-58",
                              "to":"cross-link-5"
                           },
                           {  
                              "from":"null",
                              "to":"EXT_AAI"
                           }
                        ]
                     };
                    return Observable.of(new HttpResponse({ status: 200, body: node2 }));
            }
           

            if (request.url.endsWith('ccvpncustomer/services') && request.method === 'GET') {
                var serviceData=[
                                    {
                                    "serviceInstanceId": "600",
                                    "serviceInstanceName": "600",
                                    "serviceType": "example-service-type-val-52265",
                                    "serviceRole": "example-service-role-val-24156",
                                    "resourceVersion": "1556088536117"
                                    },
                                    {
                                    "serviceInstanceId": "601",
                                    "serviceInstanceName": "601",
                                    "serviceType": "example1-service-type-val-52265",
                                    "serviceRole": "example1-service-role-val-24156",
                                    "resourceVersion": "1556088536117"
                                    }
                                ]
                return Observable.of(new HttpResponse({ status: 200, body: serviceData }));
            }


            if (request.url.endsWith('/ccvpn/summary') && request.method === 'GET') {
                var data1={
                    "bandWidth": "100",
                    "wlanAccess": [
                        "Internet"                        
                    ],
                    "serviceCost": "$133,863.00",
                    "cameraCost": "$21,269.00",
                    "vpnCost": "$30,689.00",
                    "bandwidthCost":"$198.00",
                    "vpnInformations": [
                        {
                            "vpnName": "vpnname",
                            "vpnId": "vpnid",
                            "vpnType": "vpntype",
                            "vpnBandwidth": "100",
                            "vpnThreshold": "10",
                            "cameras": "10",
                            "sites": [
                                {
                                    "siteId": 1,
                                    "siteName": "Beijing HQ",
                                    "isCloudSite": "1",
                                    "location": "Chaoyang District, Beijing, PRC",
                                    "zipCode": "100010",
                                    "role": "Spoke",
                                    "capacity": "10G",
                                    "interface": "GEO/0/3",
                                    "subnet": "192.168.1.1/24",
                                    "prize": "200",
                                    "activeStatus":"online"
                                },                                
                                {
                                    "siteId": 4,
                                    "siteName": "Hong Kong",
                                    "isCloudSite": "0",
                                    "location": "Hong Kong",
                                    "zipCode": "518000",
                                    "role": "Hub",
                                    "capacity": "12",
                                    "interface": "GEO/0/3",
                                    "subnet": "192.168.4.1/24",
                                    "prize": "200",
                                    "activeStatus":"online"
                                },
                                {
                                    "siteId": 2,
                                    "siteName": "USA",
                                    "isCloudSite": "0",
                                    "location": "Morgan",
                                    "zipCode": "95037",
                                    "role": "Spoke",
                                    "capacity": "10G",
                                    "interface": "GEO/0/3",
                                    "subnet": "192.168.2.1/24",
                                    "prize": "200",
                                    "activeStatus":"offline"
                                },
                                {
                                    "siteId": 3,
                                    "siteName": "shanghai",
                                    "isCloudSite": "0",
                                    "location": "shanghai",
                                    "zipCode": "200000",
                                    "role": "Spoke",
                                    "capacity": "10G",
                                    "interface": "GEO/0/3",
                                    "subnet": "192.168.3.1/24",
                                    "prize": "200",
                                    "activeStatus":"offline"
                                }
                            ],
                            "cost": "600"
                        }
                    ]
                };
                return Observable.of(new HttpResponse({ status: 200, body: data1 }));
            }

            if (request.url.endsWith('/service/orderService') && request.method === 'POST') {          
                if(request.body.finalPost)
                {
                  
                }
                var data ={
                    "bandWidth": "120",
                    "wlanAccess": [
                        "Internet",
                        "MPLS"
                    ],
                    "serviceCost": "$133,863.00",
                    "cameraCost": "$21,269.00",
                    "vpnCost": "$30,689.00",
                    "bandwidthCost":"$198.00",
                    "vpnInformations": [
                        {
                            "vpnName": "vpnname",
                            "vpnId": "vpnid",
                            "vpnType": "vpntype",
                            "vpnBandwidth": "120",
                            "vpnThreshold": "10",
                            "cameras": "10",
                            "sites": [
                                {
                                    "siteId": "siteid",
                                    "siteName": "HTIPL",
                                    "isCloudSite": "1",
                                    "location": "Bangalore",
                                    "zipCode": "500001",
                                    "role": "",
                                    "capacity": "12",
                                    "interface": "123",
                                    "subnet": "1.1.1.1",
                                    "price": "200",
                                    "description": "description1",
                                    "site_address": "bangalore1",
                                    "site_email": "site1@gmail.com"
                                },
                                {
                                    "siteId": "siteid",
                                    "siteName": "sitename",
                                    "isCloudSite": "1",
                                    "location": "Bangalore",
                                    "zipCode": "500001",
                                    "role": "",
                                    "capacity": "12",
                                    "interface": "123",
                                    "subnet": "1.1.1.1",
                                    "price": "200",
                                    "description": "description2",
                                    "site_address": "bangalore2",
                                    "site_email": "site2@gmail.com"
                                }
                            ],
                            "cost": "600"
                        }
                    ]
                }
                return Observable.of(new HttpResponse({ status: 200, body:data }));
            }

            if (request.url.endsWith('/sdwan/finalPostOrderedServiceData') && request.method === 'POST') {          
                if(request.body.finalPost)
                {
                  
                }
                var data ={
                    "bandWidth": "120",
                    "wlanAccess": [
                        "Internet",
                        "MPLS"
                    ],
                    "serviceCost": "$133,863.00",
                    "cameraCost": "$21,269.00",
                    "vpnCost": "$30,689.00",
                    "bandwidthCost":"$198.00",
                    "vpnInformations": [
                        {
                            "vpnName": "vpnname",
                            "vpnId": "vpnid",
                            "vpnType": "vpntype",
                            "vpnBandwidth": "120",
                            "vpnThreshold": "10",
                            "cameras": "10",
                            "sites": [
                                {
                                    "siteId": "siteid",
                                    "siteName": "HTIPL",
                                    "isCloudSite": "1",
                                    "location": "Bangalore",
                                    "zipCode": "500001",
                                    "role": "",
                                    "capacity": "12",
                                    "interface": "123",
                                    "subnet": "1.1.1.1",
                                    "price": "200",
                                    "description": "description1",
                                    "site_address": "bangalore1",
                                    "site_email": "site1@gmail.com"
                                },
                                {
                                    "siteId": "siteid",
                                    "siteName": "sitename",
                                    "isCloudSite": "1",
                                    "location": "Bangalore",
                                    "zipCode": "500001",
                                    "role": "",
                                    "capacity": "12",
                                    "interface": "123",
                                    "subnet": "1.1.1.1",
                                    "price": "200",
                                    "description": "description2",
                                    "site_address": "bangalore2",
                                    "site_email": "site2@gmail.com"
                                }
                            ],
                            "cost": "600"
                        }
                    ]
                }
                return Observable.of(new HttpResponse({ status: 200, body:data }));
            }

            if (request.url.endsWith('/service/finalPostOrderServiceData') && request.method === 'POST') {
          
                            
                if(request.body.finalPost)
                {
                   
                }
                var data ={
                    "bandWidth": "100",
                    "wlanAccess": [
                        "Internet",
                        "MPLS"
                    ],
                    "serviceCost": "$133,863.00",
                    "cameraCost": "$21,269.00",
                    "vpnCost": "$30,689.00",
                    "bandwidthCost":"$198.00",
                    "vpnInformations": [
                        {
                            "vpnName": "vpnname",
                            "vpnId": "vpnid",
                            "vpnType": "vpntype",
                            "vpnBandwidth": "100",
                            "vpnThreshold": "10",
                            "cameras": "10",
                            "sites": [
                                {
                                    "siteId": "siteid",
                                    "siteName": "HTIPL",
                                    "isCloudSite": "1",
                                    "location": "Bangalore",
                                    "zipCode": "500001",
                                    "role": "",
                                    "capacity": "12",
                                    "interface": "123",
                                    "subnet": "1.1.1.1",
                                    "price": "200",
                                    "description": "description1",
                                    "site_address": "bangalore1",
                                    "site_email": "site1@gmail.com"
                                },
                                {
                                    "siteId": "siteid",
                                    "siteName": "sitename",
                                    "isCloudSite": "1",
                                    "location": "Bangalore",
                                    "zipCode": "500001",
                                    "role": "",
                                    "capacity": "12",
                                    "interface": "123",
                                    "subnet": "1.1.1.1",
                                    "price": "200",
                                    "description": "description2",
                                    "site_address": "bangalore2",
                                    "site_email": "site2@gmail.com"
                                }
                            ],
                            "cost": "600"
                        }
                    ]
                }
                return Observable.of(new HttpResponse({ status: 200, body:data }));
            }



            // get Sites
            if (request.url.endsWith('/api/usecaseui-portal/order/sites') && request.method === 'GET') {
                return Observable.of(new HttpResponse({ status: 200, body: sites }));

            }

            // get site by id
            if (request.url.endsWith('/uui-modelservice/modelData') && request.method === 'GET') {                

                var bodyObj = {
                    "edgeNameList": [{
                            "name": "edge1",
                            "value": "EdgeController1"
                        }, {
                            "name": "edge2",
                            "value": "EdgeController2"
                        }, {
                            "name": "edge3",
                            "value": "EdgeController3"
                        }
                
                    ],
                    "modelInfoList": [{
                            "modelName": "Huangxiangyu-Liyin",
                            "createdTime": "2019-01-29 20:09:59",
                            "description": "Xiangyu-Liyin's model",
                            "url" : "ftp://root:root@pureftpd:21/model/huangxiangyu-liyin.pkl"
                        },
                        {
                            "modelName": "Libu-Rama",
                            "createdTime": "2019-01-29 21:00:39",
                            "description": "Libu-Rama's model",
                            "url" : "ftp://root:root@pureftpd:21/model/libu-rama.pkl"
                        },
                        {
                            "modelName": "Prakash E",
                            "createdTime": "2019-01-29 22:15:36",
                            "description": "Prakash's model",
                            "url" : "ftp://root:root@pureftpd:21/model/prakash.pkl"
                        },                
                        {
                            "modelName": "Shashikanth V H",
                            "createdTime": "2019-01-29 23:10:20",
                            "description": "Shashikanth's model",
                            "url" : "ftp://root:root@pureftpd:21/model/shashikanth.pkl"
                        }
                
                
                    ]
                }
                return Observable.of(new HttpResponse({ status: 200, body: bodyObj }));

            }

            // get site by id
            if (request.url.match(/\/api\/usecaseui-portal\/order\/sites\/\d+$/) && request.method === 'GET') {

                // find user by id in users array
                let urlParts = request.url.split('/');
                let id = parseInt(urlParts[urlParts.length - 1]);
                let matchedSites = sites.filter(site => { return site.id === id; });
                let site = matchedSites.length ? matchedSites[0] : null;
                return Observable.of(new HttpResponse({ status: 200, body: site }));

            }

            // delete site
            if (request.url.match(/\/api\/usecaseui-portal\/order\/sites\/\d+$/) && request.method === 'DELETE') {
                let urlParts = request.url.split('/');
                let id = parseInt(urlParts[urlParts.length - 1]);
                for (let i = 0; i < sites.length; i++) {
                    let site = sites[i];
                    if (site.id === id) {
                        // delete user
                        sites.splice(i, 1);
                        localStorage.setItem('sites', JSON.stringify(sites));
                        break;
                    }
                }
                // respond 200 OK
                return Observable.of(new HttpResponse({ status: 200 }));

            }

            if (request.url.indexOf('/uui-lcm/Sotnservices/servicesubscription') > 0 && request.method === 'DELETE') {
                // respond 200 OK
                debugger;
                return Observable.of(new HttpResponse({ status: 200 }));

            }
            // get Sites
            if (request.url.endsWith('/service/site') && request.method === 'POST') {
               
                let siteData = {
                    interface:"192.168.1.1/24",
                    subnet:"GEO/0/3",
                    price:"5000",

                };
                 return Observable.of(new HttpResponse({ status: 200, body: siteData }));

            }

            if (request.url.endsWith('/uui-datamonitor/getDataMonitorData/sotn/service-instances/service-instance/') && request.method === 'GET') {
               
                let siteData = {
                    interface:"192.168.1.1/24",
                    subnet:"GEO/0/3",
                    price:"5000",

                };
                return Observable.of(new HttpResponse({ status: 200, body: siteData }));

            }
                        
            if (request.url.endsWith('/uui-lcm/Sotnservices') && request.method === 'POST') {
               
                let siteData = {
                    "service": {
                        "serviceId": "244be53a-f84a-42e0-8aba-3ff2bf4c7347",
                        "operationId": "dd16f580-455e-4690-969b-2894effd3395"
                    }
                }
                var start = new Date().getTime();
                for (var i = 0; i < 1e7; i++) {                    
                    if ((new Date().getTime() - start) > 500000){
                        break;
                    }
                }
                
                //setTimeout(() => {
                    return Observable.of(new HttpResponse({ status: 200, body: siteData }));
                //}, 5000);    
            }

            if (request.url.endsWith('/uui-lcm/Sotnservices/serviceStatus/service-instance/244be53a-f84a-42e0-8aba-3ff2bf4c7347') && request.method === 'GET') {
               
                
                if(this.statusCnt < 100)
                {
                    this.statusCnt+=10;
                }
                else
                {
                    this.statusCnt = 10;
                }
                let siteData = {
                    "status": this.statusCnt < 100 ? "0" : "1"
                }
                return Observable.of(new HttpResponse({ status: 200, body: siteData }));

            }

            if (request.url.endsWith('/order/vpn') && request.method === 'GET') {
                let body = [
                    {
                        "vpnId": 1,
                        "vpnName": "Default VPN",
                        "type": "Hub-Spoke"
                    }

                ];

                return Observable.of(new HttpResponse({ status: 200, body: body }));

            }

            if (request.url.endsWith('/api/usecaseui-portal/order/getFakeBackEndData') && request.method == 'GET') {
             
                let body = [
                    {
                        siteId: "123456",
                        siteName: "Beijingggg",
                        isCloudSite: "1",
                        location: "shanghai",
                        postCode: "4564543",
                        role: "spoke",
                        capacity: "10 Gigabyte",
                        interface: "192.167.12.21",
                        subnet: "GEO/O/3"
                    },
                    {
                        siteId: "1234577",
                        siteName: "Ceijing",
                        isCloudSite: "0",
                        location: "lohario",
                        postCode: "42321343",
                        role: "hub",
                        capacity: "100 Gigabyte",
                        interface: "192.167.12.21",
                        subnet: "GEO/O/3"

                    }
                ];

                return Observable.of(new HttpResponse({ status: 200, body: body }));
            }

            //vas
            if (request.url.endsWith('/ccvpn/vas') && request.method === 'GET') {
                let body = {
                    "noOfCamera": 3
                };

                return Observable.of(new HttpResponse({ status: 200, body: body }));

            }



            //bandwidth 
            if (request.url.endsWith('/ccvpn/bandwidth') && request.method === 'GET') {
                let body = [
                    {
                        "vpnId": 1,
                        "vpnName": "Default VPN",
                        "Bandwidth": 100,
                        "Threshold": 50
                    }
                ];

                return Observable.of(new HttpResponse({ status: 200, body: body }));

            }

            if (request.url.endsWith('/ccvpn/activateEdge') && request.method === 'POST') {
               
                return Observable.of(new HttpResponse({ status: 200 }));
            }
            //vipService


            if (request.url.endsWith('/ccvpn/activateEdge') && request.method === 'GET') {
                this.loopcnt++;
                var objD;
                var status="";
                for(var i=0;i<this.loopcnt;i++)
                {
                    status=status+"Anjali Walsatwar, Huawei Technologies India Pvt Ltd"+i+"\n";
                }
                if(this.loopcnt < 40)
                {
                    objD = {done:false,status:status};                    
                }
                else
                {
                    objD = {done:true,status:status};
                    this.loopcnt = 0;
                }
                return Observable.of(new HttpResponse({ status: 200, body: objD }));
            }
            
            if (request.url.endsWith('/uui-datamonitor/getDataMonitorData') && request.method === 'GET') {
                this.loopcnt++;
                var t = new Date();

                if (this.loopcnt == 1) {
                    for (var i = 0; i < 10; i++) {
                        t.setSeconds(t.getSeconds() - 1);
                        var obj = {
                            bandwidth: i % 3 == 0 ? 7000 + (Math.floor(Math.random() * 1000) + 1) : (t.getSeconds() == 0) ? (Math.floor(Math.random() * 5000) + 1) : (Math.floor(Math.random() * 3000) + 1),
                            changeTime: t.getDate() + "-" + (t.getMonth() + 1) + "-19" + " " + t.getHours() + ":" + t.getMinutes() + ":" + t.getSeconds()
                        }
                        this.bodyData.push(obj);
                    }
                    this.bodyData = this.bodyData.reverse();
                }
                else {
                    this.bodyData = this.bodyData.slice(Math.max(this.bodyData.length - 9, 1));
                    var obj = {
                        bandwidth: this.loopcnt % 2 == 0 ? (Math.floor(Math.random() * 1000) + 1) : (Math.floor(Math.random() * 3000) + 1),
                        changeTime: t.getDate() + "-" + (t.getMonth() + 1) + "-19" + " " + t.getHours() + ":" + t.getMinutes() + ":" + t.getSeconds()
                    }
                    this.bodyData.push(obj);
                }
                let body = this.bodyData;


                return Observable.of(new HttpResponse({ status: 200, body: body }));

            }



            // pass through any requests not handled above
            return next.handle(request);

        })

            // call materialize and dematerialize to ensure delay even if an error is thrown (https://github.com/Reactive-Extensions/RxJS/issues/648)
            .materialize()
            .delay(500)
            .dematerialize();
    }
}

export let fakeBackendProvider = {
    // use fake backend in place of Http service for backend-less development
    provide: HTTP_INTERCEPTORS,
    useClass: FakeBackendInterceptor,
    multi: true
};